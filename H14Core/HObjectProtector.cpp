#include "HObjectProtector.h"

#include <mutex>

#include "HObject.h"

namespace HE {

HObjectProtector::HObjectProtector(HObject* object)
	: m_object{object} {
	m_object->m_objectMutex.lock();
}

HObjectProtector::~HObjectProtector() {
	m_object->m_objectMutex.unlock();
}

} // namespace HE