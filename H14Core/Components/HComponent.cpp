#include "../Modules/HComponentFactory.h"

#include "HComponent.h"

namespace HE {

unsigned long HComponent::m_idCounter{0};

HComponent::HComponent()
	: m_id{++m_idCounter} {

	m_componentTransform.InitIdentity();
}

bool HComponent::operator==(const HComponent& rhs) {
	return m_id == rhs.m_id;
}

void HComponent::RegisterComponent(std::unique_ptr<HComponent> component) {
	m_childComponents.insert(std::move(component));
}

void HComponent::Update() {
	if (m_updateable) {
		OnUpdate();
	}

	for (auto& component : m_childComponents) {
		component->Update();
	}
}

void HComponent::Render() {
	if (m_renderable) {
		OnRender();
	}

	for (auto& component : m_childComponents) {
		component->Render();
	}
}

void HComponent::FileInit(const std::string& parameterName, const std::string& parameterValue) {
	OnFileInit(parameterName, parameterValue);
}

void HComponent::SetUpdateable(bool updateable) {
	m_updateable = updateable;
}

void HComponent::SetRenderable(bool renderable) {
	m_renderable = renderable;
}

//void HComponent::SetGame(HGame* game) {
//	m_game = game;
//}

//void HComponent::SetPosition(const Vec3f& position) {
//	m_position = position;
//}
//
//void HComponent::SetPosition(Vec3f&& position) {
//	m_position = std::move(position);
//}

//HGame* HComponent::GetGame() const {
//	return m_game;
//}

unsigned long HComponent::GetID() const {
	return m_id;
}

const Mat44& HComponent::GetTransform() const {
	return m_componentTransform;
}

//const Vec3f& HComponent::GetPosition() const {
//	return m_position;
//}

}