#include "../HGame.h"
#include "HActor.h"

#include "../Modules/HLogger.h"
#include "HSprite.h"
#include "UnitController/HUnitController.h"

#include <iostream> // test

namespace HE {

HActor::HActor()
	: m_dead{false}
	, m_ltDefined{false}
	, m_ltDuration{std::chrono::milliseconds(0)}
	, m_ltCreationTime{std::chrono::steady_clock::now()}
	, m_unitController{nullptr}
	, m_actorName{"HActor"} {

	SetUpdateable();
}

HActor::~HActor() {
	if (m_unitController != nullptr) {
		m_unitController->NotifyDead();
	}
}

void HActor::OnUpdate() {
	if (m_ltDefined) {
		// This may be slow, do we need 100% deterministic precision? Maybe include a flag to turn this off or do it still deterministic with a "all 60 updates check"
		auto now = std::chrono::steady_clock::now();

		if (now - m_ltCreationTime > m_ltDuration) {
			m_dead = true;
		}
	}
}

void HActor::OnRender() {
	
}

void HActor::OnFileInit(const std::string& paramterName, const std::string& parameterValue) {
	GetGame()->GetModule<HLogger>()->Log(ELogLevel::Info, "OnFileInit() called");

	// TODO: Don't forget to implement the SetName() on file init! Still defaults set in Puppet which acts as a demo!
	// Maybe force it through the ctor later? We'll see once we implement file init..
}

void HActor::SetActorName(std::string name) {
	m_actorName = std::move(name);
}

void HActor::SetUnitControllerOwner(HUnitController* unitController) {
	m_unitController = unitController;
}

void HActor::SetLifeTime(std::chrono::steady_clock::duration duration) {
	m_ltDefined = true;
	m_ltDuration = duration;
}

void HActor::Possess(HUnitController* unitController) {
	if (m_unitController != nullptr) {
		m_unitController->SetControlledActor(nullptr);
	}

	m_unitController = unitController;
	m_unitController->SetControlledActor(this);
}

void HActor::Unpossess() {
	if (m_unitController != nullptr) {
		m_unitController->SetControlledActor(nullptr);
		m_unitController = nullptr;
	}
}

HUnitController* HActor::GetUnitcontrollerOwner() const {
	return m_unitController;
}

bool HActor::HasDeadFlag() const {
	return m_dead;
}

const std::string & HActor::GetActorName() const {
	return m_actorName;
}

//HActor::HActor() = default;
//HActor::~HActor() = default;
//HActor::HActor(HActor&&) = default;
//HActor& HActor::operator=(HActor&&) = default;

//void HActor::PostConstructInit() {
//	AddComponent<HSprite>();
//}

} // namespace HE
