#pragma once

#include "HActor.h"

namespace HE {

class _HCamera;

/**
* HComponent class
*
* Not to be confused with the low-level HCamera
*
* [ ] Copyable (Inherited from HComponent)
* [x] Movable (Inherited from HComponent)
*
**/
class HComponentCamera : public HActor
{
public:
	HFactoryConstructible

	~HComponentCamera();

	void PostConstructInit() override;

	// For testing, this should not be the way to do it, instead we need some central function?
	void _Possess();

	void MoveForward();
	void MoveBackward();

	void StrafeLeft();
	void StrafeRight();

	void RotateBy(Rad heading, Rad attitude, Rad bank);

	const _HCamera* GetCamera() const;

private:
	HComponentCamera(const HRenderer* renderer);
	HComponentCamera(const HRenderer* renderer, Rad degFOV, float nearDist, float farDist, Vec3f eye, Vec3f upAxis, Rad heading, Rad attitude, Rad bank);

	std::unique_ptr<_HCamera> m_camera;

protected:
	void OnUpdate() override;
};

}