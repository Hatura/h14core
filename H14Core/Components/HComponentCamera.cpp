#include "../HGame.h"
#include "HComponentCamera.h"

#include "../Modules/HRenderer.h"
#include "../Modules/HLogger.h"

namespace HE {

// TODO: Replace with game constants for degFOV, nearDist, farDist etc.
HComponentCamera::HComponentCamera(const HRenderer* renderer)
	: HComponentCamera{renderer, Rad(Degree(45.f)), 0.1f, 200.f, Vec3f{0.f, 0.f, 10.f}, Vec3f{0.f, 1.0f, 0.f}, Rad::FromDegree(0.f), Rad{0.f}, Rad{0.f}} {

}

HComponentCamera::HComponentCamera(const HRenderer* renderer, Rad degFOV, float nearDist, float farDist, Vec3f eye, Vec3f upAxis, Rad heading, Rad attitude, Rad bank)
	: m_camera{renderer->CreateCamera(std::move(degFOV), nearDist, farDist, std::move(eye), std::move(upAxis), std::move(heading), std::move(attitude), std::move(bank))} {

}

HComponentCamera::~HComponentCamera() {
	//GetGame()->GetModule<HRenderer>()->UnregisterCamera(this);
}

void HComponentCamera::PostConstructInit() {
	SetActorName("HComponentCamera");

	SetUpdateable();
}

void HComponentCamera::OnUpdate() {
	if (HKeyboard::IsKeyDown(EKeyboardKey::Q)) {
		//m_camera->FixatingMoveBy(Vec3f{0.f, 0.1f, 0.f});
		//GetGame()->GetModule<HLogger>()->Log(ELogLevel::Debug, "Moved camera to position\n", m_camera->GetLocation());
	}
	if (HKeyboard::IsKeyDown(EKeyboardKey::U)) {
		//m_camera->FixatingMoveBy(Vec3f{-0.1f, 0.f, 0.f});
		//GetGame()->GetModule<HLogger>()->Log(ELogLevel::Debug, "Moved camera to position\n", m_camera->GetLocation());
	}
	if (HKeyboard::IsKeyDown(EKeyboardKey::I)) {
		//m_camera->FixatingMoveBy(Vec3f{0.1f, 0.f, 0.f});
		//GetGame()->GetModule<HLogger>()->Log(ELogLevel::Debug, "Moved camera to position\n", m_camera->GetLocation());
	}
}

void HComponentCamera::_Possess() {
	GetGame()->GetModule<HRenderer>()->SetActiveCamera(m_camera.get());
}

void HComponentCamera::MoveForward() {
	//GetGame()->GetModule<HLogger>()->Log(ELogLevel::Debug, m_camera->GetCameraForward());
	//m_camera->MoveForward();
	m_camera->MoveForwards(0.001f);
}

void HComponentCamera::MoveBackward() {
	//m_camera->StrafingMoveBy(Vec3f{0.f, 0.f, 0.01f});
	m_camera->MoveBackwards(0.001f);
}

void HComponentCamera::StrafeLeft() {
	//m_camera->StrafingMoveBy(Vec3f{-0.01f, 0.f, 0.f});
	m_camera->StrafeLeft(0.0025f);
}

void HComponentCamera::StrafeRight() {
	//m_camera->StrafingMoveBy(Vec3f{0.01f, 0.f, 0.f});
	m_camera->StrafeRight(0.0025f);
}

void HComponentCamera::RotateBy(Rad heading, Rad attitude, Rad bank) {
	std::string sbuffer;

	m_camera->RotateBy(sbuffer, heading, attitude, bank);
}

const _HCamera* HComponentCamera::GetCamera() const {
	return m_camera.get();
}

} // namespace HE