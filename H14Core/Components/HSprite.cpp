#include "../HGame.h" // Because we need the access in subclasses anyway
#include "HSprite.h"

#include <iostream>
#include <limits>

#include "../Modules/HWindow.h"
#include "../Modules/HRenderer.h"
#include "../Modules/HLogger.h"

namespace HE {

HSprite::HSprite() 
	// Default init to max because 0 is a valid buffer ID, should be safe
	: m_vao(std::numeric_limits<unsigned int>::max())
	, m_vbo(std::numeric_limits<unsigned int>::max()) {

}

HSprite::~HSprite() {
	GetGame()->GetModule<HRenderer>()->UnregisterSprite(this);
}

void HSprite::PostConstructInit() {
	SetRenderable();

	m_resource = GetGame()->GetModule<HResourceManager>()->LoadSync<HImageResource>("amarsch.bmp");

	// The following is OpenGL 4.0 (4.2 targeted) code

	glGenVertexArrays(1, &m_vao);
	glBindVertexArray(m_vao);

	glGenBuffers(1, &m_vbo);

	GLfloat vertices[]{
		-0.5f, -0.5f,
		0.5f, -0.5f,
		0.5f, 0.5f,
		-0.5f, 0.5f
	};

	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	GLuint indices[]{
		0, 1, 2,
		2, 3, 0
	};

	glGenBuffers(1, &m_ebo);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	GLint posAttrib = glGetAttribLocation(GetGame()->GetWindow()->GetShaderID(), "position");
	glEnableVertexAttribArray(posAttrib);
	glVertexAttribPointer(posAttrib, 2, GL_FLOAT, GL_FALSE, 0, nullptr);

	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	const GLfloat uvIndexes[]{
		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f
	};

	GLuint uvBuffer;
	glGenBuffers(1, &uvBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, uvBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(uvIndexes), uvIndexes, GL_STATIC_DRAW);

	GLint texCoordUVAttrib = glGetAttribLocation(GetGame()->GetModule<HWindow>()->GetShaderID(), "texCoordUV");
	glEnableVertexAttribArray(texCoordUVAttrib);
	glBindBuffer(GL_ARRAY_BUFFER, uvBuffer);
	glVertexAttribPointer(texCoordUVAttrib, 2, GL_FLOAT, GL_FALSE, 0, nullptr);

	// Unbind VAO
	glBindVertexArray(0);

	GLuint textureID;
	glGenTextures(1, &textureID);

	glBindTexture(GL_TEXTURE_2D, textureID);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, m_resource.GetResource()->GetTextureWidth(), m_resource.GetResource()->GetTextureHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, &m_resource.GetResource()->GetData().front());

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	GetGame()->GetModule<HRenderer>()->RegisterSprite(this);

	// If this is done async, we need to register it in a lambda executed after the load is finished, and if there is a placeholder texture, we need to
	// register this first and unregister it when the async load is ready!
	
	//std::cout << m_resource.GetResource()->GetUsageCount() << std::endl

/*
	float points[] = {
		0.0f, 0.5f, 0.0f,
		0.5f, -0.5f, 0.0f,
		-0.5f, -0.5f, 0.0f
	};

	// VAO creation
	glGenVertexArrays(1, &m_vao);
	glBindVertexArray(m_vao);

	// VBO creation
	glGenBuffers(1, &m_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	glBufferData(GL_ARRAY_BUFFER, 9 * sizeof(float), points, GL_STATIC_DRAW);

	// VAO additional defs
	GLint posAttrib = glGetAttribLocation(GetGame()->GetWindow()->GetShaderID(), "position");
	glEnableVertexAttribArray(posAttrib);
	glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, 0, nullptr);

	// Unbind VAO
	glBindVertexArray(0);

	GetGame()->GetModule<HRenderer>()->RegisterSprite(this);
*/

/*
	float points[] = {
		0.0f, 0.5f, 0.0f,
		0.5f, -0.5f, 0.0f,
		-0.5f, -0.5f, 0.0f
	};

	glGenBuffers(1, &m_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	glBufferData(GL_ARRAY_BUFFER, 9*sizeof(float), points, GL_STATIC_DRAW);

	GetGame()->GetModule<HRenderer>()->RegisterSprite(this);
*/
}

void HSprite::OnUpdate() {}

void HSprite::OnRender() {
	// This is OpenGL 4.2 Code

	// Just testing, this isn't meant to be called on every Render
	GLuint matrixID = glGetUniformLocation(GetGame()->GetModule<HWindow>()->GetShaderID(), "mvp");

	auto combinedMat = GetTransform() * GetGame()->GetModule<HRenderer>()->GetActiveCamera()->GetViewProjectionMatrix();

	glUniformMatrix4fv(matrixID, 1, GL_FALSE, &combinedMat[0][0]);

	// If we ever really want different graphics APIs we need to put the implementations in different files, for now we just go with OpenGL
	glBindVertexArray(m_vao);
	//std::cout << "Rendering" << std::endl;
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);

/*
	glBindVertexArray(m_vao);
	glDrawArrays(GL_TRIANGLES, 0, 3);
	glBindVertexArray(0);
*/

/*
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glDrawArrays(GL_TRIANGLES, 0, 3);
*/
}

const HSharedRes<HImageResource>& HSprite::GetImageResource() const {
	return m_resource;
}

} // namespace HE
