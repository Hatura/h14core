#pragma once

#include "HFlexComponent.h"

#include <string>
#include <memory>
#include <vector>

#include "../Glew/GL/glew.h"

#include "../Modules/HResourceManagerExt/HSharedRes.h"
#include "../Resources/HImageResource.h"

namespace HE {

class H2DMaterialInstance;

class HSprite : public HFlexComponent
{
public:
	HFactoryConstructible

	//void SetMaterial
	~HSprite();

	HSprite(const HSprite&) = delete;
	HSprite& operator= (const HSprite&) = delete;
	HSprite(HSprite&&) = default;
	HSprite& operator= (HSprite&&) = default;

	const HSharedRes<HImageResource>& GetImageResource() const;

private:
	HSprite();

	void OnUpdate() override;
	void OnRender() override;

	void PostConstructInit() override;

	HSharedRes<HImageResource> m_resource;

	GLuint m_vao;
	GLuint m_vbo;
	GLuint m_ebo;
};

}