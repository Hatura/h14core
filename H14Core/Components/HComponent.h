#pragma once

#include "../HObject.h"

#include <set>
#include <memory>
#include <mutex>
#include <atomic>

#include "../Math/Math.h"
#include "../Modules/HComponentFactory.h"

// This needs to be used in custom creatable HComponent derived classes that should be constructible through HFactory, it has better semantics
// than friend class HFactory for people not knowing the Engine
#define HFactoryConstructible		friend class ::HE::HComponentFactory;

namespace HE {

class HGame;
class HComponentFactory;
class HFlexComponent;

/**
* HComponent class
*
* Virtual base class for everything that is a component. Components need to be constructed throguht HGame::ConstructComponent, they get automatically
* injected with mandatory parameters + custom paramters as a paramter pack forwarded from ConstructComponent. Components are not copyable but may
* support a Clone() method later, moving from them is legal but should only be done within factory methods since it could invalidate addresses stored
* in internal modules
*
* Remember that we never have access to GetGame() in the constructor because it is only injected after all the ctor's (virtual) are called.. I don't
* have a solution for that yet but if you really need a game module you should pass it as ctor argument for now!
*
* [ ] Copyable
* [x] Movable
*
**/
class HComponent : public HObject
{
public:
	~HComponent() = default;

	HComponent(const HComponent& other) = delete;
	HComponent& operator= (const HComponent& other) = delete;
	HComponent(HComponent&&) = default;
	HComponent& operator= (HComponent&&) = default;

	// Compares IDs
	bool operator== (const HComponent& rhs);

	HFactoryConstructible

	// We should really leave this here because these are hyper-critical functions
	friend class HActorManager;		// Access to Update()
	friend class HRenderer;			// Access to Render()

	// This is called after construction in HFactory is done, it basically allows virtual function construction if necessary (although the construction paramters are not present)
	virtual void PostConstructInit() = 0;

protected:
	HComponent();

	void RegisterComponent(std::unique_ptr<HComponent> component);

	template<typename T, typename... Args>
	T* AddComponent(Args&&... args);

	void SetUpdateable(bool updateable = true);
	void SetRenderable(bool renderable = true);	

	// Overwrite for custom Update behavior
	virtual void OnUpdate() {}

	// Overwrite for custom Render behavior
	virtual void OnRender() {}

	virtual void OnFileInit(const std::string& paramterName, const std::string& paramterValue) {}

	void SetPosition(const Vec3f& position);
	void SetPosition(Vec3f&& position);

	//HGame* GetGame() const;

	unsigned long GetID() const;

	//const Vec3f& GetPosition() const;

	const Mat44& GetTransform() const;

	//inline bool IsUpdateable() const {
	//	return m_isUpdateable;
	//}

	//inline bool IsRenderable() const {
	//	return m_isRenderable;
	//}

private:
	//void SetGame(HGame* game);

	// Internal method, don't change anything here, it just calls OnUpdate() if updateable and Update() (->OnUpdate()) on all children and their children...
	void Update();

	// Internal method, don't change anything here, it just calls OnRender() if renderable and Render() (->OnRender()) on all children and their children...
	void Render();

	// Internal method
	void FileInit(const std::string& parameterName, const std::string& parameterValue);

	//HGame* m_game;

	// Not thread safe, but we never call HFactory over a thread..! (m_id not thread safe on increment)
	static unsigned long m_idCounter;
	const unsigned long m_id;

	bool m_updateable;
	bool m_renderable;

	//Vec3f m_position;
	Mat44 m_componentTransform;

	// TODO: Faster way? I think unordered_sets get faster when there are many elements in it, benchmark it sometime
	std::multiset<std::unique_ptr<HComponent>> m_childComponents;
};

// Defined outside the class to avoid including HGame.h and HFactory.h (circular dependency)
// Since the template is insistiated once first called he gets HGame.h through the fact that every .cpp file hast to include HGame.h
// and HGame automatically includes HFactory.h
template<typename T, typename... Args>
inline T* HComponent::AddComponent(Args&&... args) {
	static_assert(std::is_base_of<HFlexComponent, T>::value, "ChildComponents must be of type HFlexComponent");
	auto uPtr = GetGame()->GetModule<HComponentFactory>()->ConstructComponent<T>(std::forward<Args>(args)...);
	auto ptr = m_childComponents.insert(std::move(uPtr))->get();

	//CreateInterfaceSpecificLinkage<T>(ptr);

	// This is the bad version, it's slow!
	//auto renderable = dynamic_cast<HRenderableInterface*>(ptr);
	//if (renderable != nullptr) {
	//	m_renderableComponents.push_back(renderable);
	//}

	return dynamic_cast<T*>(ptr);
}

//template<typename T, typename = void>
//void HComponent::CreateInterfaceSpecificLinkage() {
//
//}

} // namespace HE
