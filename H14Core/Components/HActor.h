#pragma once

#include "HComponent.h"

#include <chrono>

namespace HE {

class HUnitController;

/**
* HActor class
*
* An Actor is everything that has supports Physics and other Components, it may or may not be directly Updated if supported
*
* [ ] Copyable (Inherited from HComponent)
* [x] Movable (Inherited from HComponent)
*
*/
class HActor : public HComponent
{
public:
	~HActor();

	HFactoryConstructible
	friend class HUnitController;	// Critical access, hierachical similarity

	// Sets the lifetime of the Actor, the lifetime starts upon the creation through HFactory
	void SetLifeTime(std::chrono::steady_clock::duration duration);

	void Possess(HUnitController* unitController);
	void Unpossess();

	HUnitController* GetUnitcontrollerOwner() const;

	bool HasDeadFlag() const;

	const std::string& GetActorName() const;

protected:
	HActor();

	void OnUpdate() override;
	void OnRender() override;

	void OnFileInit(const std::string& paramterName, const std::string& parameterValue) override;

	void SetActorName(std::string name);

private:
	void SetUnitControllerOwner(HUnitController* unitController);

	// How to make this static without using CRTP? Currently every actor stores the name, that's inefficient!
	std::string								m_actorName;

	bool									m_dead;

	bool									m_ltDefined;
	std::chrono::steady_clock::time_point	m_ltCreationTime;
	std::chrono::steady_clock::duration		m_ltDuration;

	HUnitController*						m_unitController;
};

} // namespace HE