#pragma once

#include "HComponent.h"

namespace HE {

/**
* HFlexComponent class
*
* HFlexComponents can be (user)added to normal Actors/Components into their subComponents
*
* [ ] Copyable (inherited)
* [x] Movable (inherited)
*
**/
class HFlexComponent : public HComponent
{
public:
	HFlexComponent();
};

} // namespace HE