#include "HUnitController.h"

#include "../HActor.h"

namespace HE {

HUnitController::HUnitController()
	: m_controlledActor{nullptr} { }

void HUnitController::PossessActor(HActor* actor) {
	if (m_controlledActor != nullptr) {
		m_controlledActor->SetUnitControllerOwner(nullptr);
	}

	m_controlledActor = actor;
	m_controlledActor->SetUnitControllerOwner(this);
}

void HUnitController::UnpossessActor() {
	if (m_controlledActor != nullptr) {
		m_controlledActor->SetUnitControllerOwner(nullptr);
		m_controlledActor = nullptr;
	}
}

HActor* HUnitController::GetControlledActor() const {
	return m_controlledActor;
}

void HUnitController::SetNullActor() {
	m_controlledActor = nullptr;
}

void HUnitController::SetControlledActor(HActor* actor) {
	m_controlledActor = actor;
}

void HUnitController::NotifyDead() {
	SetNullActor();
}

} // namespace HE