#pragma once

#include "HUnitController.h"

#include <map>
#include <string>

#include "../../Input/HInputController.h"

namespace HE {

/**
* HPlayerController class
*
* A PlayerController polls the input in different states (type of actor possessed for example)
*
*
*
*
*/
class HPlayerController : public HUnitController
{
public:
	void Update() override;

	friend class HGameMode;	// Constructor access

	void MapButton(EKeyboardKey key, EInputBehavior behavior, std::string keybindName);
	void MapButton(EMouseButton button, EInputBehavior behavior, std::string keybindName);

	void RegisterKeybindCallback(const HKeybindCallback<HActor>& bind);
	void RegisterKeybindCallback(HKeybindCallback<HActor>&& bind);

	template<typename T>
	void RegisterMoveCallback(T&& callback) {
		m_inputController.RegisterMoveCallback(std::forward<T>(callback));
	}
	
	//HInputController<HActor>& GetInputController() {
	//	// Just forwarding the pointer is unsafe, we should implement forwarding methods to HInputContorller, we can also remove the friend class in HInputController then.
	//	return m_inputController;
	//}

	unsigned int GetPlayerID() const;

private:
	HPlayerController(HGame* game, unsigned int playerID);

	unsigned int m_playerID;

	std::map<std::string, std::function<void(HActor*)>> m_registeredKeybinds;

	HInputController<HActor> m_inputController;

	// Really save this here?
	Vec2i m_savedMousePosition;
};

} // namespace HE