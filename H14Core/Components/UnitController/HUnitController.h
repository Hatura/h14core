#pragma once

#include "../../HObject.h"

namespace HE {

class HActor;

class HUnitController : public HObject
{
public:
	HUnitController();
	virtual ~HUnitController() = default;

	friend class HActor;

	void NotifyDead();

	// Critical fuctions can be public because it's always a private member of HActor type
	virtual void Update() = 0;

	void PossessActor(HActor* actor);
	void UnpossessActor();

	HActor* GetControlledActor() const;

protected:
	void SetNullActor();

private:
	void SetControlledActor(HActor* actor);

	HActor* m_controlledActor;
};

}