#include "../../HGame.h"
#include "HPlayerController.h"

#include "../../Modules/HWindow.h"

#include "../../Input/HKeyboard.h"

#include "../HActor.h"
#include "../../HPuppet.h"

#include <iostream>

namespace HE {

HPlayerController::HPlayerController(HGame* game, unsigned int playerID)
	: m_playerID{playerID}
	, m_savedMousePosition{0, 0} {

	SetGame(game);

	m_inputController.RegisterMoveDevice<TMouseDevice>();
}

//void HPlayerController::PostConstructInit() {
//
//}

void HPlayerController::Update() {
/*
	if (HKeyboard::IsKeyDown(EKeyboardKey::W)) {
		GetGame()->GetModule<HLogger>()->Log(ELogLevel::Debug, "W down on PlayerController");
	}
*/

	if (GetControlledActor() != nullptr) {
		//m_bindContainer.EvaluateAndInvoke(GetControlledActor(), GetControlledActor()->GetActorName());
		if (GetGame()->GetWindow()->HasFocus()) {
			m_inputController.PollInput(GetControlledActor(), GetControlledActor()->GetActorName());
		}
	}
}

void HPlayerController::MapButton(EKeyboardKey key, EInputBehavior behavior, std::string keybindName) {
	m_inputController.MapButton(key, behavior, std::move(keybindName));
}

void HPlayerController::MapButton(EMouseButton button, EInputBehavior behavior, std::string keybindName) {
	m_inputController.MapButton(button, behavior, std::move(keybindName));
}

void HPlayerController::RegisterKeybindCallback(const HKeybindCallback<HActor>& bind) {
	m_inputController.RegisterKeybindCallback(bind);
}

void HPlayerController::RegisterKeybindCallback(HKeybindCallback<HActor>&& bind) {
	m_inputController.RegisterKeybindCallback(std::move(bind));
}

unsigned int HPlayerController::GetPlayerID() const {
	return m_playerID;
}

} // namespace HE
