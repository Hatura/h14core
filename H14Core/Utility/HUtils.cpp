#include "HUtils.h"

#include <chrono>
#include <time.h>

namespace HE {

std::string HUtils::GetFormattedTimeString() {
	auto now = std::chrono::system_clock::now();
	time_t tt = std::chrono::system_clock::to_time_t(std::move(now));

	tm local_tm;
#ifdef _WIN32
	localtime_s(&local_tm, &tt);
#elif __linux__
	localtime_r(&tt, &local_tm);
#endif

	auto hours = std::to_string(local_tm.tm_hour);
	auto mins = std::to_string(local_tm.tm_min);
	auto secs = std::to_string(local_tm.tm_sec);

	if (hours.size() == 1) {
		hours.insert(0, "0");
	}
	if (mins.size() == 1) {
		mins.insert(0, "0");
	}
	if (secs.size() == 1) {
		secs.insert(0, "0");
	}

	std::string timeString = "[";
	timeString.append(hours); // Append does not support move, it reads by const ref and makes internally copies?
	timeString.append(":");
	timeString.append(mins);
	timeString.append(":");
	timeString.append(secs);
	timeString.append("]");

	return timeString;
}

} // namespace HE
