#include "HHighResClock.h"

#ifdef _WIN32
#include <Windows.h>
#endif

namespace HE {

#ifdef _WIN32

namespace
{
	const long long g_Frequency = [] () -> long long {
		LARGE_INTEGER frequency;
		QueryPerformanceFrequency(&frequency);
		return frequency.QuadPart;
	}();
}

HHighResClock::time_point HHighResClock::now() {
	LARGE_INTEGER count;
	QueryPerformanceCounter(&count);
	return time_point(duration(count.QuadPart * static_cast<rep>(period::den) / g_Frequency));
}

#elif __linux

HHighResClock::time_point HHighResClock::now() {
	timespec ts;
	clock_gettime(CLOCK_REALTIME, &ts);

	// duration
	auto duration = std::chrono::seconds{ts.tv_sec}
			+ std::chrono::nanoseconds{ts.tv_nsec};

	// timepoint
	auto tp = time_point(duration);

	return tp;
}

#endif

} // namespace HE
