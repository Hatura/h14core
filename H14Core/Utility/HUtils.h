#pragma once

#include <string>

namespace HE {

class HUtils
{
public:
	HUtils() = delete;

	static std::string GetFormattedTimeString();
};

}