#pragma once

#include <chrono>

namespace HE {

/**
* HHighResClock class
*
* Some implementations of std::chrono::high_resolution_clock return not accurate enough time_stamps when dealing with nanoseconds
* this should return more accurate timeframes, but sadly is a windows only implementation, maybe check high_resolution_clock on linux
* and if it works #1 rewrite this class under same name (with same method names) as wrapper for high_resolution_clock for linux or
* #2 define the other usage (should just be auto time = after - before) in the classes where this is used (aka the shitty solution)
*
* Source: stackoverflow.com/questions/16299029/resolution-of-stdchronohigh-resolution-clock-doesnt-correspond-to-measureme
*
* No 
*/
class HHighResClock
{
public:
	HHighResClock() = delete;

	typedef long long                               rep;
	typedef std::nano				period;
	typedef std::chrono::duration<rep, period>	duration;
	typedef std::chrono::time_point<HHighResClock>	time_point;
	static const bool 				is_steady = true;

	static time_point now();
};

} // namespace HE
