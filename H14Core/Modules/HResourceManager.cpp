#include "../HGame.h"
#include "HResourceManager.h"

#include <iostream>
#include <fstream>

namespace HE {

HResourceManager::HResourceManager(HGame* game)
	: HModule{game, "HResourceManager"}
	, m_fileNotFoundBehavior{EFileNotFoundBehavior::DefaultThenQuit} {

}

void HResourceManager::ReportZeroSharedResource(HResource* resource) {
	// Check if resource is still async loaded or in queue, if not delete it
	GetGame()->GetModule<HLogger>()->Log(ELogLevel::Info, "Free'd resource ", resource->GetResourceName());

	m_resources.erase(resource->GetResourceName());
}

void HResourceManager::SetFileNotFoundBehavior(EFileNotFoundBehavior behavior) {
	m_fileNotFoundBehavior = behavior;
}

bool HResourceManager::CheckFileExists(const std::string &fileName) {
	std::ifstream fileIO(fileName);

	bool exists = fileIO.good();

	return exists;
}

} // namespace HE