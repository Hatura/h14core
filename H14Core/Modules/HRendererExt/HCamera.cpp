#include "HCamera.h"

#include "../HRenderer.h"

#include <string> // test

namespace HE {

_HCamera::_HCamera(const HRenderer* renderer, Rad degFOV, float nearDist, float farDist, Vec3f eye, Vec3f upAxis, Rad heading, Rad attitude, Rad bank)
	: m_renderer{renderer}
	, m_eye{std::move(eye)}
	, m_lookAt{0.f, 0.f, 0.f}
	, m_upAxis{upAxis}
	, m_heading{std::move(heading)}
	, m_attitude{std::move(attitude)}
	, m_bank{std::move(bank)}
	, m_camRotation{} {

	auto renderResolution = m_renderer->GetRenderResolution();

	m_pMatrix = FCreatePerspectiveMat(degFOV.GetValue(), static_cast<float>(renderResolution[0]) / static_cast<float>(renderResolution[1]), nearDist, farDist);

	//Quaternion quatHeading{heading, Vec3f{0.f, 0.f, 1.f}};
	//Quaternion quatAttitude{attitude, Vec3f{1.f, 0.f, 0.f}};
	//Quaternion quatBank{bank, Vec3f{0.f, 1.f, 0.0f}};

	//m_camRotation = quatHeading * quatAttitude * quatBank;

	//m_lookAt[0] = m_camRotation.GetX();
	//m_lookAt[1] = m_camRotation.GetY();
	//m_lookAt[2] = m_camRotation.GetZ();

	//m_lookAt.Normalize();

	//auto lookAtMat = FCreateLookAtMat(m_eye, m_lookAt, m_upAxis);

	m_vMatrix = FCreateFPSMat(m_eye, m_heading, m_attitude);

	Recalculate();
}

void _HCamera::RotateBy(std::string& buffer, Rad heading, Rad attitude, Rad bank) {
	m_heading += heading;
	m_attitude += attitude;
	m_bank += bank;

	if (m_attitude.GetValue() > FDegreeToRad(90.f)) {
		m_attitude = Rad::FromDegree(90.f);
	}
	else if (m_attitude.GetValue() < FDegreeToRad(-90.f)) {
		m_attitude = Rad::FromDegree(-90.f);
	}

	//Vec3f rotationAxis = VecCross(m_lookAt - m_eye, m_upAxis);

	//Quaternion quatHeading{m_heading.ToDegree(), rotationAxis};
	//Quaternion quatAttitude{m_attitude, Vec3f{1.f, 0.f, 0.f}};
	//Quaternion quatBank{m_bank, Vec3f{0.f, 1.f, 0.0f}};

	//buffer += std::to_string(quatHeading.GetX());
	//buffer += ", ";
	//buffer += std::to_string(quatHeading.GetY());
	//buffer += ", ";
	//buffer += std::to_string(quatHeading.GetZ());
	//buffer += ", ";
	//buffer += std::to_string(quatHeading.GetW());

	//m_camRotation = quatHeading * quatAttitude * quatBank;

	//m_lookAt[0] = m_camRotation.GetX();
	//m_lookAt[1] = m_camRotation.GetY();
	//m_lookAt[2] = m_camRotation.GetZ();

	//auto lookAtMat = FCreateLookAtMat(m_eye, m_lookAt, m_upAxis);

	m_vMatrix = FCreateFPSMat(m_eye, m_heading, m_attitude);

	Recalculate();
}

//void _HCamera::RotateByAxis(float angle, const Vec3f& axis) {
//	float halfAngle = angle * 0.5f;
//
//	Quaternion tempQ{axis[0] * std::sin(halfAngle), axis[1] * std::sin(halfAngle), axis[2] * std::sin(halfAngle), std::cos(halfAngle)};
//
//	Quaternion lookAtQ{m_lookAt[0], m_lookAt[1], m_lookAt[2], 0.f};
//
//	Quaternion tempConjQ = tempQ;
//	tempConjQ.Conjugate();
//
//	Quaternion result = tempQ * lookAtQ;
//
//	result = result * tempConjQ;
//
//	m_lookAt[0] = result.GetX();
//	m_lookAt[1] = result.GetY();
//	m_lookAt[2] = result.GetZ();
//
//	auto lookAtMat = FCreateLookAtMat(m_eye, m_lookAt, m_upAxis);
//
//	m_vpMatrix = lookAtMat * m_pMatrix;
//
//}

void _HCamera::RotateAbsolute(Rad heading, Rad attitude, Rad bank) {

}

void _HCamera::MoveForwards(float sensitivity) {
	Vec3f forward = m_lookAt;

	m_eye[0] += forward[0] * sensitivity;

	if (forward[2] < -0.001f) {
		m_eye[1] -= forward[1] * sensitivity;
	}
	else {
		m_eye[1] += forward[1] * sensitivity;
	}

	m_eye[2] -= forward[2] * sensitivity;

	m_vMatrix = FCreateFPSMat(m_eye, m_heading, m_attitude);

	Recalculate();
}

void _HCamera::MoveBackwards(float sensitivity) {
	Vec3f forward = m_lookAt;

	m_eye[0] -= forward[0] * sensitivity;
	
	if (forward[2] < -0.001f) {
		m_eye[1] += forward[1] * sensitivity;
	}
	else {
		m_eye[1] -= forward[1] * sensitivity;
	}

	m_eye[2] += forward[2] * sensitivity;

	m_vMatrix = FCreateFPSMat(m_eye, m_heading, m_attitude);

	Recalculate();	
}

void _HCamera::StrafeLeft(float sensitivity) {
	Vec3f right{m_vMatrix[0][0], m_vMatrix[1][0], m_vMatrix[2][0]};

	m_eye[0] -= right[0] * sensitivity;
	m_eye[1] -= right[1] * sensitivity;
	m_eye[2] -= right[2] * sensitivity;

	m_vMatrix = FCreateFPSMat(m_eye, m_heading, m_attitude);

	Recalculate();
}

void _HCamera::StrafeRight(float sensitivity) {
	Vec3f right{m_vMatrix[0][0], m_vMatrix[1][0], m_vMatrix[2][0]};

	m_eye[0] += right[0] * sensitivity;
	m_eye[1] += right[1] * sensitivity;
	m_eye[2] += right[2] * sensitivity;

	m_vMatrix = FCreateFPSMat(m_eye, m_heading, m_attitude);

	Recalculate();
}

void _HCamera::Recalculate() {
	m_lookAt[0] = m_vMatrix[2][0];
	m_lookAt[1] = m_vMatrix[2][1];
	m_lookAt[2] = m_vMatrix[2][2];

	m_vpMatrix = m_vMatrix * m_pMatrix;
}

const Vec3f& _HCamera::GetEye() const {
	return m_eye;
}

const Vec3f& _HCamera::GetLookAt() const {
	return m_lookAt;
}

const Vec3f& _HCamera::GetUp() const {
	return m_upAxis;
}

const Rad& _HCamera::GetHeading() const {
	return m_heading;
}

const Rad& _HCamera::GetAttitude() const {
	return m_attitude;
}

const Mat44 & _HCamera::GetViewMatrix() const {
	return m_vMatrix;
}

const Mat44 & _HCamera::GetViewProjectionMatrix() const {
	return m_vpMatrix;
}

// Old cam

HCamera::HCamera(const HRenderer& renderer, float degFOV, float nearDist, float farDist, const Vec3f& position, const Vec3f& lookAtPoint, const Vec3f& upAxis)
	: m_renderer{renderer}
	, m_eye{position}
	, m_lookAt{lookAtPoint}
	, m_savedUpAxis{upAxis} {

	auto resolution = m_renderer.GetRenderResolution();

	m_matProj = FCreatePerspectiveMat(FDegreeToRad(degFOV), static_cast<float>(resolution[0]) / static_cast<float>(resolution[1]), nearDist, farDist);

	auto lookAtMat = FCreateLookAtMat(m_eye, m_lookAt, m_savedUpAxis);
	m_bufferedLookAt = lookAtMat;
	m_rotationMatrix.InitIdentity();

	m_matrix = lookAtMat * m_matProj;
}

const Mat44& HCamera::GetMatrix() const {
	return m_matrix;
}

Vec3f HCamera::GetLocation() const {
	return m_eye;
}

Vec3f HCamera::GetCameraForward() const {
	Vec3f forward{m_matrix[3][0], m_matrix[3][1], m_matrix[3][2]};
	forward.Normalize();
	forward = forward * -1;

	return forward;
}

void HCamera::FixatingMoveBy(const Vec3f& translate) {
	m_eye = m_eye + translate;

	auto lookAtMat = FCreateLookAtMat(m_eye, m_lookAt, m_savedUpAxis);
	m_bufferedLookAt = lookAtMat;

	m_matrix = lookAtMat * m_matProj;
}

void HCamera::FixatingMoveTo(const Vec3f& position) {

}

void HCamera::StrafingMoveBy(const Vec3f & translate) {
	m_eye = m_eye + translate;
	m_lookAt = m_lookAt + translate;

	auto lookAtMat = FCreateLookAtMat(m_eye, m_lookAt, m_savedUpAxis) * m_rotationMatrix;
	m_bufferedLookAt = lookAtMat;

	m_matrix = m_bufferedLookAt * m_matProj;
}

void HCamera::StrafingMoveTo(const Vec3f& position) {
	
}

void HCamera::MoveForward() {
	Vec3f forward{m_rotationMatrix[3][0], m_rotationMatrix[3][1], m_rotationMatrix[3][2]};

	m_eye *= forward * 0.1f;

	m_bufferedLookAt.Translate(forward[0], forward[1], forward[2]);
	//m_bufferedLookAt = lookAtMat;

	m_matrix = m_bufferedLookAt * m_matProj;
}

void HCamera::MoveBackward() {

}

void HCamera::LookAt(const Vec3f& lookAtPoint) {
	
}

void HCamera::RotateBy(Rad heading, Rad attitude, Rad bank) {
	m_heading += heading;
	m_attitude += attitude;
	m_bank += bank;

	Quaternion xQuat{m_heading, Vec3f{0.f, 0.f, 1.f}};
	Quaternion yQuat{m_attitude, Vec3f{1.0f, 0.f, 0.f}};

	Quaternion rotQuat = yQuat * xQuat;
	m_rotation = rotQuat;

	m_rotationMatrix = rotQuat.ToRotationMatrix();

	auto lookAtMat = FCreateLookAtMat(m_eye, m_lookAt, m_savedUpAxis) * m_rotationMatrix;
	m_bufferedLookAt = lookAtMat;

	m_matrix = lookAtMat * m_matProj;
}

} // namespace HE