#pragma once

#include "../../Math/Math.h"

namespace HE {

class HRenderer;

class _HCamera
{
public:
	friend class HRenderer; // Ctor access

	const Mat44& GetViewMatrix() const;
	const Mat44& GetViewProjectionMatrix() const;

	void RotateBy(std::string& buffer, Rad heading, Rad attitude, Rad bank);
	//void RotateByAxis(float angle, const Vec3f& axis);

	void RotateAbsolute(Rad heading, Rad attitude, Rad bank);

	void MoveForwards(float sensitivity);
	void MoveBackwards(float sensitivity);

	void StrafeLeft(float sensitivity);
	void StrafeRight(float sensitivity);

	const Vec3f& GetEye() const;
	const Vec3f& GetLookAt() const;
	const Vec3f& GetUp() const;

	const Rad& GetHeading() const;
	const Rad& GetAttitude() const;

private:
	_HCamera(const HRenderer* renderer, Rad degFOV, float nearDist, float farDist, Vec3f eye, Vec3f upAxis, Rad heading, Rad attitude, Rad bank);

	// Updates lookAt-Vector and recalculates vpMatrix
	void Recalculate();

	const HRenderer* m_renderer;

	Vec3f m_eye;
	Vec3f m_lookAt;
	Vec3f m_upAxis;

	Mat44 m_vMatrix;
	Mat44 m_pMatrix;
	Mat44 m_vpMatrix;

	Rad m_heading;
	Rad m_attitude;
	Rad m_bank;

	Quaternion m_camRotation;
};

/**
* HCamera class
*
* Don't confuse this with ComponentCameras
* A HCamera can be anything, from a integrated camera (into the palyer for example) to a subcomponent of a HComponent (possessable Cameras etc.)
*
* A HCamera is created via a HRenderer function call 
*
**/
class HCamera
{
public:
	friend class HRenderer;	// Constructor access

	void FixatingMoveBy(const Vec3f& translate);
	void FixatingMoveTo(const Vec3f& position);

	void StrafingMoveBy(const Vec3f& translate);
	void StrafingMoveTo(const Vec3f& position);

	void MoveForward();
	void MoveBackward();

	void LookAt(const Vec3f& lookAtPoint);

	void RotateBy(Rad heading, Rad attitude, Rad bank);

	const Mat44& GetMatrix() const;
	Vec3f GetLocation() const;

	Vec3f GetCameraForward() const;

private:
	// TODO: We could optimize this with rvalues (the 3 Vecs are saved), but is it worth the effort? Would have to rewrite some other stuff, maybe later..
	HCamera(const HRenderer& renderer, float degFOV, float nearDist, float farDist, const Vec3f& position, const Vec3f& lookAtPoint, const Vec3f& upAxis);

	const HRenderer& m_renderer;

	// So we don't have to reinitialize
	Vec3f m_eye;
	Vec3f m_lookAt;
	Vec3f m_savedUpAxis;

	Rad m_heading;
	Rad m_attitude;
	Rad m_bank;

	Mat44 m_matProj;
	Mat44 m_bufferedLookAt;
	Mat44 m_rotationMatrix;

	Quaternion m_rotation;

	// The actual calculated matrix
	Mat44 m_matrix;
};

} // namespace HE
