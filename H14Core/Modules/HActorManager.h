#pragma once

#include "HModule.h"

#include <memory>
#include <deque>

namespace HE {

class HActor;

/**
* HActorManager class
*
* May be renamed, handles HActor's that may need Update() and other Methods coupled to Physics or similar sutff
*
* [ ] Copyable
* [ ] Movable
*
*
*/
class HActorManager final : public HModule
{
public:
	~HActorManager();

	//HActorManager(const HActorManager&) = delete;
	//HActorManager& operator= (const HActorManager&) = delete;
	//HActorManager(HActorManager&&) = delete;
	//HActorManager& operator= (HActorManager&&) = delete;

	friend class HGameMode;			// Ctor access
	friend class HComponentFactory;	// RegisterActor() access

	void LoadPredefinedActors(const std::string& file = "../Config/Actors/Actors.hcf");

private:
	HActorManager(HGame* game);

	void Update();

	HActor* RegisterActor(std::unique_ptr<HActor> actorUPtr);

	std::deque<std::unique_ptr<HActor>> m_actors;

	std::vector<std::deque<std::unique_ptr<HActor>>::iterator> m_bufferedDeadActors;
};

} // namespace HE