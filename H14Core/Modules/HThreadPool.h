#pragma once

#include "HModule.h"

#include <mutex>
#include <condition_variable>
#include <vector>
#include <algorithm>
//#include <deque>
#include <memory>
#include <queue>
#include <functional>

#include "HThreadPoolExt/HThread.h"
#include "HThreadPoolExt/HSimpleTask.h"
#include "HThreadPoolExt/HPackager.h"

namespace HE {

/**
* HThreadPool class
*
* Threadpool class supporting mutable tasks / tasksStacks
*
* Do not ever change the number of cores during runtime, implement something to let it change on startup through config files..
* If changing number of cores during runtime is ever implemented this class or single data members needs mutexes (numCores etc. are not thread safe!)
*
* Info: Logs are always preferred as streamed tasks (it get's ordered by EStreamTaskPriority which HLogger sets to priority), remember:
* all !streamed! tasks which are marked as EStreamTaskPriority::Priority are gonna be executed before the program shuts down! If this is not
* wanted maybe add one more element to the enum and make the execution more fine grained (if more ordering is needed!)
*
* TODO: Is starting asynch work from asynch threads safe? Because they check other threads m_working which could've been already shut down?
* TODO: Still crashing sometimes on program exit, needs debug, maybe threads are missing the notify?
*
* [ ] Copyable (inherited from HModule)
* [ ] Movable (inherited from HModule)
*
**/
class HThreadPool : public HModule
{
public:
	friend class HGame;		// ProcessPriorityStreamTasks() access
	friend class HGameMode;	// ProcessAndYield() access
	friend class HThread;	// Hierachical access

	~HThreadPool();

	//HThreadPool(const HThreadPool&) = delete;
	//HThreadPool& operator= (const HThreadPool&) = delete;
	//HThreadPool(HThreadPool&&) = delete;
	//HThreadPool& operator= (HThreadPool&&) = delete;
	
	void RegisterTask(ETaskType taskType, ETaskChannel taskPriority, std::function<void()>&& function, ETaskPriority streamTaskPriority = ETaskPriority::Normal);

	template <typename T, typename ContainerType>
	void RegisterTaskPack(const ContainerType& ptrVecInit, std::function<void(T*)>&& toInvokeFunctionInit) {
		// This is still slower than checking the cores exactly where the task is prepared and invoking it directly
		// 1. Reference passing, 2. Passing std::function, 3. Invoking std::function (is this expensive) on every container
		// member
		if (m_numTotalCores <= 1) {
			for (std::size_t i = 0; i < ptrVecInit.size(); ++i) {
				toInvokeFunctionInit(ptrVecInit[i].get());
			}
		}
		else {
			auto funcs = HPackager::PackTask(ptrVecInit, std::move(toInvokeFunctionInit), GetNumCores(), m_packTasksPerCore);
			std::unique_ptr<HSimpleTask> task = nullptr;
			{
				std::lock_guard<std::mutex> lk{m_defaultPriorityTasksMutex};
				for (auto& func : funcs) {
					task.reset(new HSimpleTask(ETaskChannel::Priority, std::move(func)));
					m_defaultPriorityTasks.push_front(std::move(task));
				}
				std::sort(m_defaultPriorityTasks.begin(), m_defaultPriorityTasks.end(), [] (const auto& lhs, const auto & rhs) {
					return lhs->GetTaskPriority() > rhs->GetTaskPriority();
				});
			}
			m_defaultWorkCV.notify_all();
			m_streamWorkCV.notify_all();
		}
	}

	unsigned int GetNumCores() const;
	unsigned int GetNumTotalWorkerThreads() const;
	unsigned int GetNumDefaultWorkerThreads() const;
	unsigned int GetNumStreamWorkerThreads() const;

private:
	HThreadPool(HGame* game, unsigned int customThreadCount = 0);

	// Called by the main thread to participate in default outstanding tasks and yield til all default workers are finished
	void ProcessAndYield();

	// Currently a bit complicated but a streamed task can be a log and if we call std::terminate or set m_shutdown in a thread to true the log stuff
	// could be lost, so we need a way to dump it before we quit, this is only called in HGame::RequestShutdown()
	void ProcessPriorityStreamTasks();

	unsigned int m_numTotalCores;
	unsigned int m_numDefaultWorkerThreads;
	unsigned int m_numStreamWorkerThreads;

	unsigned int m_packTasksPerCore;

	std::vector<std::unique_ptr<HThread>> m_workerThreads;

	std::deque<std::unique_ptr<HTask>> m_streamPriorityTasks;
	std::mutex m_streamPriorityTasksMutex;

	std::deque<std::unique_ptr<HTask>> m_defaultPriorityTasks;
	std::mutex m_defaultPriorityTasksMutex;

	std::condition_variable m_streamWorkCV;
	std::condition_variable m_defaultWorkCV;
};

} // namepsace HE
