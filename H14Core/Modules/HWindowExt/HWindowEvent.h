#pragma once

#include <set>

#include "../../Input/HKeyboard.h"

namespace HE {
	enum class EWindowEvent {
		None,
		WindowMessage,
		Keyboard,
		Mouse
	};

	enum class EWindowMessage {
		Resize,
		Quit
	};

	/**
	* HWindowEvent class
	*
	* Used by HNativeWindow to handle Message / Keyboard / Mouse / ... 
	*
	* Tightly coupled to HNativeWindow
	*
	**/
	class HWindowEvent final {
	public:
		friend class HWindow;	// Constructor access

		void PushWindowMessage(EWindowMessage message);
		void PushKeyDown(EKeyboardKey key);

		// Called by HWindow after each frame to clear up previous messages
		void Clear();

		// Checks if there is a next event
		bool HasNextEvent() const;

		// Returns the next events if there is one and removes it from the event set, logical const
		EWindowEvent GetNextEvent() const;

		bool GetWindowMessageEvent(EWindowMessage message) const;

		bool IsKeyDown(EKeyboardKey key) const;
		bool IsKeyUp(EKeyboardKey key) const;

	private:
		// HWindowEvents are only created by HNativeWindows
		HWindowEvent() { };

		// For some reason std::set is alot faster than std::unordered_set
		mutable std::set<EWindowEvent> m_events;
		mutable std::set<EWindowMessage> m_windowMessages;
		mutable std::set<EKeyboardKey> m_keysDown;
		mutable std::set<EKeyboardKey> m_keysUp;
		mutable std::set<EKeyboardKey> m_storedKeysDown;
	};
}