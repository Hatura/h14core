#pragma once

#ifdef __linux

#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>

#include "../HNativeWindow.h"

namespace HE {

/**
* HLinuxWindow class
*
* Linux Window/graphics context
*
* [ ] Copyable
* [ ] Movable
*
**/
class HLinuxWindow : public HNativeWindow
{
public:
	HLinuxWindow(HGame* game, HWindow* window);
	~HLinuxWindow();

	EContextCreationResult CreateWindowContext(unsigned int width, unsigned int height, EScreenMode screenMode) override;
	EContextCreationResult CreateGraphicsContext(EGraphicsMode graphicsMode) override;
	void InitSpecificGraphicParams(EGraphicsMode graphicsMode, const Vec4f& backgroundColor) override;
	void Update() override;
	void PostUpdate() override;
	void OnQuitRequest() override;

	Vec2ui GetResolution() const override;

	const Window& GetHandle() const; 

private:
	void ParseKey(unsigned int id);

	Display* m_display;
	XVisualInfo* m_vi;
	Window m_gameWindow;
	XEvent m_xEvent;	// Saves CTor call
};

} // namepsace HE

#endif
