#ifdef __linux__

#include "../../../HGame.h"
#include "HLinuxWindow.h"

#include <iostream> // testing
#include <string>
#include "../../../Glew/GL/glew.h"
#include <GL/glx.h>
#include <GL/glu.h>

#include "../../HWindow.h"
#include "../../HLogger.h"

namespace HE {

HLinuxWindow::HLinuxWindow(HGame* game, HWindow* window)
	: HNativeWindow{game, window}
	, m_display{nullptr} {

}

HLinuxWindow::~HLinuxWindow() {

}

EContextCreationResult HLinuxWindow::CreateWindowContext(unsigned int width, unsigned int height, EScreenMode screenMode) {
	m_display = XOpenDisplay(nullptr);

	if (m_display == nullptr) {
		GetGame()->GetModule<HLogger>()->Log(ELogLevel::Error, "Couldn't connect to X-Server [XOpenDIsplay() failed]");
		return EContextCreationResult::Unsuccessful;
	}

	Window rootWindow = DefaultRootWindow(m_display);

	GLint glxDefs[] { GLX_RGBA, GLX_DEPTH_SIZE, 24, GLX_DOUBLEBUFFER, None };

	m_vi = glXChooseVisual(m_display, 0, glxDefs);

	if (m_vi == nullptr) {
		GetGame()->GetModule<HLogger>()->Log(ELogLevel::Error, "Couldn't retrieve VisualInfo with matching Defines [glXChooseVisual() failed");
		return EContextCreationResult::Unsuccessful;
	}
	else {
		GetGame()->GetModule<HLogger>()->Log(ELogLevel::Debug, "Visual selected: ", m_vi->visualid);
	}

	// TODO: Not enough error checking below here yet

	Colormap cmap = XCreateColormap(m_display, rootWindow, m_vi->visual, AllocNone);

	XSetWindowAttributes windowAttribs;
	windowAttribs.colormap = cmap;
	windowAttribs.event_mask = ExposureMask | KeyPressMask;

	m_gameWindow = XCreateWindow(m_display, rootWindow, 0, 0, width, height, 0, m_vi->depth, InputOutput, m_vi->visual, CWColormap | CWEventMask, &windowAttribs);

	XMapWindow(m_display, m_gameWindow);
	XStoreName(m_display, m_gameWindow, "Linux XWindow");

	GetGame()->GetModule<HLogger>()->Log(ELogLevel::Debug, "X-Window creation successful!");

	return EContextCreationResult::Successful;
}

EContextCreationResult HLinuxWindow::CreateGraphicsContext(EGraphicsMode graphicsMode) {
	// TODO: Not enough error checking yet..
	GLXContext glc = glXCreateContext(m_display, m_vi, nullptr, GL_TRUE);
	glXMakeCurrent(m_display, m_gameWindow, glc);

	glewExperimental = GL_TRUE;
	if (glewInit()) {
		GetGame()->GetModule<HLogger>()->Log(ELogLevel::Error, "Couldn't initialize GLEW");
		return EContextCreationResult::Unsuccessful;
	}

	GetGame()->GetModule<HLogger>()->Log(ELogLevel::Debug, "GLX Context creation successful!");

	return EContextCreationResult::Successful;
}

void HLinuxWindow::InitSpecificGraphicParams(EGraphicsMode graphicsMode, const Vec4f& backgroundColor) {
	// Not needed in Linux..
}

void HLinuxWindow::Update() {
	while (XPending(m_display) > 0) {
		XNextEvent(m_display, &m_xEvent);

		switch (m_xEvent.type) {
		case Expose:
			break;
		case KeyPress:
			GetGame()->GetModule<HLogger>()->Log(ELogLevel::Debug, "KeyPress: ", m_xEvent.xkey.keycode, "(", m_xEvent.xkey.state ,")");
			ParseKey(m_xEvent.xkey.keycode);
			break;
		case ClientMessage:
			{
				Atom wmDeleteMessage = XInternAtom(m_display, "WM_DELETE_WINDOW", False);
				XSetWMProtocols(m_display, m_gameWindow, &wmDeleteMessage, 1);
				GetGame()->GetModule<HLogger>()->Log(ELogLevel::Debug, "Received C");
				if(static_cast<Atom>(m_xEvent.xclient.data.l[0]) == wmDeleteMessage) {
					GetGame()->GetModule<HLogger>()->Log(ELogLevel::Debug, "Received WM_DELETE_WINDOW request");
					XDestroyWindow(m_display, m_xEvent.xclient.window);
					GetWindow()->PushMessageEvent(EWindowMessage::Quit);				
				}			
			}
			break;
		default:
			break;
		}
	}
}

void HLinuxWindow::PostUpdate() {
	glXSwapBuffers(m_display, m_gameWindow);
}

void HLinuxWindow::OnQuitRequest() {
	GetGame()->GetModule<HLogger>()->Log(ELogLevel::Debug, "Posting Quit Request for X");
	XDestroyWindow(m_display, m_gameWindow);
	XCloseDisplay(m_display);
}

void HLinuxWindow::ParseKey(unsigned int id) {
	if (id == 9) { GetWindow()->PushKeyDownEvent(EKeyboardKey::ESC); }
}

Vec2ui HLinuxWindow::GetResolution() const {
	XWindowAttributes windowAttribs;
	XGetWindowAttributes(m_display, m_gameWindow, &windowAttribs);
	
	Vec2ui resolution{windowAttribs.width, windowAttribs.height};

	return resolution;
}

const Window& HLinuxWindow::GetHandle() const {
	return m_gameWindow;
}

} // namespace HE

#endif
