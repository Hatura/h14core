#pragma once

#include "../../Math/Vec.h"

namespace HE {

enum class EContextCreationResult {
	Successful,
	Unsuccessful
};

enum class EScreenMode {
	WindowedResizable,
	WindowedFixed,
	WindowedFixedNoBorder,
	Fullscreen,
	Borderless	// Borderless windowed fullscreen
};

enum class EGraphicsAPI {
	OpenGL,
	Direct3D9,
	Direct3D11
};

enum class EGraphicsMode {
	OpenGL30ES,
	OpenGL40,
	OpenGL41,
	OpenGL42,
	OpenGL43,
	OpenGL44,
	OpenGL45,
	Direct3D9,
	Direct3D11
};

class HGame;
class HWindow;

/**
* HNativeWindow class
*
* Abstract base class with methods for OS independent windows
*
* OS dependend windows have to create a window via CreateWindowContext
* and a graphic context via CreateGraphicsContext (vague because maybe DirectX implementation later?)
*
* This class is very tightly coupled to HWindow
*
* [ ] Copyable
* [ ] Movable
*
**/
class HNativeWindow
{
public:
	HNativeWindow(HGame* game, HWindow* window)
		: m_game{game}
		, m_window{window} { }

	virtual ~HNativeWindow() = default;

	HNativeWindow(const HNativeWindow& other) = delete;
	HNativeWindow& operator= (const HNativeWindow& other) = delete;
	HNativeWindow(HNativeWindow&& other) = delete;
	HNativeWindow& operator= (HNativeWindow&& other) = delete;

	virtual EContextCreationResult CreateWindowContext(unsigned int width, unsigned int height, EScreenMode screenmode) = 0;
	virtual EContextCreationResult CreateGraphicsContext(EGraphicsMode graphicsMode) = 0;

	// TODO: These need to be private if we follow our protection model..

	// This is probably only for windows, if the requested graphic mode is not OpenGL (Direct3D..) it can only be initialized here
	// it's basically the same as HWindow::InitOpenGLParams() but for a specific graphic platform.
	// That also means that this one is probably never called on non Windows OS's
	virtual void InitSpecificGraphicParams(EGraphicsMode graphicsMode, const Vec4f& backgroundClearColor) = 0;

	// Update needs to call the Message handler and fill the vector with queries
	virtual void Update() = 0;

	// Buffer swap maybe
	virtual void PostUpdate() = 0;

	// Must call OS specific exit window command or queue exit in message handler
	virtual void OnQuitRequest() = 0;

	virtual Vec2ui GetRenderResolution() const = 0;
	virtual Vec2ui GetActiveMonitorResolution() const = 0;
	virtual Vec2ui GetWindowLocation() const = 0;

	virtual bool HasFocus() const = 0;

protected:
	HWindow* GetWindow() const;
	HGame* GetGame() const;

private:
	HGame* m_game;
	HWindow* m_window;
};

// Helper function, checks enum to major/minor version
bool HHIsOpenGLVersionSupported(EGraphicsMode graphicsMode, int major, int minor);

// Helper function to translate enum to int pair
std::pair<int, int> HHTranslateOpenGLVersion(EGraphicsMode graphicsMode);

EGraphicsAPI HHGetGraphicsAPI(EGraphicsMode graphicsMode);

} // namespace HE