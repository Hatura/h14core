#include "HWindowEvent.h"

#include <vector>
#include <iostream> // Test

namespace HE {
	EWindowEvent HWindowEvent::GetNextEvent() const {
		if (m_events.size() > 0) {
			EWindowEvent eventCopy = *m_events.begin();
			m_events.erase(m_events.begin());
			return eventCopy;
		}

		return EWindowEvent::None;
	}

	bool HWindowEvent::GetWindowMessageEvent(EWindowMessage message) const {
		return m_windowMessages.find(message) != m_windowMessages.end();
	}

	bool HWindowEvent::IsKeyDown(EKeyboardKey key) const {
		return m_keysDown.find(key) != m_keysDown.end();
	}

	bool HWindowEvent::IsKeyUp(EKeyboardKey key) const {
		//auto it = m_storedKeysDown.find(key);
		//if (it != m_storedKeysDown.end()) {						// Was key in previous Update/Render down?
		//	if (m_keysDown.find(key) == m_keysDown.end()) {		// It was, but is it released this Update/Render?
		//		m_storedKeysDown.erase(it);						// If found erase it from the stored keys
		//		return true;
		//	}
		//}

		//return false;

		return m_keysUp.find(key) != m_keysUp.end();
	}

	void HWindowEvent::PushWindowMessage(EWindowMessage message) {
		m_windowMessages.emplace(message);
		m_events.emplace(EWindowEvent::WindowMessage);
	}

	void HWindowEvent::PushKeyDown(EKeyboardKey key) {
		if (m_storedKeysDown.find(key) == m_storedKeysDown.end()) {
			m_keysDown.emplace(key);
			m_storedKeysDown.emplace(key);
			m_events.emplace(EWindowEvent::Keyboard);
		}
	}

	void HWindowEvent::Clear() {
		m_events.clear();

		m_windowMessages.clear();
		m_keysDown.clear();
		m_keysUp.clear();

		// Store keyboard keys to remove the one's that received up-events afterwards
		std::vector<EKeyboardKey> tempKeys;
		for (const auto& key : m_storedKeysDown) {
			if (!HKeyboard::IsKeyDown(key)) {
				m_keysUp.emplace(key);
				tempKeys.emplace_back(key);
			}
		}

		for (const auto& key : tempKeys) {
			m_storedKeysDown.erase(key);
		}

		// Is there anything qualified for an up event? Then create a keyboard event again
		if (m_keysUp.size() > 0) {
			m_events.emplace(EWindowEvent::Keyboard);
		}

		//std::cout << m_events.size() << " | " << m_keysDown.size() << " | " << m_storedKeysDown.size() << " | " << m_windowMessages.size() << std::endl;
	}
	bool HWindowEvent::HasNextEvent() const {
		return m_events.size() > 0;
	}
} // namespace HE