#include "HNativeWindow.h"

#include "../HWindow.h"

namespace HE {
	HWindow* HNativeWindow::GetWindow() const {
		return m_window;
	}

	HGame* HNativeWindow::GetGame() const {
		return m_game;
	}

	bool HHIsOpenGLVersionSupported(EGraphicsMode graphicsMode, int major, int minor) {
		switch (graphicsMode) {
			case EGraphicsMode::OpenGL30ES:
				return major >= 3 && minor >= 0;
			case EGraphicsMode::OpenGL40:
				return major >= 4 && minor >= 0;
			case EGraphicsMode::OpenGL41:
				return major >= 4 && minor >= 1;
			case EGraphicsMode::OpenGL42:
				return major >= 4 && minor >= 2;
			case EGraphicsMode::OpenGL43:
				return major >= 4 && minor >= 3;
			case EGraphicsMode::OpenGL44:
				return major >= 4 && minor >= 4;
			case EGraphicsMode::OpenGL45:
				return major >= 4 && minor >= 5;
			default:
				return false;
		}
	}

	std::pair<int, int> HHTranslateOpenGLVersion(EGraphicsMode graphicsMode) {
		int major;
		int minor;

		switch (graphicsMode) {
			case EGraphicsMode::OpenGL30ES:
				major = 3;
				minor = 0;
				break;
			case EGraphicsMode::OpenGL40:
				major = 4;
				minor = 0;
				break;
			case EGraphicsMode::OpenGL41:
				major = 4;
				minor = 1;
				break;
			case EGraphicsMode::OpenGL42:
				major = 4;
				minor = 2;
				break;
			case EGraphicsMode::OpenGL43:
				major = 4;
				minor = 3;
				break;
			case EGraphicsMode::OpenGL44:
				major = 4;
				minor = 4;
				break;
			case EGraphicsMode::OpenGL45:
				major = 4;
				minor = 5;
				break;
			default:
				std::terminate(); // Illegal call
		}

		return std::make_pair(major, minor);
	}

	EGraphicsAPI HHGetGraphicsAPI(EGraphicsMode graphicsMode) {
		switch (graphicsMode) {
			case EGraphicsMode::OpenGL30ES:
			case EGraphicsMode::OpenGL40:
			case EGraphicsMode::OpenGL41:
			case EGraphicsMode::OpenGL42:
			case EGraphicsMode::OpenGL43:
			case EGraphicsMode::OpenGL44:
			case EGraphicsMode::OpenGL45:
				return EGraphicsAPI::OpenGL;
			case EGraphicsMode::Direct3D9:
				return EGraphicsAPI::Direct3D9;
			case EGraphicsMode::Direct3D11:
				return EGraphicsAPI::Direct3D11;
			default:
				std::terminate(); // Illegal call
		}
	}
} // namespace HE