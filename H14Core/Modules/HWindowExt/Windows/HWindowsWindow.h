#pragma once

#ifdef _WIN32 // not neccessary, is it?

#include "../HNativeWindow.h"
#include <tchar.h>
#include <windows.h>
#include <memory>

namespace HE {

/**
* HWindowsWindow class
*
* Used to create windows window and graphics context
*
* [ ] Copyable inherited from HAbstractWindowInterface
* [ ] Movable inherited from HAbstractWindowInterface
*
**/
class HWindowsWindow : public HNativeWindow
{
public:
	HWindowsWindow(HGame* game, HWindow* window);
	~HWindowsWindow();

	EContextCreationResult CreateWindowContext(unsigned int width, unsigned int height, EScreenMode screenMode) override;
	EContextCreationResult CreateGraphicsContext(EGraphicsMode graphicsMode) override;
	void InitSpecificGraphicParams(EGraphicsMode graphicsMode, const Vec4f& backgroundColor) override;
	void Update() override;
	void PostUpdate() override;
	void OnQuitRequest() override;

	Vec2ui GetRenderResolution() const override;
	Vec2ui GetActiveMonitorResolution() const override;
	Vec2ui GetWindowLocation() const override;

	bool HasFocus() const override;

	const HWND& GetHandle() const;
private:
	// The windows message handler requires static-ness (no this-pointer), but we just create this as dummy and call the non-staic version through it
	static LRESULT CALLBACK StaticWndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);

	// The actual message handler invoked
	LRESULT WndProc(const HWND& hwnd, UINT msg, const WPARAM& wParam, const LPARAM& lParam);

	// Windows can create differenet graphical contexts (OpenGL / Direct3D..)
	EContextCreationResult CreateOpenGLContext(EGraphicsMode graphicsMode);

	void ParseKeys();

	// These all make extra assignment's neccessary, but it's still cheaper than using heap allocated ptr's
	HWND	m_hwnd;
	MSG		m_msg;
	HGLRC	m_glContext;
};

} // namespace HE

#endif

