#ifdef _WIN32

#include "../../../HGame.h"
#include "HWindowsWindow.h"

// #include <Dbghelp.h>
#include <iostream> // testing
#include <string>
#include "../../../Glew/GL/glew.h"
#include "../../../Glew/GL/wglew.h"

#include "../../HWindow.h"
#include "../../HLogger.h"

namespace HE {

/* stackoverflow.com/questions/5028781/how-to-write-a-sample-code-that-will-crash-and-produce-dump-file
void make_minidump(EXCEPTION_POINTERS* e) {
	auto hDbgHelp = LoadLibraryA("dbghelp");
	if (hDbgHelp == nullptr)
		return;
	auto pMiniDumpWriteDump = (decltype(&MiniDumpWriteDump))GetProcAddress(hDbgHelp, "MiniDumpWriteDump");
	if (pMiniDumpWriteDump == nullptr)
		return;

	char name[MAX_PATH];
	{
		auto nameEnd = name + GetModuleFileNameA(GetModuleHandleA(0), name, MAX_PATH);
		SYSTEMTIME t;
		GetSystemTime(&t);
		wsprintfA(nameEnd - strlen(".exe"),
			"_%4d%02d%02d_%02d%02d%02d.dmp",
			t.wYear, t.wMonth, t.wDay, t.wHour, t.wMinute, t.wSecond);
	}

	auto hFile = CreateFileA(name, GENERIC_WRITE, FILE_SHARE_READ, 0, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
	if (hFile == INVALID_HANDLE_VALUE)
		return;

	MINIDUMP_EXCEPTION_INFORMATION exceptionInfo;
	exceptionInfo.ThreadId = GetCurrentThreadId();
	exceptionInfo.ExceptionPointers = e;
	exceptionInfo.ClientPointers = FALSE;

	auto dumped = pMiniDumpWriteDump(
		GetCurrentProcess(),
		GetCurrentProcessId(),
		hFile,
		MINIDUMP_TYPE(MiniDumpWithIndirectlyReferencedMemory | MiniDumpScanMemory),
		e ? &exceptionInfo : nullptr,
		nullptr,
		nullptr);

	CloseHandle(hFile);

	return;
}

LONG CALLBACK unhandled_handler(EXCEPTION_POINTERS* e) {
	make_minidump(e);
	return EXCEPTION_CONTINUE_SEARCH;
}
*/

HWindowsWindow::HWindowsWindow(HGame* game, HWindow* window)
	: HNativeWindow{game, window}
	, m_hwnd{nullptr}
	, m_msg{}
	, m_glContext{} {

	//SetUnhandledExceptionFilter(unhandled_handler);
}

HWindowsWindow::~HWindowsWindow() {
	wglMakeCurrent(nullptr, nullptr);		// Unbind Context
	wglDeleteContext(m_glContext);			// Is this safe if glContext is not correctly initialized?
	PostQuitMessage(0);						// Posts WM_QUIT to thread's message queue
}

EContextCreationResult HWindowsWindow::CreateWindowContext(unsigned int width, unsigned int height, EScreenMode screenMode) {
	HINSTANCE hInstance = GetModuleHandle(0);

	WNDCLASSEX wndclass{}; // memory init

	auto windowClassName = _T("MyWindow");
	auto windowTitle = _T("Test");

	wndclass.cbSize = sizeof(wndclass);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;// | CS_OWNDC;
	wndclass.lpfnWndProc = StaticWndProc;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance; // does this work from outside? (if this is a DLL..)
	wndclass.hIcon = LoadIcon(hInstance, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(hInstance, IDC_ARROW);
	wndclass.hbrBackground = static_cast<HBRUSH>(GetStockObject(WHITE_BRUSH));
	wndclass.lpszClassName = windowClassName;

	if (!RegisterClassEx(&wndclass)) {
		MessageBox(nullptr, "Couldn't register window", "Error", 0);
		return EContextCreationResult::Unsuccessful;
	}

	// Next figure out the screen settings, this is all a bit fixed and unchangeable right now but we can still put it in methods after we implemented
	// change resolution / change fullscreen methods later

	if (screenMode == EScreenMode::Fullscreen) {
		DEVMODE dmode{};
		dmode.dmSize = sizeof(dmode);
		dmode.dmPelsWidth = width;
		dmode.dmPelsHeight = height;
		dmode.dmBitsPerPel = 32;
		dmode.dmFields = DM_PELSWIDTH | DM_PELSHEIGHT | DM_BITSPERPEL;

		if (ChangeDisplaySettings(&dmode, CDS_FULLSCREEN) != DISP_CHANGE_SUCCESSFUL) {
			// Couldn't create fullscreen, ask user if to continue using windowed mode
			if (MessageBox(NULL, "Fullscreen mode in requested resolution not supported, continue in windowed mode?", "Error", MB_YESNO | MB_ICONERROR) == IDYES) {
				screenMode = EScreenMode::WindowedResizable;		// Set to default windowed so user will recognize the error easier (instead of borderless full
			}
			else {
				return EContextCreationResult::Unsuccessful;
			}
		}
	}
	else if (screenMode == EScreenMode::Borderless) {
		// Overwrite width / height with screenResolution of primary monitor, if this ever gets extended with secondary monitors we need to GetMintorInfo()?
		width = GetSystemMetrics(SM_CXSCREEN);
		height = GetSystemMetrics(SM_CYSCREEN);
	}

	// For window position, only calculated by windowed
	unsigned int windowPosX = 0;
	unsigned int windowPosY = 0;

	// Different window styles for windowed and fullscreen, we need a second if / else block here because fullscreen could've been rejected
	DWORD dwStyle{};

	if (screenMode == EScreenMode::WindowedResizable || screenMode == EScreenMode::WindowedFixed || screenMode == EScreenMode::WindowedFixedNoBorder) {
		if (screenMode == EScreenMode::WindowedResizable) {
			dwStyle = WS_OVERLAPPEDWINDOW | WS_CLIPSIBLINGS | WS_CLIPCHILDREN;
		}
		else if (screenMode == EScreenMode::WindowedFixed) {
			dwStyle = WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX | WS_CLIPSIBLINGS | WS_CLIPCHILDREN;
		}
		else if (screenMode == EScreenMode::WindowedFixedNoBorder) {
			dwStyle = WS_POPUP | WS_CLIPSIBLINGS | WS_CLIPCHILDREN;
		}

		// Again, this is for the primary screen only, centers the window
		windowPosX = GetSystemMetrics(SM_CXSCREEN) / 2 - width / 2;
		windowPosY = GetSystemMetrics(SM_CYSCREEN) / 2 - height / 2;
	}
	else {
		dwStyle = WS_POPUP | WS_CLIPSIBLINGS | WS_CLIPCHILDREN;
	}

	// This is a extra assignment after creation but seriously fuck the windows API
	m_hwnd = CreateWindowExA(
		0,
		windowClassName,
		windowTitle,
		dwStyle,
		windowPosX,								// Pos X
		windowPosY,								// Pos Y
		width,
		height,
		nullptr,
		nullptr,
		hInstance,
		this									// I don't know how exactly this works but it basically stores a pointer to self in hwnd's lpParam
												// We need to do this because this is one of two(?) methods of calling a non-static WndProc later
	);

	if (!m_hwnd) {
		MessageBox(nullptr, "Couldn't call CreateWindow", "Error", 0);
		return EContextCreationResult::Unsuccessful;
	}

	ShowWindow(m_hwnd, 5);
	//ShowWindow(GetConsoleWindow(), SW_HIDE);
	UpdateWindow(m_hwnd);

	return EContextCreationResult::Successful;
}

EContextCreationResult HWindowsWindow::CreateGraphicsContext(EGraphicsMode graphicsMode) {
	EContextCreationResult ccr = EContextCreationResult::Unsuccessful;

	switch (graphicsMode) {
		case EGraphicsMode::OpenGL30ES:
		case EGraphicsMode::OpenGL40:
		case EGraphicsMode::OpenGL41:
		case EGraphicsMode::OpenGL42:
		case EGraphicsMode::OpenGL43:
		case EGraphicsMode::OpenGL44:
		case EGraphicsMode::OpenGL45:
			ccr = CreateOpenGLContext(graphicsMode);
			break;
		case EGraphicsMode::Direct3D11:
			// TODO
			break;
	}

	return ccr;
}

EContextCreationResult HWindowsWindow::CreateOpenGLContext(EGraphicsMode graphicsMode) {
	std::pair<int, int> glVersion = HHTranslateOpenGLVersion(graphicsMode);
	GetGame()->GetModule<HLogger>()->Log(ELogLevel::Info, "Trying to create OpenGL context for Version: ", glVersion.first, ".", glVersion.second);

	PIXELFORMATDESCRIPTOR pfd{
		sizeof(pfd),
		1,																// "Version"
		PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER,		// -> double buffered OpenGL
		PFD_TYPE_RGBA,
		32,																// Colordepth
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,							// Special attributes to color depth, "useless"
		16,																// size of depth buffer?
		0,																// stencil buffer depth
		0, 0, 0, 0, 0, 0
	};

	const HDC& hdc = GetDC(m_hwnd);
	if (hdc == nullptr) {
		MessageBox(nullptr, "Couldn't access the window device context in graphics context creation", "Error", 0);
		return EContextCreationResult::Unsuccessful;
	}

	// Choose pixel format
	int pixelFormatIndex = ChoosePixelFormat(hdc, &pfd);

	// Set the pixel format for the device context
	SetPixelFormat(hdc, pixelFormatIndex, &pfd);

	// Create temporary context, it's suggested in the OpenGL Rendering Context creation, but why?
	// opengl.org/wiki/Tutorial:_OpenGL_3.1_The_First_Triangle_(C%2B%2B/Win)
	HGLRC tempContext = wglCreateContext(hdc);

	if (tempContext == nullptr) {
		GetGame()->GetModule<HLogger>()->Log(ELogLevel::Error, "Couldn't create simple OpenGL context");
		MessageBox(nullptr, "Couldn't create simple OpenGL context", "Error", MB_ICONERROR);
		return EContextCreationResult::Unsuccessful;
	}

	if (!wglMakeCurrent(hdc, tempContext)) {
		GetGame()->GetModule<HLogger>()->Log(ELogLevel::Error, "Couldn't set the simple OpenGL context to current");
		MessageBox(nullptr, "Couldn't set the simple OpenGL context to current", "Error", MB_ICONERROR);
		return EContextCreationResult::Unsuccessful;
	}
		
	glewExperimental = GL_TRUE;
	if (glewInit()) {
		GetGame()->GetModule<HLogger>()->Log(ELogLevel::Error, "Couldn't initialize GLEW");
		MessageBox(nullptr, "Couldn't initialize GLEW", "Error", MB_ICONERROR);
		return EContextCreationResult::Unsuccessful;
	}

	int majorVersion;
	glGetIntegerv(GL_MAJOR_VERSION, &majorVersion);

	int minorVersion;
	glGetIntegerv(GL_MINOR_VERSION, &minorVersion);

	const GLubyte* version = glGetString(GL_VERSION);

	GetGame()->GetModule<HLogger>()->Log(ELogLevel::Info, "Highest OpenGL version supported: ", majorVersion, ".", minorVersion);
	GetGame()->GetModule<HLogger>()->Log(ELogLevel::Info, "Full version name: ", version);
	GetGame()->GetModule<HLogger>()->Log(ELogLevel::Info, "Highest GLSL version supported: ", glGetString(GL_SHADING_LANGUAGE_VERSION));
	GetGame()->GetModule<HLogger>()->Log(ELogLevel::Info, "Vendor: ", glGetString(GL_VENDOR));

	if (!HHIsOpenGLVersionSupported(graphicsMode, majorVersion, minorVersion)) {
		std::string errorString = "Requested OpenGL version is not supported: " + std::to_string(majorVersion) + "." + std::to_string(minorVersion);
		GetGame()->GetModule<HLogger>()->Log(ELogLevel::Error, errorString);
		MessageBox(nullptr, errorString.c_str(), "Error", MB_ICONERROR);
		return EContextCreationResult::Unsuccessful;
	}

	//std::cout << "Detected Extensions: " << std::endl;

	//int numExtensions;
	//glGetIntegerv(GL_NUM_EXTENSIONS, &numExtensions);

	//for (int i = 0; i < numExtensions; ++i) {
	//	std::cout << glGetStringi(GL_EXTENSIONS, i);

	//	if (i + 1 < numExtensions) {
	//		std::cout << ", ";
	//	}
	//}

	std::pair<int, int> openGLVersion = HHTranslateOpenGLVersion(graphicsMode);

	// Prepare the real context
	int attribs[] {
		WGL_CONTEXT_MAJOR_VERSION_ARB, openGLVersion.first,
		WGL_CONTEXT_MINOR_VERSION_ARB, openGLVersion.second,
		0									// Null termination
	};

	// Only checks if context creation extension for this specific method is available, but it should be because we alraedy checked the version prior to this
	//if (!wglewIsSupported("WGL_ARB_create_context")) {
	//	MessageBox(nullptr, "OpenGL 4 not supported", "Error", MB_ICONERROR);
	//	return EContextCreationResult::Unsuccessful;
	//}

	// Create the 'real' context
	m_glContext = wglCreateContextAttribsARB(hdc, nullptr, attribs);

	if (m_glContext == nullptr) {
		GetGame()->GetModule<HLogger>()->Log(ELogLevel::Error, "Couldn't create specific Version OpenGL context");
		MessageBox(nullptr, "Couldn't create specific Version OpenGL context", "Error", MB_ICONERROR);
		return EContextCreationResult::Unsuccessful;
	}

	if (!wglMakeCurrent(nullptr, nullptr)) {
		GetGame()->GetModule<HLogger>()->Log(ELogLevel::Error, "Couldn't unbind the simple OpenGL context");
		MessageBox(nullptr, "Couldn't unbind the simple OpenGL context", "Error", MB_ICONERROR);
		return EContextCreationResult::Unsuccessful;
	}

	if (!wglDeleteContext(tempContext)) {
		GetGame()->GetModule<HLogger>()->Log(ELogLevel::Error, "Couldn't delete the simple OpenGL context");
		MessageBox(nullptr, "Couldn't delete the simple OpenGL context", "Error", MB_ICONERROR);
		return EContextCreationResult::Unsuccessful;
	}

	if (!wglMakeCurrent(hdc, m_glContext)) {
		GetGame()->GetModule<HLogger>()->Log(ELogLevel::Error, "Couldn't set the specific Version OpenGL context to current");
		MessageBox(nullptr, "Couldn't set the specific Version OpenGL context to current", "Error", MB_ICONERROR);
		return EContextCreationResult::Unsuccessful;
	}

	wglSwapIntervalEXT(0);

	//// Set the active Context to the created context
	//if (!wglMakeCurrent(hdc, m_glContext)) {
	//	MessageBox(nullptr, "Couldn't set the rendering context to the created OpenGL context", "Error", 0);
	//	return EContextCreationResult::Unsuccessful;
	//}

	// Needs to be initialized after OpenGL

	return EContextCreationResult::Successful;
}

void HWindowsWindow::InitSpecificGraphicParams(EGraphicsMode graphicsMode, const Vec4f& backgroundColor) {
	// Same as Hwindow::InitOpenGLParams but for Direct3D platforms.

	if (graphicsMode == EGraphicsMode::Direct3D11) {
		// etc
	}
}

void HWindowsWindow::Update() {
	while (PeekMessage(&m_msg, nullptr, 0, 0, PM_REMOVE)) {
		TranslateMessage(&m_msg);
		DispatchMessage(&m_msg);
	}

	ParseKeys();
}

void HWindowsWindow::PostUpdate() {
	const HDC& hdc = GetDC(m_hwnd);
	SwapBuffers(hdc); // Safe
	ReleaseDC(m_hwnd, hdc);
}

void HWindowsWindow::OnQuitRequest() {
	DestroyWindow(m_hwnd);
}

Vec2ui HWindowsWindow::GetRenderResolution() const {
	RECT rect;

	if (GetWindowRect(m_hwnd, &rect)) {
		unsigned int width = rect.right - rect.left;
		unsigned int height = rect.bottom - rect.top;

		return Vec2ui{width, height};
	}
	else {
		GetGame()->RequestQuit(true, "Couldn't determine window resolution on GetResolution()");
		return Vec2ui{0, 0};
	}
}

Vec2ui HWindowsWindow::GetActiveMonitorResolution() const {
	Vec2ui resolution;

	auto monDevice = MonitorFromWindow(m_hwnd, 0);

	MONITORINFOEX monInfo;

	GetMonitorInfo(monDevice, &monInfo);

	resolution[0] = monInfo.rcMonitor.right - monInfo.rcMonitor.left;
	resolution[1] = monInfo.rcMonitor.bottom - monInfo.rcMonitor.top;

	return resolution;
}

Vec2ui HWindowsWindow::GetWindowLocation() const {
	RECT rect;

	GetWindowRect(m_hwnd, &rect);

	return Vec2ui{rect.left, rect.top};
}

bool HWindowsWindow::HasFocus() const {
	return GetActiveWindow() == m_hwnd;
}

LRESULT HWindowsWindow::StaticWndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam) {
	if (msg == WM_NCCREATE) {
		CREATESTRUCT* createstruct = reinterpret_cast<CREATESTRUCT*>(lParam);			// Again, don't know how exactly this works but it basically stores a pointer to self
		LONG_PTR retParam = reinterpret_cast<LONG_PTR>(createstruct->lpCreateParams);	// on window creation which is then received and calls the non-staic WndProc
		SetWindowLongPtr(hwnd, GWLP_USERDATA, retParam); 																					 
	}

	HWindowsWindow* windowsWindow = reinterpret_cast<HWindowsWindow*>(GetWindowLongPtr(hwnd, GWLP_USERDATA)); // Here we retrieve the stored this pointer to call the non-static WndProc

	return windowsWindow->WndProc(hwnd, msg, wParam, lParam);
}

LRESULT HWindowsWindow::WndProc(const HWND& hwnd, UINT msg, const WPARAM& wParam, const LPARAM& lParam) {
	switch (msg) {
		case WM_CLOSE:
			DestroyWindow(hwnd); // calls WM_DESTROY?
			break;
		case WM_DESTROY:
			GetWindow()->PushMessageEvent(EWindowMessage::Quit);
			PostQuitMessage(0); // Calls WM_QUIT?
			break;
		default:
			return DefWindowProc(hwnd, msg, wParam, lParam);
	}

	return 0;
}

void HWindowsWindow::ParseKeys() {
	// We need to get this running for both events and IsKeyDown() call's later, so put it somewhere in a OS specifig Keyboard class

	if (GetAsyncKeyState('A') & 0x8000) { GetWindow()->PushKeyDownEvent(EKeyboardKey::A); }
	if (GetAsyncKeyState('B') & 0x8000) { GetWindow()->PushKeyDownEvent(EKeyboardKey::B); }
	if (GetAsyncKeyState('C') & 0x8000) { GetWindow()->PushKeyDownEvent(EKeyboardKey::C); }
	if (GetAsyncKeyState('D') & 0x8000) { GetWindow()->PushKeyDownEvent(EKeyboardKey::D); }
	if (GetAsyncKeyState('E') & 0x8000) { GetWindow()->PushKeyDownEvent(EKeyboardKey::E); }
	if (GetAsyncKeyState('F') & 0x8000) { GetWindow()->PushKeyDownEvent(EKeyboardKey::F); }
	if (GetAsyncKeyState('G') & 0x8000) { GetWindow()->PushKeyDownEvent(EKeyboardKey::G); }
	if (GetAsyncKeyState('H') & 0x8000) { GetWindow()->PushKeyDownEvent(EKeyboardKey::H); }
	if (GetAsyncKeyState('I') & 0x8000) { GetWindow()->PushKeyDownEvent(EKeyboardKey::I); }
	if (GetAsyncKeyState('J') & 0x8000) { GetWindow()->PushKeyDownEvent(EKeyboardKey::J); }
	if (GetAsyncKeyState('K') & 0x8000) { GetWindow()->PushKeyDownEvent(EKeyboardKey::K); }
	if (GetAsyncKeyState('L') & 0x8000) { GetWindow()->PushKeyDownEvent(EKeyboardKey::L); }
	if (GetAsyncKeyState('M') & 0x8000) { GetWindow()->PushKeyDownEvent(EKeyboardKey::M); }
	if (GetAsyncKeyState('N') & 0x8000) { GetWindow()->PushKeyDownEvent(EKeyboardKey::N); }
	if (GetAsyncKeyState('O') & 0x8000) { GetWindow()->PushKeyDownEvent(EKeyboardKey::O); }
	if (GetAsyncKeyState('P') & 0x8000) { GetWindow()->PushKeyDownEvent(EKeyboardKey::P); }
	if (GetAsyncKeyState('Q') & 0x8000) { GetWindow()->PushKeyDownEvent(EKeyboardKey::Q); }
	if (GetAsyncKeyState('R') & 0x8000) { GetWindow()->PushKeyDownEvent(EKeyboardKey::R); }
	if (GetAsyncKeyState('S') & 0x8000) { GetWindow()->PushKeyDownEvent(EKeyboardKey::S); }
	if (GetAsyncKeyState('T') & 0x8000) { GetWindow()->PushKeyDownEvent(EKeyboardKey::T); }
	if (GetAsyncKeyState('U') & 0x8000) { GetWindow()->PushKeyDownEvent(EKeyboardKey::U); }
	if (GetAsyncKeyState('V') & 0x8000) { GetWindow()->PushKeyDownEvent(EKeyboardKey::V); }
	if (GetAsyncKeyState('W') & 0x8000) { GetWindow()->PushKeyDownEvent(EKeyboardKey::W); }
	if (GetAsyncKeyState('X') & 0x8000) { GetWindow()->PushKeyDownEvent(EKeyboardKey::X); }
	if (GetAsyncKeyState('Y') & 0x8000) { GetWindow()->PushKeyDownEvent(EKeyboardKey::Y); }
	if (GetAsyncKeyState('Z') & 0x8000) { GetWindow()->PushKeyDownEvent(EKeyboardKey::Z); }
	if (GetAsyncKeyState(0x30) & 0x8000) { GetWindow()->PushKeyDownEvent(EKeyboardKey::_0); }
	if (GetAsyncKeyState(0x31) & 0x8000) { GetWindow()->PushKeyDownEvent(EKeyboardKey::_1); }
	if (GetAsyncKeyState(0x32) & 0x8000) { GetWindow()->PushKeyDownEvent(EKeyboardKey::_2); }
	if (GetAsyncKeyState(0x33) & 0x8000) { GetWindow()->PushKeyDownEvent(EKeyboardKey::_3); }
	if (GetAsyncKeyState(0x34) & 0x8000) { GetWindow()->PushKeyDownEvent(EKeyboardKey::_4); }
	if (GetAsyncKeyState(0x35) & 0x8000) { GetWindow()->PushKeyDownEvent(EKeyboardKey::_5); }
	if (GetAsyncKeyState(0x36) & 0x8000) { GetWindow()->PushKeyDownEvent(EKeyboardKey::_6); }
	if (GetAsyncKeyState(0x37) & 0x8000) { GetWindow()->PushKeyDownEvent(EKeyboardKey::_7); }
	if (GetAsyncKeyState(0x38) & 0x8000) { GetWindow()->PushKeyDownEvent(EKeyboardKey::_8); }
	if (GetAsyncKeyState(0x39) & 0x8000) { GetWindow()->PushKeyDownEvent(EKeyboardKey::_9); }
	if (GetAsyncKeyState(VK_F1) & 0x8000) { GetWindow()->PushKeyDownEvent(EKeyboardKey::F1); }
	if (GetAsyncKeyState(VK_F2) & 0x8000) { GetWindow()->PushKeyDownEvent(EKeyboardKey::F2); }
	if (GetAsyncKeyState(VK_F3) & 0x8000) { GetWindow()->PushKeyDownEvent(EKeyboardKey::F3); }
	if (GetAsyncKeyState(VK_F4) & 0x8000) { GetWindow()->PushKeyDownEvent(EKeyboardKey::F4); }
	if (GetAsyncKeyState(VK_F5) & 0x8000) { GetWindow()->PushKeyDownEvent(EKeyboardKey::F5); }
	if (GetAsyncKeyState(VK_F6) & 0x8000) { GetWindow()->PushKeyDownEvent(EKeyboardKey::F6); }
	if (GetAsyncKeyState(VK_F7) & 0x8000) { GetWindow()->PushKeyDownEvent(EKeyboardKey::F7); }
	if (GetAsyncKeyState(VK_F8) & 0x8000) { GetWindow()->PushKeyDownEvent(EKeyboardKey::F8); }
	if (GetAsyncKeyState(VK_F9) & 0x8000) { GetWindow()->PushKeyDownEvent(EKeyboardKey::F9); }
	if (GetAsyncKeyState(VK_F10) & 0x8000) { GetWindow()->PushKeyDownEvent(EKeyboardKey::F10); }
	if (GetAsyncKeyState(VK_F11) & 0x8000) { GetWindow()->PushKeyDownEvent(EKeyboardKey::F11); }
	if (GetAsyncKeyState(VK_F12) & 0x8000) { GetWindow()->PushKeyDownEvent(EKeyboardKey::F12); }
	if (GetAsyncKeyState(VK_ESCAPE) & 0x8000) { GetWindow()->PushKeyDownEvent(EKeyboardKey::ESC); }
	if (GetAsyncKeyState(VK_LCONTROL) & 0x8000) { GetWindow()->PushKeyDownEvent(EKeyboardKey::LSTRG); }
	if (GetAsyncKeyState(VK_RCONTROL) & 0x8000) { GetWindow()->PushKeyDownEvent(EKeyboardKey::RSTRG); }
	if (GetAsyncKeyState(VK_LSHIFT) & 0x8000) { GetWindow()->PushKeyDownEvent(EKeyboardKey::LSHIFT); }
	if (GetAsyncKeyState(VK_RSHIFT) & 0x8000) { GetWindow()->PushKeyDownEvent(EKeyboardKey::RSHIFT); }
	if (GetAsyncKeyState(VK_RETURN) & 0x8000) { GetWindow()->PushKeyDownEvent(EKeyboardKey::RETURN); }
	if (GetAsyncKeyState(VK_TAB) & 0x8000) { GetWindow()->PushKeyDownEvent(EKeyboardKey::TAB); }
}


const HWND& HWindowsWindow::GetHandle() const {
	return m_hwnd;
}

} // namespace HE

#endif
