#pragma once

#include "HModule.h"

#include <memory>
#include <string>
#include <map>

// Possible forward declaration
#include "../Resources/HResource.h"

#include "../HGame.h"
#include "HLogger.h"

namespace HE {

enum class EAllocationResult {
	Successful,
	Unsuccessful
};

enum class EFileNotFoundBehavior {
	Inherit,			// For method paramters
	Quit,
	DefaultThenQuit,
	Nullptr,
	DefaultThenNullptr
};

template<typename T>
class HSharedRes;

class HResourceManager final : public HModule
{
public:
	friend class HGame;

	// Pretty critical
	template<typename T>
	friend class HSharedRes;		// Access to count

	void SetFileNotFoundBehavior(EFileNotFoundBehavior behavior);

	template<typename T, typename U>
	HSharedRes<T> LoadSync(U&& resourceName, EFileNotFoundBehavior fileNotFoundBehavior = EFileNotFoundBehavior::Inherit);

	bool CheckFileExists(const std::string& fileName);

private:
	HResourceManager(HGame* game);

	// Called from HSharedRes destructor, free's the resource in m_resources if possible (no async load attempt or queue)
	void ReportZeroSharedResource(HResource* resource);
	
	//template <typename T>
	//T* CreateResource() {
	//	static_assert(std::is_base_of<HResource, T>::value, "Can only allocate for type HResource");
	//}

	EFileNotFoundBehavior m_fileNotFoundBehavior;

	std::map<std::string, std::unique_ptr<HResource>> m_resources;

	// Queued resources, soon going to be loaded - to prevent multiple loads
	//std::set<std::string> m_queuedResources;
};

template<typename T, typename U>
inline HSharedRes<T> HResourceManager::LoadSync(U&& resourceName, EFileNotFoundBehavior fileNotFoundBehavior)  {
	// So we have to check make_unique if the allocation is successful - whether this is on the GPU memory or the system memory, the constructor
	// of the resource has to tell us if there was success - but for now we jsut do this
	auto it = m_resources.find(resourceName);
	if (it != m_resources.end()) {
		return HSharedRes<T>(this, dynamic_cast<T*>(it->second.get()));
	}
	else {
		// We need to previously create it to determine the file not found behavior (cause we've done it with virtual function calls)
		auto resourceUPtr = std::make_unique<T>();

		if (CheckFileExists(resourceName)) {
			auto itx = m_resources.emplace(resourceName, std::move(resourceUPtr)).first;
			auto resource = itx->second.get();
			auto loadResult = itx->second->Load(std::forward<U>(resourceName));
			if (loadResult.GetResourceLoadResult() == EResourceLoadResult::Error) {
				// This isn't perfect.. what if we don't want to terminate the game just because a file had the wrong extension for example?
				GetGame()->RequestQuit(true, "Couldn't load file: " + std::string(resource->GetResourceName()) + ", Output:\n" + loadResult.GetInfoString());
			}
			else {
				GetGame()->GetModule<HLogger>()->Log(ELogLevel::Debug, "Successfully loaded file ", resource->GetResourceName());
				if (loadResult.HasWarnings()) {
					GetGame()->GetModule<HLogger>()->Log(ELogLevel::Debug, "Warnings for loading process: " + loadResult.GetInfoString());
				}
			}
			return HSharedRes<T>(this, dynamic_cast<T*>(itx->second.get()));
		}
		else {
			if (fileNotFoundBehavior == EFileNotFoundBehavior::Inherit) {
				fileNotFoundBehavior = m_fileNotFoundBehavior;
			}

			if (fileNotFoundBehavior == EFileNotFoundBehavior::Quit) {
				GetGame()->RequestQuit(true, "File not found or inaccessible: " + std::string(resourceName));

				// Pseudo, isn't called because we std::terminate before this
				return HSharedRes<T>(this, nullptr);
			}
			else if (fileNotFoundBehavior == EFileNotFoundBehavior::DefaultThenQuit && resourceUPtr->SupportsDefault()) {
				auto itd = m_resources.find(resourceUPtr->GetDefaultFilename());

				if (itd == m_resources.end()) {
					GetGame()->RequestQuit(true, "File not found or inaccessible: " + std::string(resourceName)
						+ std::string(". Tried to return default instead but couldn't find default resource with name ") + resourceUPtr->GetDefaultFilename() + std::string("!"));

					// Pseudo
					return HSharedRes<T>(this, nullptr);
				}
				else {
					GetGame()->GetModule<HLogger>()->Log(ELogLevel::Warning, "File not found or inaccessible: ", std::forward<U>(resourceName), ", returning default instead.");

					return HSharedRes<T>(this, dynamic_cast<T*>(itd->second.get()));
				}
			}
			else if (fileNotFoundBehavior == EFileNotFoundBehavior::DefaultThenQuit && !resourceUPtr->SupportsDefault()) {
				GetGame()->RequestQuit(true, "File not found or inaccessible: " + std::string(resourceName)
					+ std::string(". Note: EFileNotFoundBehavior was DefaultThenQuit but default not supported by that class!"));

				// Pseudo
				return HSharedRes<T>(this, nullptr);
			}
			else if (fileNotFoundBehavior == EFileNotFoundBehavior::Nullptr) {
				GetGame()->GetModule<HLogger>()->Log(ELogLevel::Warning, "File not found or inaccessible: ", std::forward<U>(resourceName), ", returned nullptr-SharedRes.");

				return HSharedRes<T>(nullptr);
			}
			else if (fileNotFoundBehavior == EFileNotFoundBehavior::DefaultThenNullptr && resourceUPtr->SupportsDefault()) {
				auto itd = m_resources.find(resourceUPtr->GetDefaultFilename());

				if (itd == m_resources.end()) {
					GetGame()->GetModule<HLogger>()->Log(ELogLevel::Warning, "File not found or inaccessible: ", std::forward<U>(resourceName)
						, ". Tried to return default instead but couldn't find default resource with name ", resourceUPtr->GetDefaultFilename(), "!");

					return HSharedRes<T>(nullptr);
				}
				else {
					GetGame()->GetModule<HLogger>()->Log(ELogLevel::Warning, "File not found or inaccessible: ", std::forward<U>(resourceName), ", returning default instead.");

					return HSharedRes<T>(this, dynamic_cast<T*>(itd->second.get()));
				}
			}
			else if (fileNotFoundBehavior == EFileNotFoundBehavior::DefaultThenNullptr && !resourceUPtr->SupportsDefault()) {
				GetGame()->GetModule<HLogger>()->Log(ELogLevel::Warning, "File not found or inaccessible: ", std::forward<U>(resourceName),
					", EFileNotFoundBehavior was DefaultThenNullptr but default is not supported by that class, returning nullptr-SharedRes instead.");

				return HSharedRes<T>(nullptr);
			}
			else {
				// Should never be called
				GetGame()->RequestQuit(true, "HResourceManager::LoadSync() - Illegal EFileNotFoundBehavior");
				return HSharedRes<T>(nullptr);
			}
		}
	}
}

}
