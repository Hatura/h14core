#pragma once

#include "HModule.h"

#include <memory>

#include "../Math/Vec.h"
#include "HWindowExt/HWindowEvent.h"
#include "HWindowExt/HNativeWindow.h"

#ifdef _WIN32
#define NOMINMAX
#include <Windows.h> // Since windef.h has problems on 64bit we can't forward declare HWND need to include the whole windows.h file..
#endif

namespace HE {

class HGame;

/**
* HWindow class
*
* This class represents an Window which contains a handle to the native implementation (Windows, OSX, Linux [If ever implemented..])
*
* Tightly coupled to HNativeWindow
*
* [ ] Copyable
* [x] Movable
*
**/
class HWindow : public HModule
{
public:
	HWindow(const HWindow& other) = delete;
	HWindow& operator= (const HWindow& other) = delete;
	HWindow(HWindow&& other) = default;
	HWindow& operator= (HWindow&& other) = default;

	friend class HGame;				// private Constructor access

	void PushMessageEvent(EWindowMessage message);
	void PushKeyDownEvent(EKeyboardKey key);

	void ClearEvents();

	const HWindowEvent& GetEvents() const;

	unsigned int GetShaderID() const;

	// Forwards to HNativeWindow->GetResolution
	Vec2ui GetRenderResolution() const;
	Vec2ui GetActiveMonitorResolution() const;
	Vec2ui GetWindowLocation() const;

	EScreenMode GetScreenMode() const;

	bool HasFocus() const;
	bool IsWindowClosed() const;

#ifdef _WIN32
	const HWND& GetNativeHandle() const;
#elif __linux__

#endif

private:
	HWindow(HGame* game, unsigned int width, unsigned int height, EScreenMode screenMode, const Vec4f backgroundColor, EGraphicsMode graphicsMode = EGraphicsMode::OpenGL40);

	void InitOpenGLParams();
	void Update();							// Calls generic stuff and the native window counterpart
	void PostUpdate();						// "
	void InternallyHandleWindowMessages();	// Called by update, processes standard messages from the window

	// Processed next frame
	void OnQuitRequest();

	EGraphicsAPI GetGraphicsAPI() const;
	EGraphicsMode GetGraphicsMode() const;

	unsigned int m_windowWidth;
	unsigned int m_windowHeight;
	EScreenMode m_screenMode;
	float m_aspectRatio;
	EGraphicsAPI m_graphicsAPI;
	EGraphicsMode m_graphicsMode;
	Vec4f m_backgroundClearColor;

	bool m_windowShutdown;					// set while calling InternallyHandleWindowMessages();
	bool m_externalQuitRequest;				// set via OnQuitRequest, called via HGame->Quit();

	HWindowEvent m_windowEvents;
	unsigned int m_tempShaderprogramID;

	std::unique_ptr<HNativeWindow> m_nativeWindow;
};

} // namespace HE
