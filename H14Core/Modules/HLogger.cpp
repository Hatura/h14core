#include "HLogger.h"

namespace HE {

HLogger::HLogger(HGame* game, HThreadPool* threadPool) 
	: HModule{game, "HLogger"}
	, m_threadPool{threadPool}
	, m_logOutput{"testout.txt"}
	, m_minLogLevel{ELogLevel::Debug} {

}

void HLogger::SetMinLogLevel(ELogLevel logLevel) {
	m_minLogLevel = logLevel;
}

} // namespace HE