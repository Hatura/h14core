#pragma once

#include "../../Resources/HResource.h"
#include "../HResourceManager.h"

namespace HE {

/**
* HSharedRes class
*
* Holds HResources subtypes and tells HResourceManager when to release them
*
* [ ] Copyable (yet to be implemented)
* [x] Movable
*
**/
template<typename T/*, typename = typename std::enable_if_t<std::is_base_of<HResource, T>::value && !std::is_same<HResource, T>::value>*/>
class HSharedRes final
{
public:
	HSharedRes()
		: m_resourceManager{nullptr}
		, m_resource{nullptr} { }

	HSharedRes(HSharedRes&& other) {
		bool decrUsageCount = false;

		// Cache it
		if (m_resource != nullptr) {
			decrUsageCount = true;
		}

		m_resourceManager = other.m_resourceManager;
		m_resource = other.m_resource;

		other.m_resourceManager = nullptr;
		other.m_resource = nullptr;
	}

	HSharedRes& operator= (HSharedRes&& other) {
		bool decrUsageCount = false;

		// Cache it
		if (m_resource != nullptr) {
			decrUsageCount = true;
		}

		m_resourceManager = other.m_resourceManager;
		m_resource = other.m_resource;

		other.m_resourceManager = nullptr;
		other.m_resource = nullptr;

		return *this;
	}

	~HSharedRes() {
		if (m_resource != nullptr) {
			m_resource->DecreaseUsageCount();

			if (m_resource->GetUsageCount() == 0) {
				m_resourceManager->ReportZeroSharedResource(m_resource);
			}
		}
	}

	friend class HResourceManager;

	bool CheckIntegrity() const {
		return m_resource != nullptr;
	}

	const T* GetResource() const {
		return m_resource;
	}
	
private:
	HSharedRes(HResourceManager* resourceManager, T* resource)
		: m_resourceManager{resourceManager}
		, m_resource{resource} {
		m_resource->IncreaseUsageCount();
	}

	HSharedRes(std::nullptr_t)
		: m_resource{nullptr} {

	}

	// Implement missing methods later

	HSharedRes(const HSharedRes& other) = delete;
	HSharedRes& operator= (const HSharedRes& other) = delete;

private:
	T* m_resource;
	HResourceManager* m_resourceManager;
};

} // namespace HE
