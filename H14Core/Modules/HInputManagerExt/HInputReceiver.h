#pragma once

#include <string>

namespace HE {

/**
* HInputReceiver class
*
* Abstract semi-interface class for our input 3.0 project [...]
*
**/
class HInputReceiver
{
public:

protected:
	virtual void OnKeybindActivate(const std::string& keybindName);
	virtual void OnMouseMove(int amountX, int amountY);

	virtual void OnDDStart() {}
	virtual void OnDDEnd() {}

private:

};

} // namespace HE