#pragma once

#include "HModule.h"

#include <map>
#include <string>
#include <memory>

#include "HMaterialFactoryExt/H2DMaterial.h"

namespace HE {

/**
* HMaterialFactory class
*
* The basic idea behind this and HShaderfactory is:
*
* You create a material via HMaterialFactory (maybe created through lua later) that has
* 1. Options like support lightning, casts shadows, etc.
* 2. has material nodes like color i/o, supports some kiind of AddNode()->LinkOutput
*
**/
class HMaterialFactory : public HModule
{
public:
	friend class HGame;	// Constructor access

	// Create material, maybe later check if materialtype is already present, but that is too much right know and possibly
	// easy to implement as an additional feature, NOT THREAD SAFE RIGHT NOW!
	H2DMaterial* Create2DMaterial(std::string materialName, H2DMaterialPrefs prefs);
	H2DMaterial* Get2DMaterial(const std::string& materialName);

private:
	HMaterialFactory(HGame* game);

	std::map<std::string, std::unique_ptr<H2DMaterial>> m_2DMaterials;
};

} // namespace HE