#pragma once

#include "HModule.h"

#include <fstream>
#include <chrono>
#include <time.h>
#include <string>
#include <sstream>

#include "../Math/Vec.h"
#include "HThreadPool.h"
#include "../HObjectProtector.h"
#include "../Utility/HUtils.h"

namespace HE {

enum class ELogLevel {
	Error,		// Potentially program terminating, if not caught via exception handling		(-> Console and Log)
	Warning,	// Wrong use of API, Unexpected but fixed outcomes (think exception handling)	(-> Console and Log)
	Info,		// Lifecycle events, important events											(-> Console and Log)
	Debug		// Tracking internal issues														(-> Log only)
};

class HLogger : public HModule
{
public:
	friend class HGame;	// Constructor access

	void SetMinLogLevel(ELogLevel logLevel);

	template<typename... Args>
	void Log(ELogLevel logLevel, Args&&... args) {
		std::string logLevelPrefix;
		switch (logLevel) {
			case ELogLevel::Error:
				logLevelPrefix = "[Error]";
				break;
			case ELogLevel::Warning:
				logLevelPrefix = "[Warn]";
				break;
			case ELogLevel::Info:
				logLevelPrefix = "[Info]";
				break;
			default:
			case ELogLevel::Debug:
				logLevelPrefix = "[Debug]";
				break;
		}

		std::string timeString = HUtils::GetFormattedTimeString();

		std::stringstream ss;
		using expander = int[];

		// Forwards arguments, if you get an error here the type is not supported by std::stringstream, maybe implement the missing operator?
		// Source: stackoverflow.com/questions/27375089/what-is-the-easiest-way-to-print-a-variadic-parameter-pack-using-stdostream
		(void) expander {0, (void(ss << std::forward<Args>(args)), 0)...};

		std::string text = std::move(ss.str());

		if (logLevel <= m_minLogLevel) {
			m_threadPool->RegisterTask(ETaskType::SimpleTask, ETaskChannel::Stream, [this, logLevelPrefix(std::move(logLevelPrefix)), timeString(std::move(timeString)), text(std::move(text))]{
				HObjectProtector{this};
				m_logOutput << timeString << " " << logLevelPrefix << "\t" << text << std::endl;

				// And until we have a console..
				std::cout << timeString << " " << logLevelPrefix << "\t" << text << std::endl;
			}, ETaskPriority::Priority);
		}
	}

	// Does force log synchronized, so it gets executed before shutdown is called via terminate or abort
	// Since we have the dump before quit function in HThreadPool we probabyl won't need this anymore?
	//template<typename... Args>
	//void LogCritical(Args&&... args) {
	//	std::string logLevelPrefix = "[Error]";

	//	std::string timeString = HUtils::GetFormattedTimeString();

	//	std::stringstream ss;
	//	using expander = int[];

	//	(void)expander {0, (void(ss << std::forward<Args>(args)), 0)...};

	//	std::string text = std::move(ss.str());

	//	//HObjectProtector{this};
	//	m_logOutput << timeString << " " << logLevelPrefix << "\t" << text << std::endl;
	//	m_logOutput.flush();
	//}

	//// Direct call to Log(ELogLevel::Error, T) but with one extra move
	//template<typename T>
	//void LogError(T&& text) {
	//	Log(ELogLevel::Error, std::forward<T>(text));
	//}

	//// Direct call to Log(ELogLevel::Warning, T) but with one extra move
	//template<typename T>
	//void LogWarning(T&& text) {
	//	Log(ELogLevel::Warning, std::forward<T>(text));
	//}

	//// Direct call to Log(ELogLevel::Info, T) but with one extra move
	//template<typename T>
	//void LogInfo(T&& text) {
	//	Log(ELogLevel::Info, std::forward<T>(text));
	//}

	//// Direct call to Log(ELogLevel::Debug, T) but with one extra move
	//template<typename T>
	//void LogDebug(T&& text) {
	//	Log(ELogLevel::Debug, std::forward<T>(text));
	//}

private:
	HLogger(HGame* game, HThreadPool* threadPool);

	template<typename T>
	void FilteredLog(T&& text) {

	}

	HThreadPool* m_threadPool;

	std::ofstream m_logOutput;

	ELogLevel m_minLogLevel;
};

} // namespace HE
