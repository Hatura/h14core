#include "HMaterialFactory.h"

#include <utility>

namespace HE {

HMaterialFactory::HMaterialFactory(HGame* game)
	: HModule{game, "HMaterialFactory"} {

}

H2DMaterial* HMaterialFactory::Create2DMaterial(std::string materialName, H2DMaterialPrefs prefs) {
	auto uPtr = std::unique_ptr<H2DMaterial>(new H2DMaterial(std::move(prefs)));
	decltype(auto) pair = m_2DMaterials.insert(std::make_pair(std::move(materialName), std::move(uPtr)));

	return pair.first->second.get();
}

H2DMaterial* HMaterialFactory::Get2DMaterial(const std::string& materialName) {
	auto it = m_2DMaterials.find(materialName);

	return it->second.get();
}

} // namespace HE
