#include "../HGame.h"
#include "HRenderer.h"

#include "HWindow.h"

#include "HRendererExt/HCamera.h"

#include "../Components/HSprite.h"

namespace HE {

HRenderer::HRenderer(HGame* game)
	: HModule{game, "HRenderer"}
	, m_defaultCamera{std::unique_ptr<_HCamera>(new _HCamera{this, Rad::FromDegree(45.f), 0.1f, 200.f, Vec3f(0.f, 0.f, 10.f), Vec3f(0.f, 1.0f, 0.f), Rad{0.f}, Rad{0.f}, Rad{0.f}})}
	, m_activeCamera{m_defaultCamera.get()}
	, m_farthestSpritePlane{SpriteComperator_1} {

}

void HRenderer::RegisterSprite(HSprite* sprite, E2DPlane plane) {
	m_farthestSpritePlane.insert(sprite);
}

void HRenderer::UnregisterSprite(HSprite* sprite, E2DPlane plane) {
	m_farthestSpritePlane.erase(sprite);
}

void HRenderer::UnregisterSprite(HSprite* sprite) {
	m_farthestSpritePlane.erase(sprite);
}

void HRenderer::RenderFrame() {
	for (auto* sprite : m_farthestSpritePlane) {
		sprite->Render();
	}
}

std::unique_ptr<_HCamera> HRenderer::CreateCamera(Rad degFOV, float nearDist, float farDist, Vec3f eye, Vec3f upAxis, Rad heading, Rad attitude, Rad bank) const {
	auto camUPtr = std::unique_ptr<_HCamera>{new _HCamera{this, std::move(degFOV), nearDist, farDist, std::move(eye), std::move(upAxis), std::move(heading), std::move(attitude), std::move(bank)}};

	return std::move(camUPtr);
}

void HRenderer::SetActiveCamera(_HCamera* camera) {
	m_activeCamera = camera;
}

void HRenderer::UnregisterCamera(_HCamera* newCamera) {
	if (newCamera == nullptr) {
		m_activeCamera = m_defaultCamera.get();
	}
	else {
		m_activeCamera = newCamera;
	}
}

Vec2ui HRenderer::GetRenderResolution() const {
	return GetGame()->GetModule<HWindow>()->GetRenderResolution();
}

const _HCamera* HRenderer::GetActiveCamera() const {
	return m_activeCamera;
}

_HCamera* HRenderer::GetActiveCamera() {
	return m_activeCamera;
}

bool HRenderer::SpriteComperator_1(const HSprite* lhs, const HSprite* rhs) {
	return lhs->GetID() > rhs->GetID();
}

} // namespace HE