#include "../HGame.h"
#include "HComponentFactory.h"

namespace HE {

//HActor* HFactory::InitActor(std::unique_ptr<HActor> actorUPtr) {
//	return 
//}

std::map<std::string, std::function<HActor*()>> HComponentFactory::m_actorTypeMap;

HActor* HComponentFactory::ConstructActorByName(const std::string& name) {
	auto it = m_actorTypeMap.find(name);

	if (it == m_actorTypeMap.end()) {
		GetGame()->RequestQuit(true, "Tried to actor with unknown name: " + name + ", remember to register custom non virtual Actors before creating them via string");
	}

	// Calls the construction process
	return it->second();
}

HComponentFactory::HComponentFactory(HGame* game)
	: HModule{game, "HComponentFactory"} {

}

} // namespace HE
