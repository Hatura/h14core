#pragma once

#include "MaterialNodes/HMaterialNode.h"

namespace HE {

/**
* H2DMaterialPrefs struct
*
* POD, use () or {} to default init
*
**/
struct H2DMaterialPrefs {
	bool m_supportsLightning;
	bool m_castsShadows;
	bool m_receivesShadows;
};

/**
* H2DMaterial class
*
* [x] Copyable
* [x] Movable
*
**/
class H2DMaterial
{
public:
	friend class HMaterialFactory;

	const H2DMaterialPrefs& GetMaterialPrefs() const;

	//void SetEntryNode(HMaterialNode* node);

private:
	H2DMaterial(H2DMaterialPrefs prefs);

	H2DMaterialPrefs m_materialPrefs;
};

} // namespace HD

