#include "H2DMaterial.h"

#include <utility>

namespace HE {

H2DMaterial::H2DMaterial(H2DMaterialPrefs prefs) {
	m_materialPrefs = std::move(prefs);
}

const H2DMaterialPrefs& H2DMaterial::GetMaterialPrefs() const {
	return m_materialPrefs;
}

}