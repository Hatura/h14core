#pragma once

#include "../HObject.h"

namespace HE {

enum class EModuleType {
	Mandatory,
	Optional
};

class HGame;

/**
* HModule class
*
* A abstract base class for modules (Factories) that are accessed via HGame
*
* [ ] Copyable
* [ ] Movable
*
**/
class HModule : public HObject
{
public:
	virtual ~HModule() = 0;

	HModule(const HModule&) = delete;
	HModule& operator= (const HModule&) = delete;
	HModule(HModule&&) = delete;
	HModule& operator= (HModule&&) = delete;

	//bool operator== (const HModule& rhs) {
	//	return true;
	//}

	friend class HGame;

	EModuleType GetModuleType() const {
		return m_moduleType;
	}

protected:
	HModule(HGame* game, std::string moduleName);
	
	void SetModuleType(EModuleType moduleType);

private:

	EModuleType m_moduleType;
	std::string m_moduleName;
};

}
