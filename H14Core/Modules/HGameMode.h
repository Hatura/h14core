#pragma once

#include "HModule.h"

#include <deque>
#include <memory>

#include "HActorManager.h"

namespace HE {

class HPlayerController;

/**
* HGameMode class
*
* GameMode is a class that handles rules for the game, updates Actors/Components, Player, Network and AI Controllers.
*
* [ ] Copyable (inherited from HModule)
* [ ] Movable (inherited from HModule
*
**/
class HGameMode : public HModule
{
public:
	friend class HGame; // Constructor access

	// Create a PlayerController for playerID player
	HPlayerController* CreatePlayerController(unsigned int playerID);

	HActorManager* GetActorManager() const;

private:
	HGameMode(HGame* game, std::string gameModeName);

	void Update();

	std::string m_gameModeName;

	std::unique_ptr<HActorManager> m_actorManager;

	std::deque<std::unique_ptr<HPlayerController>> m_localPlayerControllers;
};

} // namespace HE
