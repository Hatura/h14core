#include "../HGame.h"
#include "HModule.h"

#include <typeinfo>

#include "../HGame.h"
#include "HLogger.h"

namespace HE {

HModule::HModule(HGame* game, std::string moduleName) 
	: m_moduleType{EModuleType::Mandatory}
	, m_moduleName{std::move(moduleName)} {

	SetGame(game);

	auto* logger = GetGame()->GetModule<HLogger>();

	if (logger != nullptr) {
		logger->Log(ELogLevel::Info, "Initializing ", m_moduleName, " Module");
	}
}

HModule::~HModule() { }

void HModule::SetModuleType(EModuleType moduleType) {
	m_moduleType = moduleType;
}

}
