#pragma once

#include <memory>
#include <map>
#include <functional>
#include "HModule.h"
#include "../HGame.h"
#include "../Modules/HGameMode.h"

namespace HE {

class HComponent;
class HActor;
class HGame;

//class HDummyFactory {
//public:
//	using mapType = std::map<std::string, HActor*(*)()>;
//
//	static HActor* GetDummy(const std::string& name) {
//		mapType::iterator it = GetMap()->find(name);
//		return it->second();
//	}
//
//protected:
//	static mapType* GetMap() {
//		if (!m_mapInit) {
//			m_map = std::make_unique<mapType>();
//			m_mapInit = true;
//		}
//
//		return m_map.get();
//	}
//
//private:
//	static std::unique_ptr<mapType> m_map;
//	static bool m_mapInit;
//};
//
//template<typename T>
//class HDummyRegister : HDummyFactory {
//	HDummyRegister(const std::string& name) {
//		GetMap()->insert(std::make_pair(name, &createDummy<T>));
//	}
//};
//
//#define REGISTER_DEC_DUMMY(NAME) \
//    static HDummyRegister<NAME> reg
//
//#define REGISTER_DEF_DUMMY(NAME) \
//    HDummyRegister<NAME> NAME::reg(#NAME)

/**
* HFactory class
*
* Used to initiate specific component constructors, which can be in the need of only simple forwarding (HGame ptr) or
* more complex one's (registering Sprite or other Stuff to resource managers, loading async, etc.)
*
* [ ] Copyable (inherited from HModule)
* [ ] Movable (inherited from HModule)
*
**/
class HComponentFactory : public HModule
{
public:
	friend class HGame;	// Constructor access

	template<typename T, typename... Args>
	T* ConstructActor(Args&&... args);

	template<typename T, typename... Args>
	std::unique_ptr<T> ConstructComponent(Args&&... args);

	HActor* ConstructActorByName(const std::string& name);

	// A function pointer to a templatized function of this is stored by calling RegisterActorType<T>(...), this function
	// calls the construction/registration process of the entity (no ctor arguments possible?) which is itself invoked by calling
	// ConstructActorByName(name) so we can have a dynamic init process through a config file
	// This function should NEVER be called in any other way! Don't use this function (we can't hide it, because we would have to inject friend access to a user
	// created class into this class to make it private.. Use ConstructActor() or CONstructActorByName() instead!
	template<typename T>
	HActor* XConstructActorFromPredef();

	template<typename T>
	static void RegisterActorTypeF(const std::string& name, std::function<HActor*()> retFunc);

private:
	HComponentFactory(HGame* game);

	// Since we can't have the construct from predef have paramters (or can we) we need to do it this ugly way.. calls T::SetName(componentName) on the Component
	template<typename T>
	static void InjectComponentName(std::string componentName);

	// Called from within (at the end of) ConstructComponent() to specialize post construction options, TODO: Can we changed this to HComponent class internal?
	template<typename T>
	void ConfigureComponent(T* component) {}

	static std::map<std::string, std::function<HActor*()>> m_actorTypeMap;
};

#define RegisterActorType(GAMEINSTANCE, NAME) \
	HComponentFactory::RegisterActorTypeF<NAME>(#NAME, [this] () -> HActor* { return GAMEINSTANCE->GetModule<HComponentFactory>()->XConstructActorFromPredef<NAME>(); });

#include "../HGame.h"

template<typename T, typename... Args>
inline T* HComponentFactory::ConstructActor(Args&&... args) {
	auto uPtr = std::unique_ptr<T>(new T(std::forward<Args>(args)...));

	uPtr->SetGame(GetGame());

	//auto tpClientUPtr = std::unique_ptr<HTPClient>(new HTPClient(GetGame()->GetModule<HThreadPool>()));
	//uPtr->SetTPClient(std::move(tpClientUPtr));

	uPtr->PostConstructInit();

	auto ptr = GetGame()->GetModule<HGameMode>()->GetActorManager()->RegisterActor(std::move(uPtr));

	return dynamic_cast<T*>(ptr);
}

template<typename T>
inline HActor* HComponentFactory::XConstructActorFromPredef() {
	auto uPtr = std::unique_ptr<T>(new T());
	uPtr->SetGame(GetGame());
	uPtr->PostConstructInit();

	// Now pass the params to a FileInit function which then configures the Actor from within, maybe with a bool callback to signal success
	//uPtr->FileInit(params)

	auto ptr = GetGame()->GetModule<HGameMode>()->GetActorManager()->RegisterActor(std::move(uPtr));

	return dynamic_cast<T*>(ptr);
}

template<typename T, typename... Args>
inline std::unique_ptr<T> HComponentFactory::ConstructComponent(Args&&... args) {
	static_assert(std::is_base_of<HComponent, T>::value, "Can only construct types derived from HComponent!");
	static_assert(!std::is_base_of<HActor, T>::value, "Use ConstructActor() for HActor!");

	auto uPtr = std::unique_ptr<T>(new T(std::forward<Args>(args)...));

	uPtr->SetGame(GetGame());

	//auto tpClientUPtr = std::unique_ptr<HTPClient>(new HTPClient(GetGame()->GetModule<HThreadPool>()));
	//uPtr->SetTPClient(std::move(tpClientUPtr));

	uPtr->PostConstructInit();

	ConfigureComponent(uPtr.get());

	return uPtr;
}

template<typename T>
inline void HComponentFactory::RegisterActorTypeF(const std::string& name, std::function<HActor*()> retFunc) {
	m_actorTypeMap.insert(std::make_pair(name, retFunc));
}

} // namespace HE
