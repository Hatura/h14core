#include "../HGame.h"
#include "HWindow.h"

#include <iostream> // test
#include "../Glew/GL/glew.h"

#include "../Math/Math.h"
#include "../glm/mat4x4.hpp"
#include "../glm/vec3.hpp"
#include "../glm/gtc/matrix_transform.hpp"

#ifdef _WIN32
#include <gl/GL.h>
#elif __linux__
#include <GL/gl.h>
#endif

#ifdef _WIN32
#include "HWindowExt/Windows/HWindowsWindow.h"
#elif __linux__
#include "HWindowExt/Linux/HLinuxWindow.h"
#endif

#include "HLogger.h"

namespace HE {

	const char* simpleVertexShader {
		"#version 400\n"
		"uniform mat4 mvp;\n"
		"layout(location = 0) in vec2 position;"
		"layout(location = 1) in vec2 texCoordUV;"
		"out vec2 texCoord;"
		"void main() {"
		"	gl_Position = mvp * vec4(position, 0.0, 1.0);"
		"	texCoord = texCoordUV;"
		"}"
	};

	const char* simpleFragmentShader{
		"#version 400\n"
		"precision mediump float;"
		"in vec2 texCoord;"
		"out vec4 outColor;"
		"uniform sampler2D textureSampler;"
		"void main() {"
		"	outColor = texture(textureSampler, texCoord).rgba;"
		"}"
	};

HWindow::HWindow(HGame* game, unsigned int width, unsigned int height, EScreenMode screenMode, const Vec4f backgroundColor, EGraphicsMode graphicsMode)
	: HModule{game, "HWindow"}
	, m_windowWidth{width}
	, m_windowHeight{height}
	, m_backgroundClearColor{std::move(backgroundColor)}
	, m_screenMode{screenMode}
	, m_aspectRatio{static_cast<float>(width) / static_cast<float>(height)}
	, m_graphicsAPI{HHGetGraphicsAPI(graphicsMode)}
	, m_graphicsMode{graphicsMode}
	, m_windowShutdown{false}
	, m_externalQuitRequest{false} {
#ifdef _WIN32
	m_nativeWindow = std::make_unique<HWindowsWindow>(GetGame(), this);
#elif __linux__
	m_nativeWindow = std::make_unique<HLinuxWindow>(GetGame(), this);
#endif

	// If we can't create the window or the graphics context it's an unrecoverable error, terminate the program execution
	if (m_nativeWindow->CreateWindowContext(m_windowWidth, m_windowHeight, screenMode) == EContextCreationResult::Unsuccessful) {
		std::terminate(); // Terminate can still cause the set_terminate() set handler to call, so it's better than abort
	}

	if (m_nativeWindow->CreateGraphicsContext(m_graphicsMode) == EContextCreationResult::Unsuccessful) {
		std::terminate();
	}

	switch (m_graphicsMode) {
		case EGraphicsMode::OpenGL30ES:
		case EGraphicsMode::OpenGL40:
		case EGraphicsMode::OpenGL41:
		case EGraphicsMode::OpenGL42:
		case EGraphicsMode::OpenGL43:
		case EGraphicsMode::OpenGL44:
		case EGraphicsMode::OpenGL45:
			InitOpenGLParams();
			break;
		case EGraphicsMode::Direct3D11:
			// If it's anything other than OpenGL (which is default supported) it needs to be initialized in the OS defined class
			m_nativeWindow->InitSpecificGraphicParams(m_graphicsMode, backgroundColor);
			break;
	}
}

void HWindow::InitOpenGLParams() {
	//glViewport(0, 0, m_windowWidth, m_windowHeight);

	//glMatrixMode(GL_PROJECTION);

	//glLoadIdentity();

	//gluPerspective(45.0f, m_aspectRatio, 0.f, 255.f);
	//
	//glMatrixMode(GL_MODELVIEW);

	//glLoadIdentity();
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);

	GLuint vertexShaderID = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShaderID, 1, &simpleVertexShader, nullptr);
	glCompileShader(vertexShaderID);

	int bufferLength;
	glGetShaderiv(vertexShaderID, GL_INFO_LOG_LENGTH, &bufferLength);
	if (bufferLength > 1) {
		GLchar* logString = new char[bufferLength + 1];
		glGetShaderInfoLog(vertexShaderID, bufferLength, nullptr, logString);
		std::cout << "Error compiling vertex Shader: " << std::endl << logString << std::endl;

		delete[] logString;
	}

	GLuint fragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShaderID, 1, &simpleFragmentShader, nullptr);
	glCompileShader(fragmentShaderID);

	glGetShaderiv(vertexShaderID, GL_INFO_LOG_LENGTH, &bufferLength);
	if (bufferLength > 1) {
		GLchar* logString = new char[bufferLength + 1];
		glGetShaderInfoLog(fragmentShaderID, bufferLength, nullptr, logString);
		std::cout << "Error compiling fragment Shader: " << std::endl << logString << std::endl;

		delete[] logString;
	}

	GLuint shaderProgramID = glCreateProgram();
	glAttachShader(shaderProgramID, fragmentShaderID);
	glAttachShader(shaderProgramID, vertexShaderID);
	glLinkProgram(shaderProgramID);

	m_tempShaderprogramID = shaderProgramID;
}

void HWindow::Update() {
	ClearEvents();						// Clear previously stored Window Events

	m_nativeWindow->Update();			// Do native window specific updates, also stores new Window Events

	if (m_externalQuitRequest) {
		m_nativeWindow->OnQuitRequest();
	}

	InternallyHandleWindowMessages();	// Handles standard Window Events internally, such as resize

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glClearColor(
		m_backgroundClearColor.Get(0),
		m_backgroundClearColor.Get(1),
		m_backgroundClearColor.Get(2),
		m_backgroundClearColor.Get(3)
	);

	glUseProgram(m_tempShaderprogramID);

/*
	GLuint matrixID = glGetUniformLocation(m_tempShaderprogramID, "mvp");

	glm::mat4 projectionMat = glm::perspective(glm::radians(45.0f), static_cast<float>(m_windowWidth) / static_cast<float>(m_windowHeight), 0.1f, 100.f);

	glm::mat4 viewMat = glm::lookAt(
		glm::vec3(4, 3, 3),
		glm::vec3(0, 0, 0),
		glm::vec3(0, 1, 0)
	);

	glm::mat4 modelMat = glm::mat4(1.0f);

	glm::mat4 mvp = projectionMat * viewMat;

	Vec3f vec1{0.f, 0.f, 0.f};
	Vec3f vec2{1.f, 1.f, 1.f};
	GetGame()->GetModule<HLogger>()->Log(ELogLevel::Debug, "Distance of vecs: ", VecDistance(vec1, vec2));

	Mat44 mat;
	mat.InitIdentity();
	mat.Translate(0, 0.5f, 0.0f);

	auto matPerspective = FCreatePerspectiveMat(FDegreeToRad(45.f), static_cast<float>(m_windowWidth) / static_cast<float>(m_windowHeight), 0.1f, 100.f);

	auto matLookAt = FCreateLookAtMat(
		Vec3f{4.f, 3.f, 3.f},
		Vec3f{0.f, 0.f, 0.f},
		Vec3f{0.f, 1.f, 0.f}
	);

	auto matModel = Mat44{};
	matModel.InitIdentity();

	auto matMVP = matModel * matLookAt * matPerspective;

	auto matDebug = FCreateDebugMat();
	auto matDebug2 = FCreateDebugMat();

	auto matDResult = matDebug * matDebug2;

	glUniformMatrix4fv(matrixID, 1, GL_FALSE, &matMVP[0][0]);
*/
}

void HWindow::OnQuitRequest() {
	m_externalQuitRequest = true;
}

void HWindow::PostUpdate() {
	m_nativeWindow->PostUpdate();
}

void HWindow::InternallyHandleWindowMessages() {
	if (m_windowEvents.GetWindowMessageEvent(EWindowMessage::Quit)) {
		m_windowShutdown = true;
	}
}

void HWindow::PushMessageEvent(EWindowMessage message) {
	m_windowEvents.PushWindowMessage(message);
}

void HWindow::PushKeyDownEvent(EKeyboardKey key) {
	m_windowEvents.PushKeyDown(key);
}

const HWindowEvent& HWindow::GetEvents() const {
	return m_windowEvents;
}

void HWindow::ClearEvents() {
	m_windowEvents.Clear();
}

bool HWindow::HasFocus() const {
	return m_nativeWindow->HasFocus();
}

bool HWindow::IsWindowClosed() const {
	return m_windowShutdown;
}

EGraphicsAPI HWindow::GetGraphicsAPI() const {
	return m_graphicsAPI;
}

EGraphicsMode HWindow::GetGraphicsMode() const {
	return m_graphicsMode;
}


unsigned int HWindow::GetShaderID() const {
	return m_tempShaderprogramID;
}

Vec2ui HWindow::GetRenderResolution() const {
	return m_nativeWindow->GetRenderResolution();
}

Vec2ui HWindow::GetActiveMonitorResolution() const {
	return m_nativeWindow->GetActiveMonitorResolution();
}

Vec2ui HWindow::GetWindowLocation() const {
	return m_nativeWindow->GetWindowLocation();
}

EScreenMode HWindow::GetScreenMode() const {
	return m_screenMode;
}

#ifdef _WIN32
const HWND& HWindow::GetNativeHandle() const {
	return dynamic_cast<HWindowsWindow*>(m_nativeWindow.get())->GetHandle();
}
#elif __linux__

#endif
} // namespace HE
