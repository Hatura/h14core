#include "HTask.h"

namespace HE {

//HTask::HTask() {
//
//}

HTask::HTask(ETaskType taskType, ETaskChannel taskPriority, std::function<void()>&& task, ETaskPriority streamTaskPriority)
	: m_taskType{taskType}
	, m_taskChannel{taskPriority}
	, m_taskPriority{streamTaskPriority}
	, m_task{std::move(task)} { }

HTask::~HTask() { }

ETaskType HTask::GetTaskType() const {
	return m_taskType;
}

ETaskChannel HTask::GetTaskChannel() const {
	return m_taskChannel;
}

ETaskPriority HTask::GetTaskPriority() const {
	return m_taskPriority;
}

void HTask::ExecuteTask() {
	m_task();
}

} // namespace HE
