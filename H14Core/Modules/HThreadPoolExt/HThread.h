#pragma once

#include <string>
#include <memory>
#include <thread>
#include <mutex>
#include <atomic>
#include <condition_variable>
#include <iostream> // test

namespace HE {

struct SThreadType_Stream {};
struct SThreadType_Default {};

enum EThreadType {
	StreamPriority,
	Default
};

class HThreadPool;

class HThread
{
public:
	HThread(HThreadPool* threadPool, EThreadType threadType, std::string threadName);

	HThread(const HThread&) = delete;
	HThread& operator= (const HThread&) = delete;
	HThread(HThread&&) = default;
	HThread& operator= (HThread&&) = default;

	// Threadsafe, set's m_shutdown to true (making it exiting the workqueue after job finish or next condition var notify (which is called after this from HThreadPool)
	void RequestShutdown();

	// Calls thread->join(), called from HThreadPool (not from within the thread!)
	void JoinPool();

	// Threadsafe, copy of atomic bool
	bool IsWorking() const;

	const std::string& GetCurrentTaskDesc() const;

private:
	// Template specialization would be cheaper, but then again we start the thread just one time (beginning), also it get's hella messy with circular dependencies because
	// This header would need a HTreadPool.h include (while HThreadPool.h includes HThread.h..)
	void RunThread(SThreadType_Default);
	void RunThread(SThreadType_Stream);

	HThreadPool* m_threadPool;
	std::string m_threadName;
	EThreadType m_threadType;

	std::string m_currentTaskDesc;

	std::unique_ptr<std::thread> m_thread;
	std::mutex m_threadMutex;
	std::atomic<bool> m_shutdown;
	std::atomic<bool> m_working;
};

}