#pragma once

#include <functional>

namespace HE {

enum class ETaskType {
	SimpleTask,
	TimedTask	
};

enum class ETaskChannel { // High necessary?
	Stream,
	Priority	
};

// Priority means
// On streamed tasks:
// - ::Priority = Get forced out on game Quit to complete the log
//
// On normal tasks (not implemented yet):
// - ::Priority = Get forced out on process and yield
// - ::Normal = For timed tasks and [TODO], only get forced out if certain conditions are met
//
// On inserting a task in the Threadpool they get ordered repecting their priority (this way we can avoid iterators which are not really a good tool for multithreading)
// It always gets ordered Highest to lowest.
enum class ETaskPriority {
	Priority = 3,
	Normal = 2,
	Timed = 1
};

/**
* HTask class
*
* A HTask is a complex task for the HThreadPool class that can have different priorities. A low priority task for example gets executed at undefined time, when there is leftover computing power,
* a FramePerfect priority task has to get executed before the next time the specific task holder gets Updated (clarify later..)
*
* [ ] Copyable (Maybe we should make them copyable)
* [x] Movable
* 
**/
class HTask
{
public:
	//HTask(); // empty task ctor for HThread
	HTask(ETaskType taskType, ETaskChannel taskPriority, std::function<void()>&& task, ETaskPriority streamTaskPriority);

	virtual ~HTask() = 0;

	HTask(const HTask&) = delete;
	HTask& operator= (const HTask&) = delete;
	HTask(HTask&&) = default;
	HTask& operator= (HTask&&) = default;

	void ExecuteTask();

	ETaskType GetTaskType() const;
	ETaskChannel GetTaskChannel() const;
	ETaskPriority GetTaskPriority() const;

private:
	ETaskType m_taskType;
	ETaskChannel m_taskChannel;
	ETaskPriority m_taskPriority;	// that's pretty lazy but whats the alternative, multiple inheritance?

	std::function<void()> m_task;
};

} // namespace HE
