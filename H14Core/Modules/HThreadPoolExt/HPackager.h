#pragma once

#include <deque>
#include <vector>
#include <functional>
#include <iostream>
#include <atomic>
#include <mutex>
#include <memory>

namespace HE {

class HPackager
{
public:
	HPackager();

	// ContainerType has to be of type std::unique_ptr or std::shared_ptr!
	template <typename T, typename ContainerType>
	static auto PackTask(const ContainerType& ptrVecInit, std::function<void(T*)>&& toInvokeFunctionInit, unsigned int availableCores, unsigned int tasksPerCore) {
		auto numElements = static_cast<unsigned int>(ptrVecInit.size());
		unsigned int numPackagesMax = availableCores * tasksPerCore;

		unsigned int numPackages = 0;
		if (numElements >= numPackagesMax) {
			numPackages = numPackagesMax;
		}
		else {
			numPackages = numElements;
		}

		unsigned int tasksPerPackage = numElements / numPackages;
		
		// If there is a 'rest' we need an additional task for the rest
		//if (numElements % tasksPerPackage != 0) {
		//	++numPackages;
		//}

		// Heap allocated, the last invoked packaged function deletes it (calls counted via atomic counter)
		PackageInfo<T, ContainerType>* packageInfo = new PackageInfo<T, ContainerType>(ptrVecInit, numPackages);
		packageInfo->m_function = std::make_unique<std::function<void(T*)>>(std::move(toInvokeFunctionInit));

		//std::cout << "Elements in ptrVec: " << numElements << std::endl;
		//std::cout << "Num packages: " << numPackages << std::endl;
		//std::cout << "Tasks per package: " << tasksPerPackage << std::endl;

		std::vector<std::function<void()>> funcs;

		for (unsigned int i = 0; i < numPackages; ++i) {
			unsigned int start = i * tasksPerPackage;
			unsigned int limit = (i + 1) * tasksPerPackage;

			if (i == numPackages - 1) {
				limit = (i * tasksPerPackage) + (numElements - (i * tasksPerPackage));
			}

			funcs.emplace_back([packageInfo(packageInfo), index(start), limit(limit)]() mutable {
				for (; index < limit; ++index) {
					(*packageInfo->m_function)(packageInfo->m_ptrVec[index].get());
				}

				unsigned int currentValue = --packageInfo->m_processedCounter; // std::memory_order_relaxed possible?

				if (currentValue == 0) {
					delete packageInfo;
				}
			});
		}

		return funcs;
	}

private:
	static const unsigned int m_tasksPerPackage = 20;

	template<typename T, typename ContainerType>
	class PackageInfo {
	public:
		PackageInfo(const ContainerType& ptrVec, unsigned int numPackages)
			: m_ptrVec{ptrVec}
			, m_processedCounter{numPackages} {}

		PackageInfo(const PackageInfo&) = delete;
		PackageInfo& operator= (const PackageInfo&) = delete;
		PackageInfo(PackageInfo&&) = delete;
		PackageInfo& operator= (PackageInfo&&) = delete;

		const ContainerType& m_ptrVec;
		std::unique_ptr<std::function<void(T*)>> m_function;
		std::atomic<unsigned int> m_processedCounter;
	};
};

}