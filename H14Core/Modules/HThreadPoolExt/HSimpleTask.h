#pragma once

#include "HTask.h"

namespace HE {

class HSimpleTask : public HTask
{
public:
	HSimpleTask(ETaskChannel taskPriority, std::function<void()>&& task, ETaskPriority streamTaskPriority = ETaskPriority::Normal);
};

} // namespace HE