#include "HThread.h"

#include <iostream> // test

#include "../HThreadPool.h"

namespace HE {

HThread::HThread(HThreadPool* threadPool, EThreadType threadType, std::string threadName)
	: m_threadPool{threadPool}
	, m_threadType{threadType}
	, m_threadName{std::move(threadName)}
	, m_thread{nullptr}
	, m_working{false}
	, m_shutdown{false} {

	m_thread = std::make_unique<std::thread>([this] () {
		if (m_threadType == EThreadType::StreamPriority) {
			std::cout << "Started " << m_threadName << " as Stream-Thread" << std::endl;
			RunThread(SThreadType_Stream{});
		}
		else if (m_threadType == EThreadType::Default) {
			std::cout << "Started " << m_threadName << " as Default-Thread" << std::endl;
			RunThread(SThreadType_Default{});
		}		

		//while (true) {
		//	std::unique_lock<std::mutex> lk(m_threadMutex);

		//	m_threadPool->m_workNotifierCondition.wait(lk, [this] {
		//		return m_shutdown || !m_threadPool->m_fetchPriorityTasks.empty();
		//	});
		//}
	});
}

void HThread::JoinPool() {
	m_thread->join();
}

bool HThread::IsWorking() const {
	return m_working.load();
}

const std::string& HThread::GetCurrentTaskDesc() const {
	return m_currentTaskDesc;
}

void HThread::RunThread(SThreadType_Default) {
	std::unique_ptr<HTask> task;

	while (true) {
		std::unique_lock<std::mutex> lk(m_threadMutex);

		m_threadPool->m_defaultWorkCV.wait(lk, [this] {
			m_threadPool->m_defaultPriorityTasksMutex.lock();

			auto doWork = m_shutdown || !m_threadPool->m_defaultPriorityTasks.empty();

			if (!doWork) {
				m_working = false;
				m_threadPool->m_defaultPriorityTasksMutex.unlock();
			}
			else {
				m_working = true;
			}

			return doWork;
		});

		if (m_shutdown) {
			m_threadPool->m_defaultPriorityTasksMutex.unlock();
			return; // Stop thread
		}

		// More elegant would be a continous mutex but it's pretty complex to set up due to the condition on the wait different release algorithm, 
		// this should work too because notify_one should only wake one

		// Check again, because we re-acquired the mutex..

		task = std::move(m_threadPool->m_defaultPriorityTasks.front());
		m_threadPool->m_defaultPriorityTasks.pop_front();

		m_threadPool->m_defaultPriorityTasksMutex.unlock();

		//std::cout << "(exec) Thread " << m_threadName << std::endl;
		task->ExecuteTask();
	}
}

void HThread::RunThread(SThreadType_Stream) {
	std::unique_ptr<HTask> task;
	
	bool streamWork = false;
	bool defaultWork = false;

	while (true) {
		streamWork = false;
		defaultWork = false;

		std::unique_lock<std::mutex> lk(m_threadMutex);

		m_threadPool->m_streamWorkCV.wait(lk, [this, &streamWork, &defaultWork] {
			m_threadPool->m_streamPriorityTasksMutex.lock();

			auto doWork = m_shutdown || !m_threadPool->m_streamPriorityTasks.empty();

			if (!doWork) {
				m_threadPool->m_streamPriorityTasksMutex.unlock();
			}
			else {
				m_working = true;
				streamWork = true;
				return doWork;
			}

			m_threadPool->m_defaultPriorityTasksMutex.lock();

			doWork = !m_threadPool->m_defaultPriorityTasks.empty();

			if (!doWork) {
				m_working = false;
				m_threadPool->m_defaultPriorityTasksMutex.unlock();
			}
			else {
				m_working = true;
				defaultWork = true;
			}

			return doWork;
		});

		if (m_shutdown) {
			if (streamWork) { m_threadPool->m_streamPriorityTasksMutex.unlock(); }
			else if (defaultWork) { m_threadPool->m_defaultPriorityTasksMutex.unlock(); }
			return; // Stop thread
		}

		if (streamWork) { 
			task = std::move(m_threadPool->m_streamPriorityTasks.front());
			m_threadPool->m_streamPriorityTasks.pop_front();
			m_threadPool->m_streamPriorityTasksMutex.unlock();
		}
		else if (defaultWork) {
			task = std::move(m_threadPool->m_defaultPriorityTasks.front());
			m_threadPool->m_defaultPriorityTasks.pop_front();
			m_threadPool->m_defaultPriorityTasksMutex.unlock();
		}

		//std::cout << "Stream thread, streamWork: " << streamWork << " | defaultWork: " << defaultWork << ", executing Task!" << std::endl;
		task->ExecuteTask();
	}
}

void HThread::RequestShutdown() {
	m_shutdown = true;
}

} // namespace HE