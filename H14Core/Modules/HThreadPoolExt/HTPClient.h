#pragma once

#include <mutex>

#include "HTask.h"

namespace HE {

class HThreadPool;

/**
* HTPClient class
*
* A class extending the component class. This class makes asynchronous tasks possible, providing a simple mutex interface for the component.
*
* [ ] Copyable
* [x] Movable (to comply with HComponent)
*
**/
//class HTPClient
//{
//public:
//	friend class HComponentFactory;
//
//	HTPClient(const HTPClient&) = delete;
//	HTPClient& operator= (const HTPClient&) = delete;
//	HTPClient(HTPClient&&) = default;
//	HTPClient& operator= (HTPClient&&) = default;
//
//	void Lock();
//
//	void CreateAsyncTask(ETaskType taskType, ETaskPriority taskPriority, std::function<void()>&& function);
//
//private:
//	HTPClient(HThreadPool* threadPool);
//
//	HThreadPool* m_threadPool;
//
//	mutable std::mutex m_mutex;
//};

} // namespace HE