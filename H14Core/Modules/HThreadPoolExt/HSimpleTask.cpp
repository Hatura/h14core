#include "HSimpleTask.h"

namespace HE {

HSimpleTask::HSimpleTask(ETaskChannel taskPriority, std::function<void()>&& task, ETaskPriority streamTaskPriority)
	: HTask{ETaskType::SimpleTask, taskPriority, std::move(task), streamTaskPriority} {}

} // namespace HE