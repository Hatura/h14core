#pragma once

#include "HModule.h"

#include <set>
#include <functional>
#include <memory>

#include "../Math/Vec.h"

#include "HRendererExt/HCamera.h"

namespace HE {

class HSprite;

enum class E2DPlane {
	Nearest,
	Near,
	Middle,
	Far,
	Farthest
};

/**
* HRenderer class
*
* The purpose of this class is:
* - Ordering of renderable Components in an efficient way
* - Managing Shaders (Transmitting UBO data)
* - Managing RenderTargets (Scene + Framebuffers [FBO] if needed)
* - Drawing renderable Components to Target
* 
* Sometimes an explicit order of drawing elements is necessary (2D Sprites) so maybe we set up some priority layers that can be drawn
* upon and make a order (Priority > Shadertype || Geometry [VAO]) for 3D stuff the z-Order should to the trick so we need only Shadertype > Geometry.
*
* TODO: Read up on VAO and glMutlipleArrayDraw (before doing anything here)
* TODO: How do we handle D3D9/11 Rendering, or do we support it at all? If we do we need to create unique Sprite/Mesh implementations, maybe
* SpriteD3D9, SpriteD3D11 and a unique class of this which handles this.
*
* [ ] Copyable (inherited from HModule)
* [ ] Movable (inherited from HModule)
*
**/
class HRenderer final : public HModule
{
public:
	friend class HGame;		// Constructor access

	// Remember: This can neither be static nor can HCamera have a public ctor because we inject this HRenderer into the class
	std::unique_ptr<_HCamera> CreateCamera(Rad degFOV, float nearDist, float farDist, Vec3f eye, Vec3f upAxis, Rad heading, Rad attitude, Rad bank) const;

	// Register and place into container, called from within PostConstructInit() in HSprite
	void RegisterSprite(HSprite* sprite, E2DPlane plane = E2DPlane::Farthest);

	// Slower than with plane, checks all planes
	void UnregisterSprite(HSprite* sprite);

	// Faster, when plane is known
	void UnregisterSprite(HSprite* sprite, E2DPlane plane);

	void SetActiveCamera(_HCamera* camera);

	// Should be automatically called by camera dtor, if newCamera is nullptr it sets the camera to m_defaultCamera
	// TODO: Is this the best way? Because when we attempt to switch a camera to something entirely different we call this thorugh the dtor
	// with nullptr and then SetActiveCamera() afterwards (2 function calls) - another solution would be manual unregistering, but if that is
	// forgotten we point to something deleted.. ? Maybe re-evaluate
	void UnregisterCamera(_HCamera* newCamera = nullptr);

	// Maybe better to store the resolution here and let HWindow (or HGame) update it on resolution change, but for now we just query the HWindow
	Vec2ui GetRenderResolution() const;

	const _HCamera* GetActiveCamera() const;
	_HCamera* GetActiveCamera();

private:
	HRenderer(HGame* game);

	void RenderFrame();

	// We always need to have some sort of default or dummy camera if none is set up or we have no Matrix to send to the shader
	std::unique_ptr<_HCamera> m_defaultCamera;

	// The actual camera active
	_HCamera* m_activeCamera;

	std::set<HSprite*, std::function<bool(const HSprite* lhs, const HSprite* rhs)>> m_farthestSpritePlane;

	// Function to order HSprite's into the sets to get efficiency while drawing
	static bool SpriteComperator_1(const HSprite* lhs, const HSprite* rhs);
};

} // namespace HE