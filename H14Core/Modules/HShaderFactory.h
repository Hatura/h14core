#pragma once

#include "HModule.h"

#include <deque>
#include <memory>

#include "HShaderFactoryExt/H2DShader.h"

namespace HE {

class H2DMaterial;

class HShaderFactory : public HModule
{
public:
	friend class HGame;

	// For every material a shader is created, for every instance (submaterial) a shader is linked?
	void CreateFromMaterial(H2DMaterial* material);
private:
	HShaderFactory(HGame* game);

	std::deque<std::unique_ptr<H2DShader>> m_2dShaders;
};

} // namespace HE