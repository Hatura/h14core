#include "../HGame.h"
#include "HActorManager.h"

#include <vector>
#include <iostream>

#include "HThreadPool.h"
#include "../Components/HActor.h"
#include "../HObjectProtector.h"

namespace HE {

HActorManager::HActorManager(HGame* game)
	: HModule(game, "HActorManager") {

}

void HActorManager::Update() {
	// TODO: Don't do this every update.. HUGE BOTTLENECK ATM!
	unsigned int numDelActors = 0;
	m_actors.erase(std::remove_if(m_actors.begin(), m_actors.end(), [&numDelActors] (auto& actor) {
		auto delActor = actor->HasDeadFlag();
		if (actor->HasDeadFlag()) {
			++numDelActors;
			return true;
		}
		return false;
	}), m_actors.end());

	if (numDelActors > 0) {
		std::cout << "Deleted " << numDelActors << " Actors" << std::endl;
	}

	if (m_actors.size() > 0) {
		GetGame()->GetModule<HThreadPool>()->RegisterTaskPack<HActor>(m_actors, [] (HActor* actor) {
			HObjectProtector{actor};
			actor->Update();
		});
	}
}

HActorManager::~HActorManager() = default;

HActor* HActorManager::RegisterActor(std::unique_ptr<HActor> actorUPtr) {
	m_actors.push_back(std::move(actorUPtr));

	return m_actors.back().get();
}

}
