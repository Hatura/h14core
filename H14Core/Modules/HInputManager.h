#pragma once

#include "HModule.h"

namespace HE {

/**
* HInputManager class
*
* delete me
*
* [ ] Copyable (inherited from HModule)
* [ ] Movable (inherited from HModule)
*
**/
class HInputManager : public HModule
{
public:
	friend class HGame;

private:
	HInputManager(HGame* game);
};

}