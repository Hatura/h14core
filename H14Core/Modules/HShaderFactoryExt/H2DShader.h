#pragma once

namespace HE {

/**
* H2DShader class
*
* [ ] Copyable
* [x] Movable
*
**/
class H2DShader
{
public:
	H2DShader(const H2DShader&) = delete;
	H2DShader& operator= (const H2DShader&) = delete;
	H2DShader(H2DShader&&) = default;
	H2DShader& operator= (H2DShader&&) = default;

	friend class HShaderFactory;

private:
	H2DShader();
};

} // namespace HE