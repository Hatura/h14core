#include "../HGame.h"
#include "HGameMode.h"

#include "HThreadPool.h"
#include "../Utility/HHighResClock.h"
#include "../Components/UnitController/HPlayerController.h"

namespace HE {

HGameMode::HGameMode(HGame* game, std::string gameModeName) 
	: HModule{game, "HGameMode"}
	, m_actorManager{nullptr}
	, m_gameModeName{std::move(gameModeName)} {

	m_actorManager = std::unique_ptr<HActorManager>(new HActorManager{GetGame()});
}

HPlayerController* HGameMode::CreatePlayerController(unsigned int playerID) {
	bool playerIDReserved = false;

	for (auto& player : m_localPlayerControllers) {
		if (playerID == player->GetPlayerID()) {
			playerIDReserved = true;
			break;
		}
	}

	if (playerIDReserved) {
		GetGame()->RequestQuit(true, "Tried to create PlayerController for already existing player");
	}

	m_localPlayerControllers.emplace_back(std::unique_ptr<HPlayerController>(new HPlayerController{GetGame(), playerID}));

	return m_localPlayerControllers.back().get();
}

void HGameMode::Update() {
	auto before = HHighResClock::now();

	for (auto& pc : m_localPlayerControllers) {
		pc->Update();
	}

	m_actorManager->Update();

	GetGame()->GetModule<HThreadPool>()->ProcessAndYield();

	auto after = HHighResClock::now();
	auto amount = after - before;

	//GetGame()->GetModule<HLogger>()->Log(ELogLevel::Debug, "Amount of time: ", amount.count());
}

HActorManager* HGameMode::GetActorManager() const {
	return m_actorManager.get();
}

} // namespace HE
