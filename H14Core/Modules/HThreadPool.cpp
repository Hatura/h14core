#include "HThreadPool.h"

#ifdef _WIN32
#include <Windows.h> // GetSystemInfo()
#elif __linux__
#include <unistd.h>
#endif

namespace HE {

HThreadPool::HThreadPool(HGame* game, unsigned int customThreadCount)
	: HModule{game, "HThreadPool"}
	, m_numTotalCores{0}
	, m_numDefaultWorkerThreads{0}
	, m_numStreamWorkerThreads{0}
	, m_packTasksPerCore{6} {
	
	if (customThreadCount != 0) {
		m_numTotalCores = customThreadCount;
	}
	else {
#ifdef _WIN32
		SYSTEM_INFO systemInfo;
		GetSystemInfo(&systemInfo);

		m_numTotalCores = systemInfo.dwNumberOfProcessors; // dword = unsigned 32 bit
#elif __linux__
		m_numTotalCores = sysconf(_SC_NPROCESSORS_ONLN); // stackoverflow.com/questions/150355/programmatically-find-the-number-of-cores-on-a-machine
#endif
	}

	if (m_numTotalCores == 0) {
		// Couldn't detect cores, give some debug message of type "Cores need to be set manually"
	}
	else if (m_numTotalCores == 1) {
		// Shouldn't need any attention because ProcessAndYield automatically does it singlethreaded then
	}
	else {
		// If we have 2+ Cores we use the default setup, atleast 1 Stream hardware Thread + rest workers if possible (not on 2 cores)
		for (unsigned int i = 0; i < m_numTotalCores - 1; ++i) {
			std::string threadName = "WorkerThread # " + std::to_string(i + 1);

			EThreadType threadType = EThreadType::Default;

			if (i == 0) {
				// We always want on thread that does streaming/logging (unimportant stuff)
				threadType = EThreadType::StreamPriority;
				++m_numStreamWorkerThreads;
			}
			else {
				++m_numDefaultWorkerThreads;
			}

			m_workerThreads.emplace_back(std::make_unique<HThread>(this, threadType, std::move(threadName)));
		}
	}
}

HThreadPool::~HThreadPool() {
	for (auto& thread : m_workerThreads) {
		thread->RequestShutdown();
	}

	m_defaultWorkCV.notify_all();
	m_streamWorkCV.notify_all();

	for (auto& thread : m_workerThreads) {
		thread->JoinPool();
	}
}

void HThreadPool::RegisterTask(ETaskType taskType, ETaskChannel taskPriority, std::function<void()>&& function, ETaskPriority streamTaskPriority) {
	if (m_numTotalCores <= 1) {
		function();
	}
	else {
		if (taskPriority == ETaskChannel::Stream) {
			auto task = std::make_unique<HSimpleTask>(taskPriority, std::move(function), streamTaskPriority);
			{
				std::lock_guard<std::mutex> lk{m_streamPriorityTasksMutex};
				m_streamPriorityTasks.push_back(std::move(task));
				std::sort(m_streamPriorityTasks.begin(), m_streamPriorityTasks.end(), [] (const auto& lhs, const auto & rhs) {
					return lhs->GetTaskPriority() > rhs->GetTaskPriority();
				});
			}
			m_streamWorkCV.notify_one();
		}
		else if (taskPriority == ETaskChannel::Priority) {
			auto task = std::make_unique<HSimpleTask>(taskPriority, std::move(function));
			{
				std::lock_guard<std::mutex> lk{m_defaultPriorityTasksMutex};
				m_defaultPriorityTasks.push_front(std::move(task));
				std::sort(m_defaultPriorityTasks.begin(), m_defaultPriorityTasks.end(), [] (const auto& lhs, const auto & rhs) {
					return lhs->GetTaskPriority() > rhs->GetTaskPriority();
				});
			}
			m_defaultWorkCV.notify_one();
		}
	}
}

void HThreadPool::ProcessAndYield() {
	{
		std::unique_ptr<HTask> task;

		while (true) {
			m_defaultPriorityTasksMutex.lock();

			if (!m_defaultPriorityTasks.empty()) {
				task = std::move(m_defaultPriorityTasks.front());
				m_defaultPriorityTasks.pop_front();

				m_defaultPriorityTasksMutex.unlock();

				//std::cout << "CPU0 yield / Exec" << std::endl;
				task->ExecuteTask();
			}
			else {
				m_defaultPriorityTasksMutex.unlock();
				break;
			}
		}
	}

	for (const auto& thread : m_workerThreads) {
		while (thread->IsWorking()) {
			std::this_thread::yield();
		}
	}
}

void HThreadPool::ProcessPriorityStreamTasks() {
	std::unique_ptr<HTask> task;

	std::lock_guard<std::mutex> lk{m_streamPriorityTasksMutex};

	while (true) {
		if (m_streamPriorityTasks.size() > 0) {
			task = std::move(m_streamPriorityTasks.front());
			m_streamPriorityTasks.pop_front();
			if (task->GetTaskPriority() == ETaskPriority::Priority) {
				task->ExecuteTask();
			}
			else {
				break;
			}
		}
		else {
			break;
		}
	}
}

unsigned int HThreadPool::GetNumCores() const {
	return m_numTotalCores;
}

unsigned int HThreadPool::GetNumTotalWorkerThreads() const {
	return m_numDefaultWorkerThreads + m_numStreamWorkerThreads;
}

unsigned int HThreadPool::GetNumDefaultWorkerThreads() const {
	return m_numDefaultWorkerThreads;
}

unsigned int HThreadPool::GetNumStreamWorkerThreads() const {
	return m_numStreamWorkerThreads;
}

} // namespace HE
