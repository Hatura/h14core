#pragma once

namespace HE {

class HObject;

/**
* HObjectProtector class
*
* Simple lock_guard like ctor/dtor mutex protection for HObjects used in the Threadpool
*
* [ ] Copyable
* [ ] Movable
*
**/
class HObjectProtector
{
public:
	HObjectProtector(HObject* object);
	~HObjectProtector();

	HObjectProtector(const HObjectProtector&) = delete;
	HObjectProtector& operator= (const HObjectProtector&) = delete;
	HObjectProtector(HObjectProtector&&) = delete;
	HObjectProtector& operator= (HObjectProtector&&) = delete;

private:
	HObject* m_object;
};

}