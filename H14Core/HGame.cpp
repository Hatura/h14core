#include "HGame.h"

#include "Modules/HThreadPool.h"
#include "Modules/HLogger.h"
#include "Modules/HWindow.h"
#include "Modules/HGameMode.h"
#include "Modules/HRenderer.h"
#include "Modules/HComponentFactory.h"
#include "Modules/HResourceManager.h"
#include "Modules/HMaterialFactory.h"
#include "Modules/HShaderFactory.h"
#include "Modules/HInputManager.h"

#include "Input/HMouse.h"

namespace HE {

HGame::HGame(std::string gameName, unsigned int width, unsigned int height, EScreenMode screenMode, const Vec4f& backgroundColor)
	: m_gameName{std::move(gameName)}
	, m_threadpool{nullptr}
	, m_logger{nullptr}
	, m_window{nullptr}
	, m_gameMode{nullptr}
	, m_renderer{nullptr}
	, m_factory{nullptr}
	, m_resourceManager{nullptr}
	, m_materialFactory{nullptr}
	, m_shaderFactory{nullptr}
	, m_exitGame{false} {

	// Can't use make_unique because of XMEMORY ACCESS!!!
	m_threadpool = std::unique_ptr<HThreadPool>(new HThreadPool{this});
	m_logger = std::unique_ptr<HLogger>(new HLogger{this, m_threadpool.get()});
	m_window = std::unique_ptr<HWindow>(new HWindow{this, width, height, screenMode, backgroundColor});
	m_gameMode = std::unique_ptr<HGameMode>(new HGameMode{this, "default"});
	m_factory = std::unique_ptr<HComponentFactory>(new HComponentFactory{this});
	m_resourceManager = std::unique_ptr<HResourceManager>(new HResourceManager{this});
	m_materialFactory = std::unique_ptr<HMaterialFactory>(new HMaterialFactory{this});
	m_shaderFactory = std::unique_ptr<HShaderFactory>(new HShaderFactory{this});
	m_renderer = std::unique_ptr<HRenderer>(new HRenderer{this});

	HMouse::NotifyDisplayChange(m_window.get());
	HMouse::DisplayCursor(false);
}

HGame::~HGame() = default;

bool HGame::Update() {
	m_window->Update();

	HMouse::BufferPosition(m_window.get());

	//auto before = std::chrono::high_resolution_clock::now();

	m_gameMode->Update();
	
	//auto after = std::chrono::high_resolution_clock::now();

	//auto time = std::chrono::duration_cast<std::chrono::milliseconds>(after - before);

	//std::cout << time.count() << "ms" << std::endl;

	GetModule<HRenderer>()->RenderFrame();

	m_window->PostUpdate();

	// This mechanism guarantuees that one extra loop for user speicif quit messages is done, maybe we can rewrite it in a less complex way?
	if (m_exitGame == true) {
		return false;
	}

	if (m_window->IsWindowClosed()) {
		m_exitGame = true;
	}

	return true;
}

void HGame::RequestQuit(bool error, const std::string& quitReason) {
	// Signal the window to close, this is then fetched in HGame::Update (every update) which then returns false to indicate the game loop that it stopped running

	// TODO: Log reason for quit
	HLogger* logger = GetModule<HLogger>();
	if (logger != nullptr) {
		if (error) {
			logger->Log(ELogLevel::Error, quitReason, " - calling std::terminate()");
			m_threadpool->ProcessPriorityStreamTasks();
			std::this_thread::sleep_for(std::chrono::milliseconds(1500));
			std::terminate();
		}
		else {
			logger->Log(ELogLevel::Info, quitReason);
			m_threadpool->ProcessPriorityStreamTasks();
			m_window->OnQuitRequest();
		}
	}	
}

bool HGame::IsRunning() const {
	return m_exitGame;
}

const HWindowEvent& HGame::GetEvents() const {
	return m_window->m_windowEvents;
}

HWindow* HGame::GetWindow() {
	return m_window.get();
}

} // namespace HE
