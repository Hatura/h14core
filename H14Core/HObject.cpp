#include "HGame.h"
#include "HObject.h"

namespace HE {

HObject::HObject()
	: m_game{nullptr} {}

HObject::~HObject() {}

HGame* HObject::GetGame() const {
	return m_game;
}

void HObject::SetGame(HGame* game) {
	m_game = game;
}

} // namespace HE
