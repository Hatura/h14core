#pragma once

#include "HResource.h"

#include <memory>
#include <vector>
#include <iostream> //test

namespace HE {

enum class EImageType {
	Undefined,
	BMP,
	PNG,
	JPG
};

/**
* SDeleterInfo class
*
* Virtual baseclass for different deleter options because we use malloc and new .. used within the deleter
*
* [ ] Copyable
* [x] Movable
*
**/
//struct SDeleterInfo {
//	SDeleterInfo() = default;
//	virtual ~SDeleterInfo() = 0;
//
//	SDeleterInfo(const SDeleterInfo&) = delete;
//	SDeleterInfo& operator= (const SDeleterInfo&) = delete;
//	SDeleterInfo(SDeleterInfo&&) = default;
//	SDeleterInfo& operator= (SDeleterInfo&&) = default;
//};
//
//struct PNGDeleterInfo : public SDeleterInfo {
//	unsigned int m_rows;
//};
//
//struct BMPDeleterInfo : public SDeleterInfo {
//	unsigned int m_uints;
//};
//
//struct SImageDeleter {
//	SImageDeleter()
//		: m_imageType{EImageType::Undefined}
//		, m_deleterInfo{nullptr} { }
//
//	void operator()(unsigned char** p) {
//		switch (m_imageType) {
//			default:
//			case EImageType::BMP:
//			{
//				auto* bmpDeleterInfo = static_cast<BMPDeleterInfo*>(m_deleterInfo.get());
//
//				std::cout << "Deleting BMP image from memory, num uints: " << bmpDeleterInfo->m_uints << std::endl;
//
//				for (unsigned int i = 0; i < bmpDeleterInfo->m_uints; ++i) {
//					delete p[i];
//				}
//
//				delete[] p;
//			}
//				break;
//			case EImageType::JPG:
//				break;
//			case EImageType::PNG:
//			{
//				auto* pngDeleterInfo = static_cast<PNGDeleterInfo*>(m_deleterInfo.get());
//
//				std::cout << "Deleting PNG image from memory, height: " << pngDeleterInfo->m_rows << std::endl;
//
//				for (unsigned int i = 0; i < pngDeleterInfo->m_rows; ++i) {
//					free(p[i]);
//				}
//
//				free(p);
//			}
//			break;
//		}
//	}
//
//	EImageType m_imageType;
//	std::unique_ptr<SDeleterInfo> m_deleterInfo;
//};

class HImageResource : public HResource
{
public:
	HImageResource();

	bool SupportsDefault() const override;
	std::string GetDefaultFilename() const override;

	unsigned int GetTextureWidth() const;
	unsigned int GetTextureHeight() const;
	const std::vector<unsigned char>& GetData() const;

private:
	void LoadResource(SResourceLoadInfo& loadInfo) override;

	void XLoadPNG(SResourceLoadInfo& loadInfo);
	void XLoadJPG(SResourceLoadInfo& loadInfo);
	void XLoadBMP(SResourceLoadInfo& loadInfo);

	// RGBA
	std::vector<unsigned char> m_data;

	unsigned int m_textureWidth;
	unsigned int m_textureHeight;
};

} // namespace HE