#include "HImageResource.h"

#include <fstream>
#include <stdio.h>

#include "../../libpng/png.h"

namespace HE {

// Needs to be pure virtual, for GCC (can't add definition in header, unless inline, but this should be alright)
//SDeleterInfo::~SDeleterInfo() {
//
//}

HImageResource::HImageResource()
	: m_textureWidth{0}
	, m_textureHeight{0} {
	// Since C++ doesn't have a static initializer.. is there a better way?
	std::call_once(GetFileExtensionsOnceFlag(), [] { SetFileExtensions(std::set<std::string>{"png", "jpg", "jpeg", "bmp"}); });
}

void HE::HImageResource::LoadResource(SResourceLoadInfo& loadInfo) {
	// Since this works only with file extensions, it's always cached!
	if (GetFileExtension() == "png") {
		XLoadPNG(loadInfo);
	}
	else if (GetFileExtension() == "bmp") {
		XLoadBMP(loadInfo);
	}
	else {
		loadInfo.AppendError("File format '" + GetFileExtension() + "' not supported!");
	}
}

void HE::HImageResource::XLoadPNG(SResourceLoadInfo& loadInfo) {
	// Some of the source taken from gist.github.com/niw/5963798 (MIT license)

	FILE* pngFile = fopen(GetResourceName().c_str(), "rb");

	if (pngFile == nullptr) {
		loadInfo.AppendError("Couldn't open PNG File" + GetResourceName());
		return;
	}

	auto pngReadStruct = png_create_read_struct(PNG_LIBPNG_VER_STRING, nullptr, nullptr, nullptr);

	if (!pngReadStruct) {
		loadInfo.AppendError("Couldn't create PNG read_struct");
		return;
	}

	auto pngInfoStruct = png_create_info_struct(pngReadStruct);
	if (!pngInfoStruct) {
		loadInfo.AppendError("Couldn't create PNG info_struct");
		return;
	}

	if (setjmp(png_jmpbuf(pngReadStruct))) {
		loadInfo.AppendError("Couldn't set up libpng's shitty jump-goto buffer");
		return;
	}

	// Can this throw?
	png_init_io(pngReadStruct, pngFile);
	png_read_info(pngReadStruct, pngInfoStruct);

	auto width = png_get_image_width(pngReadStruct, pngInfoStruct);
	auto height = png_get_image_height(pngReadStruct, pngInfoStruct);
	auto colorType = png_get_color_type(pngReadStruct, pngInfoStruct);
	auto bitDepth = png_get_bit_depth(pngReadStruct, pngInfoStruct);

	// Read everything as 8bit color channel RGBA (255, 255, 255, 255)
	if (colorType == PNG_COLOR_TYPE_PALETTE) {
		png_set_palette_to_rgb(pngReadStruct);
	}
	if (bitDepth == 16) {
		png_set_strip_16(pngReadStruct);
	}

	// PNG_COLOR_TYPE_GRAY_ALPHA always 8 or 16 bit (????)
	png_set_expand_gray_1_2_4_to_8(pngReadStruct);

	if (png_get_valid(pngReadStruct, pngInfoStruct, PNG_INFO_tRNS)) {
		png_set_tRNS_to_alpha(pngReadStruct);
	}

	// If the PNG image is of these types it doesn't have an alpha channel, we just fill it with 0xFF
	if (colorType == PNG_COLOR_TYPE_RGB || colorType == PNG_COLOR_TYPE_GRAY || colorType == PNG_COLOR_TYPE_PALETTE) { // is it really still palette? Didn't we redescribe it earlier? Maybe in update_info
		png_set_filler(pngReadStruct, 0xFF, PNG_FILLER_AFTER);
	}

	

	if (colorType == PNG_COLOR_TYPE_GRAY || colorType == PNG_COLOR_TYPE_GRAY_ALPHA) {
		png_set_gray_to_rgb(pngReadStruct);
	}

	png_read_update_info(pngReadStruct, pngInfoStruct);

	::png_bytep* buffer = reinterpret_cast<::png_bytep*>(malloc(sizeof(png_bytep) * height));

	for (unsigned int i = 0; i < height; ++i) {
		buffer[i] = (png_bytep)malloc(png_get_rowbytes(pngReadStruct, pngInfoStruct));
	}
	
	png_read_image(pngReadStruct, buffer);

	// We copy the data here, may be expensive but is there really another way?
	for (unsigned int row = 0; row < height; ++row) {
		for (unsigned int bytes = 0; bytes < 4 * width; ++bytes) {
			m_data.push_back(buffer[height - row - 1][bytes]);
		}
	}

	// Free data memory
	for (unsigned int i = 0; i < height; ++i) {
		free(buffer[i]);
	}

	free(buffer);

	// Free the structs memory
	png_destroy_read_struct(&pngReadStruct, &pngInfoStruct, nullptr);

	m_textureWidth = width;
	m_textureHeight = height;
}

void HE::HImageResource::XLoadJPG(SResourceLoadInfo& loadInfo) {


}

void HE::HImageResource::XLoadBMP(SResourceLoadInfo& loadInfo) {
	std::ifstream bmpFile{GetResourceName(), std::ios::binary};

	if (!bmpFile.good()) {
		loadInfo.AppendError("Couldn't open BMP file " + GetResourceName());
		return;
	}

	unsigned char bmpHeader[54];

	bmpFile.read(reinterpret_cast<char*>(bmpHeader), 54);

	if (bmpHeader[0] != 'B' || bmpHeader[1] != 'M') {
		loadInfo.AppendError("Not a correct BMP-File (header not starting with BM)");
	}

	std::uint32_t bmpDataPos = (*reinterpret_cast<std::uint32_t*>(&(bmpHeader[10])));
	std::uint32_t bmpImageSize = (*reinterpret_cast<std::uint32_t*>(&(bmpHeader[34])));
	std::uint32_t bmpWidth = (*reinterpret_cast<std::uint32_t*>(&(bmpHeader[18])));
	std::uint32_t bmpHeight = (*reinterpret_cast<std::uint32_t*>(&(bmpHeader[22])));

	bmpFile.seekg(bmpDataPos);

	// Check for validity
	if (bmpImageSize == 0) {
		bmpImageSize = bmpWidth * bmpHeight * 3;
	}

	m_data.resize(bmpWidth * bmpHeight * 4); // RGBA reserve, should be faster than continuous push_backs

	for (unsigned int i = 0; i < bmpWidth * bmpHeight; ++i) {
		unsigned char rgba[3];
		bmpFile.read(reinterpret_cast<char*>(rgba), 3);
		
		// Switch R and B since Bitmaps are BGR
		m_data[i * 4 + 0] = rgba[2];
		m_data[i * 4 + 1] = rgba[1];
		m_data[i * 4 + 2] = rgba[0];
		m_data[i * 4 + 3] = 255;		// Bitmap has no alpha channel
	}

	m_textureWidth = bmpWidth;
	m_textureHeight = bmpHeight;
}

bool HImageResource::SupportsDefault() const {
	return true;
}

std::string HImageResource::GetDefaultFilename() const {
	return "placeholder.png";
}

unsigned int HE::HImageResource::GetTextureWidth() const {
	return m_textureWidth;
}

unsigned int HE::HImageResource::GetTextureHeight() const {
	return m_textureHeight;
}

const std::vector<unsigned char>& HE::HImageResource::GetData() const {
	return m_data;
}

}