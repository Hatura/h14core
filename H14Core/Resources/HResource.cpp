#include "HResource.h"

#include <iostream> // test

namespace HE {

bool HResource::m_specificExtensionsOnly = false;
std::once_flag HResource::m_fileExtensionsOnceFlag;
std::set<std::string> HResource::m_supportedFileExtensions;

HResource::HResource()
	: m_usageCount{0} {

}

void HResource::SetFileExtensions(std::set<std::string> extensions) {
	m_supportedFileExtensions = std::move(extensions);

	m_specificExtensionsOnly = true;
}

std::once_flag& HResource::GetFileExtensionsOnceFlag() {
	return m_fileExtensionsOnceFlag;
}

bool HResource::GetSpecificExtensionsOnlyFlag() const {
	return m_specificExtensionsOnly;
}

const std::string& HResource::GetFileExtension() const {
	return m_cachedFileExtension;
}

bool HResource::ValidateFileExtension() const {
	std::size_t lastPointPos = GetResourceName().find_last_of(".");

	if (lastPointPos == std::string::npos) {
		// No point in filename
		return false;
	}

	auto extension = GetResourceName().substr(lastPointPos+1);

	if (extension == "") {
		// No extensions after point
		return false;
	}

	m_cachedFileExtension = std::move(extension);

	return true;
}

void HResource::IncreaseUsageCount() {
	++m_usageCount;
}

void HResource::DecreaseUsageCount() {
	--m_usageCount;
}

void HResource::SetUsageCountExplicit(unsigned int usageCount) {
	m_usageCount = usageCount;
}

unsigned int HResource::GetUsageCount() const {
	return m_usageCount;
}

const std::string& HResource::GetResourceName() const {
	return m_resourceName;
}

bool HResource::SupportsDefault() const {
	return false;
}

std::string HResource::GetDefaultFilename() const {
	return "";
}

}