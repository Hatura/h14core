#pragma once

#include <string>
#include <set>
#include <mutex>

namespace HE {

enum class EResourceLoadResult {
	Success,
	SuccessPlusWarnings,
	Error
};

/**
* struct SResourceLoadInfo
*
* Describes the result of the loading process (Success, Warning, Error) + describtions of what happened.
* The default result is Success.
*
* TODO: Maybe later add something that indicates that the source of failure is not enough memory, so if
* we pre-allocate memory we can free something of that and try again!
*
**/
struct SResourceLoadInfo {
	SResourceLoadInfo()
		: m_loadResult{EResourceLoadResult::Success} { }

	void AppendWarning(std::string warning) {
		m_infoString.append(std::string("Warning: ") + std::move(warning) + "\n");
		if (m_loadResult != EResourceLoadResult::Error) {
			m_loadResult = EResourceLoadResult::SuccessPlusWarnings;
		}
	}

	void AppendError(std::string error) {
		m_infoString.append(std::string("Error: ") + std::move(error) + "\n");

		m_loadResult = EResourceLoadResult::Error;
	}

	EResourceLoadResult GetResourceLoadResult() const {
		return m_loadResult;
	}

	bool HasWarnings() const {
		return m_infoString.size() > 0;
	}

	const std::string& GetInfoString() const {
		return m_infoString;
	}

private:
	EResourceLoadResult m_loadResult;
	std::string m_infoString;
};

class HGame;

class HResource
{
public:
	HResource();
	virtual ~HResource() = default;

	template<typename T>
	friend class HSharedRes;
	friend class HResourceManager;

	unsigned int GetUsageCount() const;
	const std::string& GetResourceName() const;

	// Overwrite these functions if a default is supported (like a placeholder image, soundfile..)
	virtual bool SupportsDefault() const;
	virtual std::string GetDefaultFilename() const;

protected:
	static void SetFileExtensions(std::set<std::string> extensions);
	static std::once_flag& GetFileExtensionsOnceFlag();

	// Indicates if the resource can load from any file (via signature checks) or only from specific extensions (.png for example)
	// the standard is false, it is set automatically to true if SetFileExtensions() is called
	bool GetSpecificExtensionsOnlyFlag() const;

	// Be careful to only use this if file extensions are supported!
	const std::string& GetFileExtension() const;

private:
	virtual void LoadResource(SResourceLoadInfo& loadInfo) = 0;

	// Returns true if load was successful
	template<typename T>
	SResourceLoadInfo Load(T&& fileName) {
		m_resourceName = std::forward<T>(fileName);

		SResourceLoadInfo loadInfo;

		if (GetSpecificExtensionsOnlyFlag() == true) {
			bool legitExtension = ValidateFileExtension();

			if (!legitExtension) {
				// TODO: Return errorcode
				loadInfo.AppendError("No legit file extension, '.' missing or no extension after '.'?");
				return loadInfo;
			}
		}

		LoadResource(loadInfo);

		return loadInfo;
	}

	bool ValidateFileExtension() const;

	void IncreaseUsageCount();
	void DecreaseUsageCount();
	void SetUsageCountExplicit(unsigned int usageCount);

	const HGame* game;

	std::string m_resourceName;
	mutable std::string m_cachedFileExtension;
	unsigned int m_usageCount;

	static bool m_specificExtensionsOnly;

	static std::once_flag m_fileExtensionsOnceFlag;
	static std::set<std::string> m_supportedFileExtensions;
};

} // namespace HE
