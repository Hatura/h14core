#pragma once

#include "Components/HActor.h"
#include "Components/HSprite.h"

namespace HE {

/**
*
* Just for testing purposes, like main.cpp not part of the library..
*
**/
class HPuppet : public HActor
{
public:
	HFactoryConstructible

	void PostConstructInit() override;

	void OnUpdate() override {
		//std::this_thread::sleep_for(std::chrono::milliseconds(1));
		HActor::OnUpdate();

		m_dummyVar = 3.11233f * 2311;
		m_dummyVar = m_dummyVar * m_dummyVar * m_dummyVar;
	}

	void Puppet();

private:
	HPuppet() = default;

	float m_dummyVar;
};

}