#pragma once

#include <string>
#include <functional>

namespace HE {

template<typename T>
class HCallback
{
public:
	// Be careful when using this manually, it's not intended to be used this way.
	// Instead use the free functions starting with FCreateBind_
	HCallback() {}
	virtual ~HCallback() {}

	HCallback(const HCallback&) = default;
	HCallback& operator= (const HCallback&) = default;
	HCallback(HCallback&&) = default;
	HCallback& operator= (HCallback&&) = default;

	enum class EBindComperatorType {
		NoComperator,
		StringComperator,
		FunctionCompterator
	};

	EBindComperatorType GetComperatorType() const {
		return m_bindComperatorType;
	}

	void SetStringComperator(std::string comperator) {
		m_bindComperatorType = EBindComperatorType::StringComperator;
		m_bindComperatorString = std::move(comperator);
	}

	void SetFunctionComperator(std::function<const std::string&()> fun) {
		m_bindComperatorType = EBindComperatorType::FunctionCompterator;
		m_bindComperatorFunction = std::move(fun);
	}

	bool HasComperator() const {
		return m_bindComperatorType != EBindComperatorType::NoComperator;
	}

	bool HasStringComperator() const {
		return m_bindComperatorType == EBindComperatorType::StringComperator;
	}

	bool HasFunctionComperator() const {
		return m_bindComperatorType == EBindComperatorType::FunctionCompterator;
	}

	const std::string& GetComperatorString() const {
		if (HasFunctionComperator()) {
			return m_bindComperatorFunction();
		}
		else {
			return m_bindComperatorString;
		}
	}

private:
	EBindComperatorType						m_bindComperatorType;
	mutable std::string						m_bindComperatorString;
	std::function<const std::string&()>		m_bindComperatorFunction;
};

template<typename T>
class HKeybindCallback : public HCallback<T>
{
public:
	HKeybindCallback(std::function<void(T*)> function, std::string bindKeyword)
		: m_function{std::move(function)}
		, m_keybindName{std::move(bindKeyword)} {}

	void Execute(T* invoker) {
		m_function(invoker);
	}

	const std::string& GetKeybindName() const {
		return m_keybindName;
	}

private:
	std::string								m_keybindName;
	std::function<void(T*)>					m_function;
};

template<typename T>
class HMoveCallback : public HCallback<T>
{
public:
	HMoveCallback(std::function<void(T*, int, int)> function)
		: m_function{std::move(function)} { }

	void Execute(T* invoker, int amountX, int amountY) {
		m_function(invoker, amountX, amountY);
	}

private:
	std::function<void(T*, int, int)>		m_function;
};

// May be dangerous if captured object goes out of scope, not tested
template<typename T>
inline static HKeybindCallback<T> FCreateBind_Basic_CF(std::string keybindName, std::function<const std::string&()> comperatorFunction, std::function<void(T*)> keybindFunction) {
	auto bind = HKeybindCallback<T>(
		[keybindFunction(std::move(keybindFunction))](T* invoker) { keybindFunction(invoker); },
		std::move(keybindName)
	);

	bind.SetFunctionComperator(std::move(comperatorFunction));

	return bind;
}

template<typename BaseClass, typename DerivedClass>
inline static HKeybindCallback<BaseClass> FCreateBind_Polymorphic(std::string keybindName, std::string comperatorName, std::function<void(DerivedClass*)> keybindFunction) {
	auto bind = HKeybindCallback<BaseClass>(
		[keybindFunction(std::move(keybindFunction))](BaseClass* invoker) { keybindFunction(static_cast<DerivedClass*>(invoker)); },
		std::move(keybindName)
	);

	bind.SetStringComperator(std::move(comperatorName));

	return bind;
}

// May be dangerous if captured object goes out of scope, not tested
template<typename BaseClass, typename DerivedClass>
inline static HKeybindCallback<BaseClass> FCreateBind_Polymorphic_CF(std::string keybindName, std::function<const std::string&()> comperatorFunction, std::function<void(DerivedClass*)> keybindFunction) {
	auto bind = HKeybindCallback<BaseClass>(
		[keybindFunction(std::move(keybindFunction))](BaseClass* invoker) { keybindFunction(static_cast<DerivedClass*>(invoker)); },
		std::move(keybindName)
	);

	bind.SetFunctionComperator(std::move(comperatorFunction));

	return bind;
}

inline static HKeybindCallback<void> FCreateBind_Plain(std::string keybindName, std::function<void()> keybindFunction) {
	auto bind = HKeybindCallback<void>(
		[keybindFunction(std::move(keybindFunction))](void* invoker) { keybindFunction(); },
		std::move(keybindName)
	);

	return bind;
}

template<typename BaseClass, typename DerivedClass>
inline static HMoveCallback<BaseClass> FMoveCallback_Polymorphic(std::string comperatorName, std::function<void(DerivedClass*, int, int)> moveFunction) {
	auto bind = HMoveCallback<BaseClass>(
		[keybindFunction(std::move(moveFunction))](BaseClass* invoker, int amountX, int amountY) { keybindFunction(static_cast<DerivedClass*>(invoker), amountX, amountY); }
	);

	bind.SetStringComperator(std::move(comperatorName));

	return bind;
}

} // namespace HE
