#include "HKeyboard.h"

#include "HMouse.h"

#ifdef _WIN32
#include <windows.h>
#endif

#include "../Modules/HWindow.h"

namespace HE {

bool HMouse::m_positionBuffered = false;
Vec2i HMouse::m_bufferedMousePos{0, 0};
Vec2i HMouse::m_posDifference{0, 0};
Vec2i HMouse::m_center{0, 0};
	
#ifdef _WIN32

bool HMouse::IsButtonDown(EMouseButton mouseButton) {
	if (GetAsyncKeyState(static_cast<unsigned int>(mouseButton)) & 0x8000) {
		return true;
	}
	
	return false;
}

Vec2i HMouse::GetMousePosition() {
	POINT mousePos;

	// This can return false indicating a problem with retrieving the mouse position, GetLastError() can state the cause of the problem
	GetCursorPos(&mousePos);

	return Vec2i{mousePos.x, mousePos.y};
}

void HMouse::DisplayCursor(bool display) {
	ShowCursor(display);
}

void HMouse::RecalculateCenter() {

}

Vec2i HMouse::GetPositionDifference() {
	return m_posDifference;
}

void HMouse::NotifyDisplayChange(HWindow* window) {
	switch (window->GetScreenMode()) {
		case EScreenMode::Borderless:
		case EScreenMode::Fullscreen:
		{
			auto resolution = window->GetRenderResolution();
			Vec2f center{static_cast<float>(resolution[0]) / 2.f, static_cast<float>(resolution[1]) / 2.f};

			// Floors
			m_center[0] = static_cast<int>(center[0]);
			m_center[1] = static_cast<int>(center[1]);
		}
		break;
		case EScreenMode::WindowedFixed:
		case EScreenMode::WindowedFixedNoBorder:
		case EScreenMode::WindowedResizable:
		{
			auto resolution = window->GetRenderResolution();
			auto position = window->GetWindowLocation();
			Vec2f center{static_cast<float>(resolution[0]) / 2.f, static_cast<float>(resolution[1]) / 2.f};

			// Floors
			m_center[0] = position[0] + static_cast<int>(center[0]);
			m_center[1] = position[1] + static_cast<int>(center[1]);
		}
		break;
	}
}

void HMouse::BufferPosition(HWindow* window) {
	POINT mousePos;

	GetCursorPos(&mousePos);

	if (!window->HasFocus()) {
		// Don't update the mouse position if we have no focus, we still set the bufferedPosition so we get no distortion once we tab in again..
		m_bufferedMousePos[0] = mousePos.x;
		m_bufferedMousePos[1] = mousePos.y;

		return;
	}

	if (!m_positionBuffered) {
		// It's the first time we take the coords, we don't have a position buffered and just skip setting the posDifference for this update, we can only get it
		// from the second update onwards..
		m_positionBuffered = true;
	}
	else {
		m_posDifference[0] = 0;
		m_posDifference[1] = 0;
		if (mousePos.x != m_center[0]) {
			m_posDifference[0] = mousePos.x - m_bufferedMousePos[0];
		}
		if (mousePos.y !=  m_center[1]) {
			m_posDifference[1] = mousePos.y - m_bufferedMousePos[1];
		}
	}

	m_bufferedMousePos[0] = mousePos.x;
	m_bufferedMousePos[1] = mousePos.y;

	// TODO if locked
	SetCursorPos(m_center[0], m_center[1]);
}

#elif __linux__

#endif
	



}
