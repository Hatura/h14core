#pragma once

#include "../Math/Vec.h"

namespace HE {

#ifdef _WIN32

#elif __linux__

#endif

enum class EMouseButton {
	Left = 0x01,
	Right = 0x02,
	Middle = 0x04,
	MouseAlt1 = 0x05,
	MouseAlt2 = 0x06
};

class HWindow;

class HMouse
{
public:
	static bool IsButtonDown(EMouseButton mouseButton);

	static void BufferPosition(HWindow* window);
	static Vec2i GetPositionDifference();

	static void NotifyDisplayChange(HWindow* window);

	static Vec2i GetMousePosition();

	static void DisplayCursor(bool display);

private:
	static void RecalculateCenter();

	static bool m_positionBuffered;
	static Vec2i m_bufferedMousePos;
	static Vec2i m_posDifference;
	static Vec2i m_center;
};

} // namespace HE
