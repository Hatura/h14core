#pragma once

namespace HE {

// I don't know how linux retrieves keyboard states, but this needs a linux definition, possibly the same way but with linux control ids?
// We use this to quickly get a status using GetAsyncKeyState() in Windows, we could also do 60-100 if/else or switch statements, but this
// should be alot faster!
#ifdef _WIN32
enum class EKeyboardKey : unsigned int {
	_0 = 0x30, _1 = 0x31, _2 = 0x32, _3 = 0x33, _4 = 0x34, _5 = 0x35, _6 = 0x36, _7 = 0x37, _8 = 0x38, _9 = 0x39,
	A = 0x41, B = 0x42, C = 0x43, D = 0x44, E = 0x45, F = 0x46, G = 0x47, H = 0x48, I = 0x49, J = 0x4A,
	K = 0x4B, L = 0x4C, M = 0x4D, N = 0x4E, O = 0x4F, P = 0x50, Q = 0x51, R = 0x52, S = 0x53, T = 0x54,
	U = 0x55, V = 0x56, W = 0x57, X = 0x58, Y = 0x59, Z = 0x5A,

	LEFTARROW = 0x25, UPARROW = 0x26, RIGHTARROW = 0x27, DOWNARROW = 0x28,
	
	F1 = 0x70, F2 = 0x71, F3 = 0x72, F4 = 0x73, F5 = 0x74, F6 = 0x75, F7 = 0x76, F8 = 0x77, F9 = 0x78, F10 = 0x79, F11 = 0x7A, F12 = 0x7B,
	SPACE = 0x20, ESC = 0x1B, LSTRG = 0xA2, RSTRG = 0xA3, LALT = 0x12, LSHIFT = 0xA0, RSHIFT = 0xA1, CAPS = 0x14, TAB = 0x09, RETURN = 0x0D, BACKSPACE = 0x08
};
#elif __linux__
enum class EKeyboardKey : unsigned int {
	_0, _1, _2, _3, _4, _5, _6, _7, _8, _9,
	A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z,
	F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11, F12,
	SPACE, ESC, LSTRG, RSTRG, LALT, RALT, LSHIFT, RSHIFT, CAPS, TAB, RETURN, BACKSPACE
};
#endif

class HKeyboard
{
public:
	static bool IsKeyDown(EKeyboardKey key);
};

} // namespace HE
