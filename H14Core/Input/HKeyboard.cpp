#include "HKeyboard.h"

#ifdef _WIN32
#include <windows.h>
#endif

namespace HE {
	
#ifdef _WIN32
	bool HKeyboard::IsKeyDown(EKeyboardKey key) {
		if (GetAsyncKeyState(static_cast<unsigned int>(key)) & 0x8000) {
			return true;
		}
		
		return false;
	}
#elif __linux__
	//... missing implementation
	bool HKeyboard::IsKeyDown(EKeyboardKey key) {
		return false;
	}
#endif

}
