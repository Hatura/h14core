#pragma once

#include <string>
#include <map>
#include <set>
#include <vector>
#include <iterator>

#include "HKeyboard.h"
#include "HMouse.h"

#include "HBind.h"

namespace HE {

enum class EInputBehavior {
	Down,
	DownTriggerOnce,
	Up
};

template<typename T>
class HInputController;

template<typename T>
struct SButtonInfo {
	friend bool operator< (const SButtonInfo<T>& lhs, const SButtonInfo<T>& rhs) {
		return lhs.m_button < rhs.m_button;
	}

	typename T::button m_button;
	EInputBehavior m_behavior;
};

// Need inheritance to use std::is_base_of in HInpuDevice Ctor
class TInputDevice {};

class TKeyboardDevice : public TInputDevice {
public:
	using button = EKeyboardKey;

};

class TMouseDevice : public TInputDevice {
public:
	using button = EMouseButton;
};

class TGamePadDevice : public TInputDevice {};

template <typename T, typename U>
class HInputDevice {
public:
	HInputDevice()
		: m_locationInitialized{false}
		, m_previousLocation{0, 0} {
		static_assert(std::is_base_of<TInputDevice, T>::value && !std::is_same<TInputDevice, T>::value, "No default class HInputDevice available, use specializations!");
	}

	using bindIterator = typename std::multimap<SButtonInfo<T>, std::string>::iterator;

	void MapButton(typename T::button button, EInputBehavior behavior, std::string keybindName) {
		SButtonInfo<T> buttonInfo;
		buttonInfo.m_button = button;
		buttonInfo.m_behavior = behavior;

		m_buttonMappings.insert(std::make_pair(std::move(buttonInfo), std::move(keybindName)));
	}

	bool CheckStatus(const bindIterator& position) {
		return true;
	}

	auto cbegin() const {
		return m_buttonMappings.cbegin();
	}

	auto cend() const {
		return m_buttonMappings.cbegin();
	}

	void PollKeybinds(HInputController<U>* processor, U* invoker, bool usingComperator, const std::string& comperator) {
		for (auto it = m_buttonMappings.begin(); it != m_buttonMappings.end(); ++it) {
			bool keyDown = IsButtonDown(it->first.m_button);
			bool insertedThisParse = false;

			if (keyDown) {
				auto itf = std::find(m_pressedButtons.begin(), m_pressedButtons.end(), it->first.m_button);

				if (itf == m_pressedButtons.end()) {
					insertedThisParse = true;
					m_pressedButtons.insert(it->first.m_button);
				}
			}

			if (it->first.m_behavior == EInputBehavior::Down) {
				if (keyDown) {
					processor->XCheckAndInvoke(it->second, invoker, usingComperator, comperator);
				}
			}
			else if (it->first.m_behavior == EInputBehavior::DownTriggerOnce) {
				if (keyDown) {
					// Check if key is already buffered
					if (insertedThisParse) {
						processor->XCheckAndInvoke(it->second, invoker, usingComperator, comperator);
					}
				}
			}
			else if (it->first.m_behavior == EInputBehavior::Up) {
				auto itk = std::find(m_pressedButtons.begin(), m_pressedButtons.end(), it->first.m_button);

				if (itk != m_pressedButtons.end() && !keyDown) {
					processor->XCheckAndInvoke(it->second, invoker, usingComperator, comperator);
				}
			}

			if (!keyDown) {
				m_pressedButtons.erase(it->first.m_button);
			}
		}
	}

	template<typename _T = T>
	std::enable_if_t<std::is_same<_T, TMouseDevice>::value, Vec2i> PollMoveDifference() {
		if (!m_locationInitialized) {
			m_previousLocation = std::move(HMouse::GetMousePosition());
			m_locationInitialized = true;

			return Vec2i{0, 0};
		}
		else {
			auto newPosition = HMouse::GetMousePosition();
			//if (newPosition == m_previousLocation) {
			//	return Vec2i{0, 0};
			//}
			auto positionDifference = m_previousLocation - newPosition;
			m_previousLocation = newPosition;
			return positionDifference;
		}
	}

private:
	template<typename _T = T>
	std::enable_if_t<std::is_same<_T, TKeyboardDevice>::value, bool> IsButtonDown(EKeyboardKey key) {
		return HKeyboard::IsKeyDown(key);
	}

	template<typename _T = T>
	std::enable_if_t<std::is_same<_T, TMouseDevice>::value, bool> IsButtonDown(EMouseButton button) {
		return HMouse::IsButtonDown(button);
	}

	std::multimap<SButtonInfo<T>, std::string> m_buttonMappings;

	std::set<typename T::button> m_pressedButtons;

	bool m_locationInitialized;
	Vec2i m_previousLocation;
};

//enum class EInputType {
//	Keyboard = 1,
//	Mouse = 2,
//	Gamepad = 4
//};
//
//inline EInputType operator| (EInputType lhs, EInputType rhs) {
//	return static_cast<EInputType>(static_cast<unsigned int>(lhs) | static_cast<unsigned int>(rhs));
//}

template<typename T = void>
class HInputController 
{
public:
	HInputController()
		: m_usingKeyboard{false}
		, m_usingMouse{false}
		, m_usingGamePad{false} {

	}

	template<typename U>
	void RegisterMoveDevice() {
		static_assert(true, "Error: Use specialized functions with RegisterMoveCallback<Type>(...) and make sure the device supports moving!");
	}

	template<>
	void RegisterMoveDevice<TMouseDevice>() {
		m_moveDevices.insert(EMoveDevice::Mouse);
	}

	template<>
	void RegisterMoveDevice<TGamePadDevice>() {
		m_moveDevices.insert(EMoveDevice::Gamepad);
	}

	// Possibly fastest implementation .. callback should be anything that can construct a std::function<void(int, int)>
	template<typename U>
	void RegisterMoveCallback(U&& callback) {
		m_moveCallbacks.emplace_back(std::forward<U>(callback));
	}

	void MapButton(EKeyboardKey button, EInputBehavior behavior, std::string keybindName) {
		m_usingKeyboard = true;
		m_keyboardDevice.MapButton(button, behavior, std::move(keybindName));
	}

	void MapButton(EMouseButton button, EInputBehavior behavior, std::string keybindName) {
		m_usingMouse = true;
		m_mouseDevice.MapButton(button, behavior, std::move(keybindName));
	}

	template<typename U>
	void RegisterKeybindCallback(U&& callback) {
		m_keybindCallbacks.emplace_back(std::forward<U>(callback));
	}

	template<typename _T = T>
	std::enable_if_t<std::is_same<_T, void>::value, void> PollInput() {
		ParseInput(nullptr, false);
	}

	template<typename _T = T>
	std::enable_if_t<!std::is_same<_T, void>::value, void> PollInput(T* invoker) {
		ParseInput(invoker, false);
	}

	template<typename _T = T>
	std::enable_if_t<!std::is_same<_T, void>::value, void> PollInput(T* invoker, const std::string& comperator) {
		ParseInput(invoker, true, comperator);
	}

	// Called from HInputDevice currently because of the shitty workaround..
	void XCheckAndInvoke(const std::string& keybindName, T* invoker, bool usingComperator, const std::string& comperator) {
		auto itb = std::find_if(m_keybindCallbacks.begin(), m_keybindCallbacks.end(), [&keybindName] (HKeybindCallback<T>& bind) {
			if (bind.GetKeybindName() == keybindName) {
				return true;
			}

			return false;
		});

		if (itb == m_keybindCallbacks.end()) {
			// No match
			return;
		}

		if (usingComperator) {
			if (itb->HasComperator()) {
				if (comperator == itb->GetComperatorString()) {
					// Compare matching
					itb->Execute(invoker);
					// return true;
				}
			}
			else {
				// No saved comperator (nothing to compare to)
			}
		}
		else {
			itb->Execute(invoker);
			// return true;
		}

		return;	// false;
	}

private:
	// Yeah we need another enum to assign the move callbacks - but we can hide it atleast
	enum class EMoveDevice {
		Mouse,
		Gamepad
	};

	// This is not very effective yet .. we just pass two pointers and a bool but we do it 2 times ..
	// An alternative would be buffering the input, but that's cancer if this needs to be threadsafe anytime..
	// The other alternative is to let the HInputDevice support begin() and end() iterations
	void ParseInput(T* invoker, bool usingComperator, const std::string& comperator = "") {
		Vec2i locationDifference{0, 0};

		if (m_usingKeyboard) {
			m_keyboardDevice.PollKeybinds(this, invoker, usingComperator, comperator);

			for (auto it = m_keyboardDevice.cbegin(); it != m_keyboardDevice.cend(); ++it) {

			}
		}

		if (m_usingMouse) {
			m_mouseDevice.PollKeybinds(this, invoker, usingComperator, comperator);

			locationDifference = HMouse::GetPositionDifference();
		}

		if (locationDifference[0] != 0 || locationDifference[1] != 0) {
			for (auto it = m_moveCallbacks.begin(); it != m_moveCallbacks.end(); ++it) {
				if (!usingComperator) {
					it->Execute(invoker, locationDifference[0], locationDifference[1]);
				}
				else if (usingComperator) {
					if (it->HasComperator()) {
						if (comperator == it->GetComperatorString()) {
							it->Execute(invoker, locationDifference[0], locationDifference[1]);
						}
					}
					else {
						// No saved comperator
					}
				}
			}
		}
	}

	bool m_usingKeyboard;
	bool m_usingMouse;
	bool m_usingGamePad;

	HInputDevice<TKeyboardDevice, T> m_keyboardDevice;
	HInputDevice<TMouseDevice, T> m_mouseDevice;

	std::vector<HKeybindCallback<T>> m_keybindCallbacks;

	std::set<EMoveDevice> m_moveDevices;
	std::vector<HMoveCallback<T>> m_moveCallbacks;
};

template<typename T>
inline static HCallback<T> FCreateBind_Basic(std::string keybindName, std::string comperatorName, std::function<void(T*)> keybindFunction) {
	auto bind = HCallback<T>(
		[keybindFunction(std::move(keybindFunction))](T* invoker) { keybindFunction(invoker); },
		std::move(keybindName)
	);

	bind.SetStringComperator(std::move(comperatorName));

	return bind;
}

} // namespace HE
