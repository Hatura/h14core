/*
* For testing purposes only
*/

#include <iostream>
#include <cstdio>
#include <memory>
#include <chrono> // test
#include "Glew/GL/glew.h"

#ifdef _WIN32
#include <gl/GL.h>
#elif __linux__
#include <GL/gl.h>
#endif

#include "HGame.h"
#include "Modules/HLogger.h"
#include "Modules/HWindow.h"
#include "Modules/HRenderer.h"
#include "Math/Math.h"
#include "Components/HActor.h"
#include "Components/HSprite.h"
#include "Components/HComponentCamera.h"
#include "Components/UnitController/HPlayerController.h"

#include "HPuppet.h"

#include "../zlib/zlib.h"

class MyGame {
public:
	MyGame()
		: m_game(std::make_unique<HE::HGame>("MyGame_Test", 1280, 720, HE::EScreenMode::WindowedResizable, HE::Vec4f{0.0f, 0.0f, 0.0f, 1.0f})) {}

	~MyGame();

	void Run() {
		// Simple test loop, to be called from the game 
		using namespace HE;

		//z_stream stream;
		//deflate(&stream, 0);

		std::cout << "MyGame::Run()" << std::endl;

		RegisterActorType(m_game, HPuppet);

		auto* actor = m_game->GetModule<HComponentFactory>()->ConstructActorByName("HPuppet");

		auto* pc = m_game->GetModule<HGameMode>()->CreatePlayerController(1);

		auto cam1 = m_game->GetModule<HComponentFactory>()->ConstructActor<HComponentCamera>(m_game->GetModule<HRenderer>());
		pc->PossessActor(cam1);

		auto puppetComperatorFunction = [&actor] () -> decltype(auto) { return actor->GetActorName(); };
		auto camComperatorFunction = [&cam1] () -> decltype(auto) { return cam1->GetActorName(); };

		pc->MapButton(EKeyboardKey::M, EInputBehavior::Down, "puppetJump");
		pc->MapButton(EMouseButton::Middle, EInputBehavior::DownTriggerOnce, "puppetJump");
		pc->RegisterKeybindCallback(FCreateBind_Polymorphic<HActor, HPuppet>("puppetJump", "HPuppet", [] (HPuppet* puppet) { puppet->Puppet(); }));

		pc->MapButton(EKeyboardKey::A, EInputBehavior::Down, "puppetJump");
		pc->MapButton(EKeyboardKey::A, EInputBehavior::Down, "camStrafeLeft");
		pc->MapButton(EKeyboardKey::D, EInputBehavior::Down, "camStrafeRight");
		pc->MapButton(EKeyboardKey::W, EInputBehavior::Down, "camMoveForward");
		pc->MapButton(EKeyboardKey::S, EInputBehavior::Down, "camMoveBackward");
		pc->MapButton(EKeyboardKey::LEFTARROW, EInputBehavior::Down, "camRotateLeft");
		pc->MapButton(EKeyboardKey::RIGHTARROW, EInputBehavior::Down, "camRotateRight");

		pc->RegisterKeybindCallback(FCreateBind_Polymorphic<HActor, HComponentCamera>("camStrafeLeft", "HComponentCamera", [] (auto* cam) { cam->StrafeLeft(); }));
		pc->RegisterKeybindCallback(FCreateBind_Polymorphic<HActor, HComponentCamera>("camStrafeRight", "HComponentCamera", [] (auto* cam) { cam->StrafeRight(); }));
		pc->RegisterKeybindCallback(FCreateBind_Polymorphic<HActor, HComponentCamera>("camMoveForward", "HComponentCamera", [] (auto* cam) { cam->MoveForward(); }));
		pc->RegisterKeybindCallback(FCreateBind_Polymorphic<HActor, HComponentCamera>("camMoveBackward", "HComponentCamera", [] (auto* cam) { cam->MoveBackward(); }));

		pc->RegisterKeybindCallback(FCreateBind_Polymorphic<HActor, HComponentCamera>("camRotateLeft", "HComponentCamera", [] (auto* cam) { cam->RotateBy(Rad{0.01f}, Rad{}, Rad{}); }));
		pc->RegisterKeybindCallback(FCreateBind_Polymorphic<HActor, HComponentCamera>("camRotateRight", "HComponentCamera", [] (auto* cam) { cam->RotateBy(Rad{-0.01f}, Rad{}, Rad{}); }));

		pc->RegisterMoveCallback(FMoveCallback_Polymorphic<HActor, HComponentCamera>("HComponentCamera", [&] (HComponentCamera* cam, int amountX, int amountY) {
			cam->RotateBy(Rad{static_cast<float>(-amountX) * 0.0025f}, Rad{static_cast<float>(-amountY) * 0.0025f}, Rad{});


			//m_game->GetModule<HLogger>()->Log(ELogLevel::Debug, "Heading: ", cam->GetCamera()->GetHeading().GetValue(), ", Attitude: ", cam->GetCamera()->GetAttitude().GetValue());
			//m_game->GetModule<HLogger>()->Log(ELogLevel::Debug, "LookAt: ", cam->GetCamera()->GetLookAt());
		}));

		//actor->SetLifeTime(std::chrono::seconds(20));

		//for (unsigned int i = 1; i <= 200; ++i) {
		//	auto actor = m_game->GetModule<HComponentFactory>()->ConstructActor<HPuppet>();
		//	actor->SetLifeTime(std::chrono::milliseconds(i * 1000));
		//}

		Vec3f topLeft{0, 10.f, 0};
		Vec3f topRight{10.0f, 10.0f, 0};
		
		Mat44 scaleMat;
		scaleMat.InitIdentity().Scale(10.f);

		topLeft *= scaleMat;
		topRight = topRight * scaleMat;

		Quaternion quat{Degree{90.f}, Degree{0.f}, Degree{0.f}};

		m_game->GetModule<HLogger>()->Log(ELogLevel::Debug, "Left: ", topLeft, ", Right: ", topRight, "\n");

		while (m_game->Update()) {
			const HWindowEvent& events = m_game->GetEvents();
			while (events.HasNextEvent()) {
				switch (events.GetNextEvent()) {
					case EWindowEvent::Keyboard:
					{
						if (events.IsKeyDown(EKeyboardKey::ESC)) {
							m_game->RequestQuit(false, "ESC Key pressed");
						}
						if (events.IsKeyDown(EKeyboardKey::_1)) {
							cam1->_Possess();
							cam1->Possess(pc);
						}
						if (events.IsKeyDown(EKeyboardKey::_2)) {
							actor->Possess(pc);
						}
						if (events.IsKeyDown(EKeyboardKey::_3)) {
							m_game->GetModule<HLogger>()->Log(ELogLevel::Debug, "\nPosition: ", cam1->GetCamera()->GetEye(), "\nLookAt: ", cam1->GetCamera()->GetLookAt());
						}
						if (events.IsKeyDown(EKeyboardKey::_4)) {
							m_game->GetModule<HLogger>()->Log(ELogLevel::Debug, "\nMat: ", cam1->GetCamera()->GetViewMatrix(), "\nLookAt: ", cam1->GetCamera()->GetLookAt(), "\nEye: ", cam1->GetCamera()->GetEye());
						}
						break;
					}
					case EWindowEvent::WindowMessage:
					{
						if (events.GetWindowMessageEvent(EWindowMessage::Quit)) {
							
						}
						break;
					}
				}
			}
			// Loop
		}
	}

private:
	std::unique_ptr<HE::HGame> m_game;
};

MyGame::~MyGame() {

}

int main() {
	using namespace HE;

	std::cout << "Test main() starting!" << std::endl;

	MyGame game;
	game.Run();

	std::cout << "Test main() exiting!" << std::endl;

	return 0;
}
