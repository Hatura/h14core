#include "HGame.h"
#include "HPuppet.h"

#include "Modules\HLogger.h"

namespace HE {

void HPuppet::PostConstructInit() {
	SetActorName("HPuppet");

	AddComponent<HE::HSprite>();
}

void HPuppet::Puppet() {
	GetGame()->GetModule<HLogger>()->Log(ELogLevel::Debug, "Puppet do puppet!");
}

}