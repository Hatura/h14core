#pragma once

#include <mutex>

class HGame;

namespace HE {

class HObject
{
public:
	HObject();
	virtual ~HObject();

	friend class HObjectProtector;

protected:
	void SetGame(HGame* game);

	HGame* GetGame() const;

private:
	HGame* m_game;

	mutable std::mutex m_objectMutex;
};

}