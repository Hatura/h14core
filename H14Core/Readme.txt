Abbreviations / Prefixes used:
H = Engine internal class (example: HGame)
S = Engine internal struct (example: SThreadType_Stream)
E = Engine internal enum (example ETaskType)
F = Engine internal common function
XF = Internal function NOT intended for user call where hiding is not possible otherwise (example: XFConstructActorFromPredef)