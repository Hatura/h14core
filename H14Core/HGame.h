#pragma once

#include <memory>
#include <vector>
#include <algorithm>

#include "Math/Vec.h"

#include "Modules/HWindowExt/HNativeWindow.h"
#include "Modules/HWindowExt/HWindowEvent.h"

// Modules

namespace HE {

class HModule;
class HThreadPool;
class HLogger;
class HWindow;
class HGameMode;
class HRenderer;
class HComponentFactory;
class HResourceManager;
class HMaterialFactory;
class HShaderFactory;

/**
* HGame class
*
* [ ] Copyable
* [ ] Movable
*
**/
class HGame
{
public:
	HGame(std::string gameName = "Unnamed Game", unsigned int width = 1280, unsigned int height = 720, EScreenMode screenMode = EScreenMode::WindowedResizable, const Vec4f& backgroundColor = Vec4f{0.f, 0.f, 0.f, 1.f});
	virtual ~HGame();

	HGame(const HGame& other) = delete;
	HGame& operator= (const HGame& other) = delete;
	HGame(HGame&& other) = delete;
	HGame& operator= (HGame&& other) = delete;

	template<typename T, typename... Args>
	void RegisterModule(Args&&... args);

	template<typename T>
	T* GetModule();

	// Forwards to HFactory::Same
	//template<typename... Args>
	//HActor* ConstructActor(Args&&... args) {
	//	return m_factory->ConstructActor(std::forward<Args>(args)...);
	//}

	// Forward to HFactory::Same
	// Constructs everything else than an actor cause here direct ownership is needed? (TODO: Let's see if we need this later..)
	//template<typename T, typename... Args>
	//std::unique_ptr<T> ConstructComponent(Args&&... args) {
	//	return m_factory->ConstructComponent<T>(std::forward<Args>(args)...);
	//}

	template<typename T, typename... Args>
	void SetGameMode(Args&&... args);

	// Returns false if game is exiting, does one extra call after 
	bool Update();

	// error indicates that something happend we can't recover from, it logs the reason (if Logger is initialized) and calls std::terminate
	// if error is false we try to shut everything down in an orderly fashion
	void RequestQuit(bool error, const std::string& quitReason = "Unknown reason");

	bool IsRunning() const;

	// Forwards to HWindow->HNativeWindow->HWindowEvent..
	const HWindowEvent& GetEvents() const;
	
	HWindow* GetWindow();

private:
	std::string m_gameName;

	// Mandatory modules
	std::unique_ptr<HThreadPool> m_threadpool;
	std::unique_ptr<HLogger> m_logger;
	std::unique_ptr<HWindow> m_window;
	std::unique_ptr<HRenderer> m_renderer;
	std::unique_ptr<HGameMode> m_gameMode;
	std::unique_ptr<HComponentFactory> m_factory;
	std::unique_ptr<HResourceManager> m_resourceManager;
	std::unique_ptr<HMaterialFactory> m_materialFactory;
	std::unique_ptr<HShaderFactory> m_shaderFactory;

	std::vector<std::unique_ptr<HModule>> m_optionalModules;

	mutable bool m_exitGame;			// Used in logical const, flag is set one Update/Render after m_windowShutdown form HWindow is received
};

template<typename T, typename... Args>
void HGame::RegisterModule(Args&&... args) {
	//static_assert(std::is_base_of<HModule<EModuleType::Optional>, T>::value, "Trying to register class not base on HModule<Optional>! Remember, you don't need to register default modules!");

	// We can't use std::make_unique here because the constructor is private/protected and xmemory.h has no access..
	std::unique_ptr<T> tPtr(new T(std::forward<Args>(args)...));

	m_optionalModules.emplace_back(std::move(tPtr));
}

template<typename T, typename... Args>
void HGame::SetGameMode(Args&&... args) {
	if (m_gameMode != nullptr) {
		m_gameMode.reset();
	}

	m_gameMode = std::make_unique<T>(std::forward<Args>(args)...);
	// Not yet implemented
	//m_gameMode->OnInit();
}

template<typename T>
inline T* HGame::GetModule()  {
	auto it = std::find_if(m_optionalModules.begin(), m_optionalModules.end(), [this] (const auto& lhs) {
		if (typeid(*lhs) == typeid(T)) {
			return true;
		}

		return false;
	});

	return dynamic_cast<T*>(it->get());
}

template<>
inline HThreadPool* HGame::GetModule<HThreadPool>() {
	return m_threadpool.get();
}

template<>
inline HLogger* HGame::GetModule<HLogger>() {
	return m_logger.get();
}

template<>
inline HWindow* HGame::GetModule<HWindow>() {
	return m_window.get();
}

template<>
inline HGameMode* HGame::GetModule<HGameMode>() {
	return m_gameMode.get();
}

template<>
inline HComponentFactory* HGame::GetModule<HComponentFactory>() {
	return m_factory.get();
}

template<>
inline HResourceManager* HGame::GetModule<HResourceManager>() {
	return m_resourceManager.get();
}

template<>
inline HMaterialFactory* HGame::GetModule<HMaterialFactory>() {
	return m_materialFactory.get();
}

template<>
inline HShaderFactory* HGame::GetModule<HShaderFactory>() {
	return m_shaderFactory.get();
}

template<>
inline HRenderer* HGame::GetModule<HRenderer>() {
	return m_renderer.get();
}

} // namespace HE
