#pragma once

#include <algorithm>

#include "Mat.h"
#include "Quaternion.h"
#include "Vec.h"

namespace HE {

/**
* Math.h
*
* Multiplication between classes, Mat * Vec .. [and more?]
*
* Not the best solution for including yet, for now never include Mat.h or Vec.h but instead this (Math.h)
*
**/

// * operator for Vec * Mat
template<typename T, bool ProjMat, unsigned int Rows, unsigned int Cols, unsigned int Num>
inline auto operator* (const Vec<T, Num>& vec, const Mat<T, ProjMat, Rows, Cols>& mat) {
	static_assert(Cols == 4 && (Num == 3 || Num == 4), "Vector ~ Matrix operator * is only available for Vec<3> or Vec<4> * 4x4 Matrix!");

	Vec<T, Num> rVec = XFMultiplyMatVec(vec, mat);

	return rVec;
}

// *= operator for Vec * Mat
template<typename T, bool ProjMat, unsigned int Rows, unsigned int Cols, unsigned int Num>
inline auto operator*=(Vec<T, Num>& vec, const Mat<T, ProjMat, Rows, Cols>& mat) {
	static_assert(Cols == 4 && (Num == 3 || Num == 4), "Vector ~ Matrix operator *= is only available for Vec<3> or Vec<4> * 4x4 Matrix!");

	vec = XFMultiplyMatVec(vec, mat);

	return vec;
}

// For Vec<4> * 4x4
template<typename T, bool ProjMat, unsigned int Rows, unsigned int Cols, unsigned int Num>
inline std::enable_if_t<Cols == 4 && Num == 4, Vec<T, Num>> XFMultiplyMatVec (const Vec<T, Num>& vec, const Mat<T, ProjMat, Rows, Cols>& mat) {
	Vec<T, Num> rVec(
		vec[0] * mat[0][0] + vec[1] * mat[1][0] + vec[2] * mat[2][0] + mat[3][0],
		vec[0] * mat[0][1] + vec[1] * mat[1][1] + vec[2] * mat[2][1] + mat[3][1],
		vec[0] * mat[0][2] + vec[1] * mat[1][2] + vec[2] * mat[2][2] + mat[3][2],
		vec[0] * mat[0][3] + vec[1] * mat[1][3] + vec[2] * mat[2][3] + mat[3][3]
	);

	return rVec;
}

// For Vec<3> * 4x4 Non-Projection Matrix
template<typename T, bool ProjMat, unsigned int Rows, unsigned int Cols, unsigned int Num>
inline std::enable_if_t<Cols == 4 && Num == 3 && ProjMat == false, Vec<T, Num>> XFMultiplyMatVec(const Vec<T, Num>& vec, const Mat<T, ProjMat, Rows, Cols>& mat) {
	Vec<T, Num> rVec(
		vec[0] * mat[0][0] + vec[1] * mat[1][0] + vec[2] * mat[2][0] + mat[3][0],
		vec[0] * mat[0][1] + vec[1] * mat[1][1] + vec[2] * mat[2][1] + mat[3][1],
		vec[0] * mat[0][2] + vec[1] * mat[1][2] + vec[2] * mat[2][2] + mat[3][2]
		);

	return rVec;
}

// For Vec<3> * 4x4 Projection Matrix
template<typename T, bool ProjMat, unsigned int Rows, unsigned int Cols, unsigned int Num>
inline std::enable_if_t<Cols == 4 && Num == 3 && ProjMat == true, Vec<T, Num>> XFMultiplyMatVec(const Vec<T, Num>& vec, const Mat<T, ProjMat, Rows, Cols>& mat) {
	Vec<T, Num> rVec(
		vec[0] * mat[0][0] + vec[1] * mat[1][0] + vec[2] * mat[2][0] + mat[3][0],
		vec[0] * mat[0][1] + vec[1] * mat[1][1] + vec[2] * mat[2][1] + mat[3][1],
		vec[0] * mat[0][2] + vec[1] * mat[1][2] + vec[2] * mat[2][2] + mat[3][2]
	);

	T w = vec[0] * mat[0][3] + vec[1] * mat[1][3] + vec[2] * mat[2][3] + mat[3][3];

	rVec[0] /= w;
	rVec[1] /= w;
	rVec[2] /= w;

	return rVec;
}

// This is basically copied from gsm::lookAt (MIT license)
inline Mat44 FCreateLookAtMat(const Vec3f& position, const Vec3f& focus, const Vec3f& up) {
	auto fv = focus - position;
	fv.Normalize();

	auto sv = VecCross(fv, up);
	sv.Normalize();

	auto uv = VecCross(sv, fv);
	uv.Normalize();

	Mat44 lookAtMat;
	lookAtMat.InitIdentity();
	lookAtMat[0][0] = sv[0];
	lookAtMat[1][0] = sv[1];
	lookAtMat[2][0] = sv[2];
	lookAtMat[0][1] = uv[0];
	lookAtMat[1][1] = uv[1];
	lookAtMat[2][1] = uv[2];
	lookAtMat[0][2] = -fv[0];
	lookAtMat[1][2] = -fv[1];
	lookAtMat[2][2] = -fv[2];

	lookAtMat[3][0] = -VecDot(sv, position);
	lookAtMat[3][1] = -VecDot(uv, position);
	lookAtMat[3][2] =  VecDot(fv, position);

	return lookAtMat;
}

// Also basically copied from gsm::perspective (MIT license)
inline Mat44 FCreatePerspectiveMat(float fovRads, float aspectRatio, float nearDist, float farDist) {
	// TODO: Needs division by zero check!

	float halfFov = std::tan(fovRads * 0.5f);

	Mat44 perspectiveMat;
	perspectiveMat.Init();
	perspectiveMat[0][0] = 1 / (aspectRatio * halfFov);
	perspectiveMat[1][1] = 1 / halfFov;
	perspectiveMat[2][2] = -1 * (farDist + nearDist) / (farDist - nearDist);
	perspectiveMat[2][3] = -1 * 1.f;
	perspectiveMat[3][2] = -1 * (2.f * farDist * nearDist) / (farDist - nearDist);

	return perspectiveMat;
}

inline Mat44 FCreateFPSMat(const Vec3f& eye, const Rad& heading, const Rad& attitude) {
	float cosAttitude = std::cos(attitude.GetValue());
	float sinAttitude = std::sin(attitude.GetValue());
	float cosHeading = std::cos(heading.GetValue());
	float sinHeading = std::sin(heading.GetValue());

	Mat44 fpsMat;
	fpsMat.InitIdentity();

	Vec3f x{cosHeading, 0.f, -sinHeading};
	Vec3f y{sinHeading * sinAttitude, cosAttitude, cosHeading * sinAttitude};
	Vec3f z{sinHeading * cosAttitude, -sinAttitude, cosAttitude * cosHeading};

	fpsMat[0][0] = x[0];
	fpsMat[1][0] = x[1];
	fpsMat[2][0] = x[2];
	fpsMat[0][1] = y[0];
	fpsMat[1][1] = y[1];
	fpsMat[2][1] = y[2];
	fpsMat[0][2] = z[0];
	fpsMat[1][2] = z[1];
	fpsMat[2][2] = z[2];

	fpsMat[3][0] = -VecDot(x, eye);
	fpsMat[3][1] = -VecDot(y, eye);
	fpsMat[3][2] = -VecDot(z, eye);

	return fpsMat;
}

inline Mat44 FCreateDebugMat() {
	Mat44 debugMat;
	debugMat.Init();

	debugMat[0][0] = 1.f;
	debugMat[0][1] = 2.f;
	debugMat[0][2] = 3.f;
	debugMat[0][3] = 4.f;
	debugMat[1][0] = 5.f;
	debugMat[1][1] = 6.f;
	debugMat[1][2] = 7.f;
	debugMat[1][3] = 8.f;
	debugMat[2][0] = 9.f;
	debugMat[2][1] = 10.f;
	debugMat[2][2] = 11.f;
	debugMat[2][3] = 12.f;
	debugMat[3][0] = 13.f;
	debugMat[3][1] = 14.f;
	debugMat[3][2] = 15.f;
	debugMat[3][3] = 16.f;

	return debugMat;
}

} // namespace HE