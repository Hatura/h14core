#pragma once

#include "MathHelper.h"

#include "Vec.h"
#include "Mat.h"

namespace HE {

class Quaternion
{
public:
	Quaternion();
	Quaternion(float x, float y, float z, float w);
	Quaternion(Degree heading, Degree attitude, Degree bank);
	Quaternion(Rad heading, Rad attitude, Rad bank);
	
	// CTor for angle around axis
	Quaternion(Degree angle, const Vec3f& axis);

	// Used as degree angle, TODO: should we disallow this since it's no clear type (HRad/HDegree)?
	Quaternion(const Vec3f& angleVec);

	// TODO: Chaneg to free function if GCC can do it.. (See Mat or Vec comment on same operator)
	friend std::ostream& operator<< (std::ostream& out, const Quaternion& quat) {
		out << "Quaternion [" << quat.GetX() << ", " << quat.GetY() << ", " << quat.GetZ() << ", " << quat.GetW() << "]";

		return out;
	}

	void Set(float x, float y, float z, float w);

	float Magnitude() const;
	Quaternion& Normalize();
	Quaternion& Conjugate();

	Quaternion NormalizedCopy();
	Quaternion ConjugatedCopy();

	Mat44 ToRotationMatrix() const;
	Vec3f ToEuler() const;

	float GetX() const;
	float GetY() const;
	float GetZ() const;
	float GetW() const;

private:
	Quaternion& InitFromEuler(float degX, float degY, float degZ);

	float m_x, m_y, m_z, m_w;
};

inline Quaternion::Quaternion()
	: m_x{0.f}
	, m_y{0.f}
	, m_z{0.f}
	, m_w{1.f} {

}

inline Quaternion::Quaternion(float x, float y, float z, float w)
	: m_x{x}
	, m_y{y}
	, m_z{z}
	, m_w{w} {

}

inline Quaternion::Quaternion(Degree heading, Degree attitude, Degree bank) {
	InitFromEuler(heading.ToRad().GetValue(), attitude.ToRad().GetValue(), bank.ToRad().GetValue());
}

inline Quaternion::Quaternion(Rad heading, Rad attitude, Rad bank) {
	InitFromEuler(heading.GetValue(), attitude.GetValue(), bank.GetValue());
}

inline Quaternion::Quaternion(Degree angle, const Vec3f& axis) {
	float amp = std::sin(angle.GetValue() * 0.5f);

	m_x = amp * axis[0];
	m_y = amp * axis[1];
	m_z = amp * axis[2];
	m_w = std::cos(angle.GetValue() * 0.5f);
}

inline Quaternion::Quaternion(const Vec3f& angleVec) {
	InitFromEuler(angleVec[0], angleVec[1], angleVec[2]);
}

inline void Quaternion::Set(float x, float y, float z, float w) {
	m_x = x;
	m_y = y;
	m_z = z;
	m_w = w;
}

inline float Quaternion::Magnitude() const {
	float magnitude = std::sqrt(m_w * m_w + m_x * m_x + m_y * m_y + m_z * m_z);

	return magnitude;
}

inline Quaternion& Quaternion::Normalize() {
	float magFac = 1.f / Magnitude();

	m_x *= magFac;
	m_y *= magFac;
	m_z *= magFac;
	m_w *= magFac;

	return *this;
}

inline Quaternion& Quaternion::Conjugate() {
	m_x *= -1.f;
	m_y *= -1.f;
	m_z *= -1.f;

	return *this;
}

inline Quaternion Quaternion::NormalizedCopy() {
	float magFac = 1.f / Magnitude();

	Quaternion copy {
		GetX() * magFac,
		GetY() * magFac,
		GetZ() * magFac,
		GetW() * magFac
	};

	return copy;
}

inline Quaternion Quaternion::ConjugatedCopy() {
	Quaternion copy {
		GetX() * -1.f,
		GetY() * -1.f,
		GetZ() * -1.f,
		GetW()
	};

	return copy;
}

inline Quaternion& Quaternion::InitFromEuler(float heading, float attitude, float bank) {
	float c1 = std::cos(heading * 0.5f);
	float c2 = std::cos(attitude * 0.5f);
	float c3 = std::cos(bank * 0.5f);

	float s1 = std::sin(heading * 0.5f);
	float s2 = std::sin(attitude * 0.5f);
	float s3 = std::sin(bank * 0.5f);

	float c1c2 = c1 * c2;
	float s1s2 = s1 * s2;

	m_w = c1c2 * c3 - s1s2 * s3;
	m_x = c1c2 * s3 - s1s2 * c3;
	m_y = s1 * c2 * c3 + c1 * s2 * s3;
	m_z = c1 * s2 * c3 - s1 * c2 * s3;

	return *this;
}

inline Mat44 Quaternion::ToRotationMatrix() const {
	Mat44 mat;
	mat.InitIdentity();

	mat[0][0] = 1 - 2 * (m_y * m_y + m_z * m_z);
	mat[1][0] = 2 * (m_x * m_z - m_y * m_w);
	mat[2][0] = 2 * (m_z * m_w + m_x * m_y);
	mat[0][1] = 2 * (m_x * m_z + m_y * m_w);
	mat[1][1] = 1 - 2 * (m_x * m_x + m_y * m_y);
	mat[2][1] = 2 * (m_z * m_y - m_x * m_w);
	mat[0][2] = 2 * (m_x * m_y - m_z * m_w);
	mat[1][2] = 2 * (m_z * m_y + m_x * m_w);
	mat[2][2] = 1 - 2 * (m_z * m_z + m_x * m_x);

	return mat;
}

inline Vec3f Quaternion::ToEuler() const {
	float singularityTest = m_x * m_y + m_z * m_w;

	Vec3f eulerAngles{};

	if (singularityTest > 0.499) {
		eulerAngles[0] = 2 * std::atan2(m_x, m_w);
		eulerAngles[1] = Pi / 2.f;
		eulerAngles[2] = 0.f;
	}
	else if (singularityTest < -0.499) {
		eulerAngles[0] = -2 * std::atan2(m_x, m_w);
		eulerAngles[1] = Pi / 2.f;
		eulerAngles[2] = 0.f;
	}
	else {
		float xx = m_x * m_x;
		float yy = m_y * m_y;
		float zz = m_z * m_z;

		eulerAngles[0] = std::atan2(2 * m_y * m_w - 2 * m_x * m_z, 1 - 2 * yy - 2 * zz);
		eulerAngles[1] = std::asin(2 * singularityTest);
		eulerAngles[2] = std::atan2(2 * m_x * m_w - 2 * m_y * m_z, 1 - 2 * xx - 2 * zz);
	}

	return eulerAngles;
}

inline float Quaternion::GetX() const {
	return m_x;
}

inline float Quaternion::GetY() const {
	return m_y;
}

inline float Quaternion::GetZ() const {
	return m_z;
}

inline float Quaternion::GetW() const {
	return m_w;
}

// Free function operators for Quaternion

inline Quaternion operator* (const Quaternion& lhs, const Quaternion& rhs) {
	float x = rhs.GetW() * lhs.GetX() + rhs.GetX() * lhs.GetW() + rhs.GetY() * lhs.GetZ() - rhs.GetZ() * lhs.GetY();
	float y = rhs.GetW() * lhs.GetY() + rhs.GetY() * lhs.GetW() + rhs.GetZ() * lhs.GetX() - rhs.GetX() * lhs.GetZ();
	float z = rhs.GetW() * lhs.GetZ() + rhs.GetZ() * lhs.GetW() + rhs.GetX() * lhs.GetY() - rhs.GetY() * lhs.GetX();
	float w = rhs.GetW() * lhs.GetW() - rhs.GetX() * lhs.GetX() - rhs.GetY() * lhs.GetY() - rhs.GetZ() * lhs.GetZ();

	Quaternion quat{x, y, z, w};

	return quat;
}

inline Quaternion& operator*= (Quaternion& lhs, const Quaternion& rhs) {
	lhs.Set(
		rhs.GetW() * lhs.GetX() + rhs.GetX() * lhs.GetW() + rhs.GetY() * lhs.GetZ() - rhs.GetZ() * lhs.GetY(),
		rhs.GetW() * lhs.GetY() + rhs.GetY() * lhs.GetW() + rhs.GetZ() * lhs.GetX() - rhs.GetX() * lhs.GetZ(),
		rhs.GetW() * lhs.GetZ() + rhs.GetZ() * lhs.GetW() + rhs.GetX() * lhs.GetY() - rhs.GetY() * lhs.GetX(),
		rhs.GetW() * lhs.GetW() - rhs.GetX() * lhs.GetX() - rhs.GetY() * lhs.GetY() - rhs.GetZ() * lhs.GetZ()
	);

	return lhs;
}

} // namespace HE