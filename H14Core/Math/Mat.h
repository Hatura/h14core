#pragma once

#include <cmath>
#include <ostream>
#include <typeinfo>

#include "MathHelper.h"

namespace HE {

/**
* Mat class
*
* Templatized matrix class (Row-major?)
*
* Methods only available for 3x3 and 4x4 orthogonal matrices: 
* Rotate(), RotateAroundX(), RotateAroundY(), RotateAroundZ()
*
* Methods only available for 4x4 orthogonal matrices:
* Translate()
*
* [x] Copyable
* [x] Movable
*
*/
template <typename T, bool ProjMat = false, unsigned int Rows = 4, unsigned int Columns = 4>
class Mat
{
public:
	Mat() {
		static_assert(Rows == Columns, "Only square Matrices allowed for now (Rows must be == Cols)");
	}

	Mat(const Mat& other) = default;
	Mat& operator= (const Mat& other) = default;
	Mat(Mat&& other) = default;
	Mat& operator= (Mat&& other) = default;

	const T* operator[] (unsigned int i) const {
		return m_data[i];
	}

	T* operator[] (unsigned int i) {
		return m_data[i];
	}

	//template<typename U, bool OtherProjMat, unsigned int OtherRows, unsigned int OtherColumns>
	//Mat(const Mat<U, OtherProjMat, OtherRows, OtherColumns>& other) {
	//	static_assert(U == T, "Can't copy construct Matrix: Matrices must be of same type!");
	//	static_assert()
	//}

	template<typename U, bool U_ProjMat, unsigned int U_Rows, unsigned int U_Cols>
	friend auto operator* (const Mat& lhs, const Mat<U, U_ProjMat, U_Rows, U_Cols>& rhs) {
		static_assert(lhs.m_columns == rhs.m_rows, "Can only multiply matrices of type (m, p) * (p, n) | [m == n]!");

		Mat<U, ProjMat, Rows, U_Cols> nMat;
		nMat.Init();

		for (unsigned int i = 0; i < lhs.m_rows; ++i) {
			for (unsigned int j = 0; j < rhs.m_columns; ++j) {
				for (unsigned int m = 0; m < lhs.m_columns; ++m) {
					nMat[i][j] += lhs[i][m] * rhs[m][j];
				}
			}
		}

		return nMat;
	}

	// Scalar multiplication
	friend auto operator* (Mat lhs, T rhs) {
		Mat<T, ProjMat, Rows, Columns> rMat;

		for (unsigned int i = 0; i < lhs.m_rows; ++i) {
			for (unsigned int j = 0; j < lhs.m_columns; ++j) {
				rMat.m_data[i][j] = lhs[i][j] * rhs;
			}
		}

		return rMat;
	}

	friend std::ostream& operator<< (std::ostream& os, const Mat& mat) {
		for (unsigned int i = 0; i < mat.m_rows; ++i) {
			for (unsigned int j = 0; j < mat.m_columns; ++j) {
				os << mat[i][j];
				if (j + 1 < mat.m_columns) {
					os << ", ";
				}
			}

			if (i + 1 < mat.m_rows) {
				os << std::endl;
			}
		}

		return os;
	}

	void Init() {
		for (unsigned int i = 0; i < Rows; ++i) {
			for (unsigned int j = 0; j < Columns; ++j) {
				m_data[i][j] = 0;
			}
		}
	}

	auto& InitIdentity() {
		static_assert(Rows == Columns, "Can only transform to a identity-matrix if the matrix is a square-matrix (rows == columns");

		for (int i = 0; i < Rows; ++i) {
			for (int j = 0; j < Columns; ++j) {
				if (i == j) {
					m_data[i][j] = 1.f;
				}
				else {
					m_data[i][j] = 0;
				}
			}
		}

		return *this;
	}

	// Scale Uniform
	std::enable_if_t<Rows == Columns && (Rows == 3 || Rows == 4), Mat<T, ProjMat, Rows, Columns>&> Scale(float uniformFactor) {
		Mat<T, ProjMat, Rows, Rows> scaleMat;
		scaleMat.InitIdentity();

		scaleMat[0][0] = uniformFactor;
		scaleMat[1][1] = uniformFactor;
		scaleMat[2][2] = uniformFactor;

		*this = *this * scaleMat;

		return *this;
	}

	// Scale X, Y, Z
	std::enable_if_t<Rows == Columns && (Rows == 3 || Rows == 4), Mat<T, ProjMat, Rows, Columns>&> Scale(float onX, float onY, float onZ) {
		Mat<T, ProjMat, Rows, Rows> scaleMat;
		scaleMat.InitIdentity();

		scaleMat[0][0] = onX;
		scaleMat[1][1] = onY;
		scaleMat[2][2] = onZ;

		*this = *this * scaleMat;

		return *this;
	}

	// Scale X
	std::enable_if_t<Rows == Columns && (Rows == 3 || Rows == 4), Mat<T, ProjMat, Rows, Columns>&> ScaleX(float factor) {
		Mat<T, ProjMat, Rows, Rows> scaleMat;
		scaleMat.InitIdentity();

		scaleMat[0][0] = factor;

		*this = *this * scaleMat;

		return *this;
	}

	// Scale Y
	std::enable_if_t<Rows == Columns && (Rows == 3 || Rows == 4), Mat<T, ProjMat, Rows, Columns>&> ScaleY(float factor) {
		Mat<T, ProjMat, Rows, Rows> scaleMat;
		scaleMat.InitIdentity();

		scaleMat[1][1] = factor;

		*this = *this * scaleMat;

		return *this;
	}

	// Scale Z
	std::enable_if_t<Rows == Columns && (Rows == 3 || Rows == 4), Mat<T, ProjMat, Rows, Columns>&> ScaleZ(float factor) {
		Mat<T, ProjMat, Rows, Rows> scaleMat;
		scaleMat.InitIdentity();

		scaleMat[2][2] = factor;

		*this = *this * scaleMat;

		return *this;
	}

	std::enable_if_t<Rows == Columns && Rows == 4, Mat<T, ProjMat, Rows, Columns>&> Translate(float x, float y, float z) {
		m_data[3][0] += x;
		m_data[3][1] += y;
		m_data[3][2] += z;

		return *this;
	}

	// Angle in degrees, Rotation Order: X -> Y -> Z
	std::enable_if_t<Rows == Columns && (Rows == 3 || Rows == 4), Mat<T, ProjMat, Rows, Columns>&> Rotate(float aroundXAxis, float aroundYAxis, float aroundZAxis) {
		// Could use internal functions RotateAroundX, Y, Z but reusing the matrix is faster!

		Mat<T, ProjMat, Rows, Rows> rotMatrix;

		// X
		if (aroundXAxis > 0) {
			rotMatrix.InitIdentity();
			rotMatrix[1][1] = std::cos(FDegreeToRad(aroundXAxis));
			rotMatrix[2][1] = -1 * std::sin(FDegreeToRad(aroundXAxis));
			rotMatrix[1][2] = std::sin(FDegreeToRad(aroundXAxis));
			rotMatrix[2][2] = std::cos(FDegreeToRad(aroundXAxis));

			*this = *this * rotMatrix;
		}

		// Y
		if (aroundYAxis > 0) {
			rotMatrix.InitIdentity();
			rotMatrix[0][0] = std::cos(FDegreeToRad(aroundYAxis));
			rotMatrix[2][0] = -1 * std::sin(FDegreeToRad(aroundYAxis));
			rotMatrix[0][2] = std::sin(FDegreeToRad(aroundYAxis));
			rotMatrix[2][2] = std::cos(FDegreeToRad(aroundYAxis));

			*this = *this * rotMatrix;
		}

		// Z
		if (aroundZAxis > 0) {
			rotMatrix.InitIdentity();
			rotMatrix[0][0] = std::cos(FDegreeToRad(aroundZAxis));
			rotMatrix[1][0] = -1 * std::sin(FDegreeToRad(aroundZAxis));
			rotMatrix[0][1] = std::sin(FDegreeToRad(aroundZAxis));
			rotMatrix[1][1] = std::cos(FDegreeToRad(aroundZAxis));

			*this = *this * rotMatrix;
		}

		return *this;
	}

	std::enable_if_t<Rows == Columns && (Rows == 3 || Rows == 4), Mat<T, ProjMat, Rows, Columns>&> RotateAroundX(float angle) {
		Mat<float, false> rotMatrix;
		rotMatrix.InitIdentity();
		rotMatrix[1][1] = std::cos(FDegreeToRad(angle));
		rotMatrix[2][1] = -1 * std::sin(FDegreeToRad(angle));
		rotMatrix[1][2] = std::sin(FDegreeToRad(angle));
		rotMatrix[2][2] = std::cos(FDegreeToRad(angle));

		*this = *this * rotMatrix;
		return *this;
	}

	std::enable_if_t<Rows == Columns && (Rows == 3 || Rows == 4), Mat<T, ProjMat, Rows, Columns>&> RotateAroundY(float angle) {
		Mat<float, false> rotMatrix;
		rotMatrix.InitIdentity();
		rotMatrix[0][0] = std::cos(FDegreeToRad(angle));
		rotMatrix[2][0] = -1 * std::sin(FDegreeToRad(angle));
		rotMatrix[0][2] = std::sin(FDegreeToRad(angle));
		rotMatrix[2][2] = std::cos(FDegreeToRad(angle));

		*this = *this * rotMatrix;
		return *this;
	}

	std::enable_if_t<Rows == Columns && (Rows == 3 || Rows == 4), Mat<T, ProjMat, Rows, Columns>&> RotateAroundZ(float angle) {
		Mat<float, false> rotMatrix;
		rotMatrix.InitIdentity();
		rotMatrix[0][0] = std::cos(FDegreeToRad(angle));
		rotMatrix[1][0] = -1 * std::sin(FDegreeToRad(angle));
		rotMatrix[0][1] = std::sin(FDegreeToRad(angle));
		rotMatrix[1][1] = std::cos(FDegreeToRad(angle));

		*this = *this * rotMatrix;
		return *this;
	}

	// Transpose the matrix (inverse for orthogonal), if you want a copy use CloneTranspoed()!
	std::enable_if_t<Rows == Columns && (Rows == 3 || Rows == 4), Mat<T, ProjMat, Rows, Columns>&> Transpose() {
		Mat<T, ProjMat, Rows, Columns> tempMat = *this;

		for (unsigned int i = 0; i < Rows; ++i) {
			for (unsigned int j = 0; j < Columns; ++j) {
				m_data[i][j] = tempMat[j][i];
			}
		}

		return *this;
	}

	// Return a transposed copy of the matrix
	std::enable_if_t<Rows == Columns && (Rows == 3 || Rows == 4), Mat<T, ProjMat, Rows, Columns>> CloneTransposed() {
		Mat<T, ProjMat, Rows, Columns> tempMat;
		
		for (unsigned int i = 0; i < Rows; ++i) {
			for (unsigned int j = 0; j < Columns; ++j) {
				tempMat[i][j] = m_data[j][i];
			}
		}

		return tempMat;
	}

	// We need these for extern access (?)
	using type = T;

	static constexpr unsigned int m_rows = Rows;
	static constexpr unsigned int m_columns = Columns;
	static constexpr bool m_projMat = ProjMat;

private:
	
	T m_data[Rows][Columns];
};

using Mat44 = Mat<float, false>;
using ProjMat44 = Mat<float, true>;
using Mat33 = Mat<float, false, 3, 3>;
using ProjMat33 = Mat<float, true, 3, 3>;

} // namespace HE
