/*
* Copyright 2015 @ Hatur (hawsk@web.de)
*/

#pragma once

#include <cmath>
#include <iostream>	// operator << for default types
#include <array>
#include <typeinfo>

#include "MathHelper.h"

namespace HE {

/*
* A universal usable vector class, usable with integral and floating point types.. if you need universal containers for more complex types use std::vector or std::deque
*
* This class supports basic arithmetic but is by no means yet completely safe (no overflow protection included) so be careful when dealing with unsigned types!
*/
template<typename U, unsigned int Size, typename = typename std::enable_if_t<std::is_arithmetic<U>::value>>
class Vec
{
public:
	// Constructor for universal/forward reference .. if, for whatever reason, this ever needs implementation consider SFINAE to make the copy ctor available!
	//template<typename... Args>
	//Vec(Args&&... args)
	//	: m_content({std::forward<U>(static_cast<U>(args))...}) {
	//	static_assert(sizeof...(Args) == 0 || sizeof...(Args) == Size, "Vec: Wrong number of initializers, use default-init or exact params");
	//}

	// Using a universal/forward reference ctor is actually not giving us anything for fundamental types, there is no performance gain on moving.. So we create per value instead..
	template<typename... Args,
		typename = typename std::enable_if_t<sizeof...(Args) == Size || sizeof...(Args) == 0 >> // Accept only 0 params or exact match
		explicit Vec(const Args... args)
		: m_data({static_cast<U>(args)...}) {}

	const U& operator[] (std::size_t index) const {
		return m_data[index];
	}

	U& operator[] (std::size_t index) {
		return m_data[index];
	}

	// ostream operator <<, prints vector content to stream
	// Note: GCC had problems with it as free function, maybe try again sometime..
	friend std::ostream& operator<< (std::ostream& out, const Vec& vec) {
		out << "Vec<" << typeid(U).name() << "> ";
		for (std::size_t i = 0; i < vec.GetNumArgs(); ++i) {
			out << "[" << i << "]: " << vec.Get(i);
			if (i + 1 < vec.GetNumArgs()) {
				out << ", ";
			}
		}

		return out;
	}

	// By value for fundamental types, they don't have move-ctor's .. if non-fundamental types should ever be supported change this to universal/forward ref
	void Set(std::size_t index, U newValue) {
		m_data[index] = newValue;
	}

	// Get() has a const and a non-const getter because it's basically the same as the [] operator which supports similar behavior. We still return by value because it's faster
	U& Get(std::size_t index) {
		return m_data[index];
	}

	U Get(std::size_t index) const {
		return m_data[index];
	}

	unsigned int GetNumArgs() const {
		return Size;
	}

	decltype(auto) Normalize() {
		// Faster, but should be compiler optimized anyways?
		float magFactor = 1 / Magnitude();

		for (int i = 0; i < Size; ++i) {
			m_data[i] = m_data[i] * magFactor;
		}

		return *this;
	}

	auto Magnitude() {
		float sum = 0.f;

		for (unsigned int i = 0; i < Size; ++i) {
			sum += std::pow(static_cast<float>(m_data[i]), 2);
		}

		return std::sqrt(sum);
	}

private:
	std::array<U, Size> m_data;
};

using Vec2f = Vec<float, 2>;
using Vec3f = Vec<float, 3>;
using Vec4f = Vec<float, 4>;

using Vec2i = Vec<int, 2>;
using Vec3i = Vec<int, 3>;
using Vec4i = Vec<int, 4>;

using Vec2ui = Vec<unsigned int, 2>;
using Vec3ui = Vec<unsigned int, 3>;
using Vec4ui = Vec<unsigned int, 4>;

using RGB = Vec<uint8_t, 3>;
using RGBA = Vec<uint8_t, 4>;

// Helper functions

// Helper to determine whether T or U is floating_point, used to return the right type in operator functions
template<typename T, typename U>
using detType = typename std::conditional<std::is_floating_point<T>::value, T, typename std::conditional<std::is_floating_point<U>::value, U, T>::type>::type;

// Quite complicated and maybe not that cheap, but the most universal solution
template<typename T, typename U, unsigned int Size>
Vec<detType<T, U>, Size> operator+ (const Vec<T, Size>& lhs, const Vec<U, Size>& rhs) {
	Vec<detType<T, U>, Size> newVec;

	for (std::size_t i = 0; i < Size; ++i) {
		newVec[i] = lhs[i] + rhs[i];
	}

	return newVec;
}

template<typename T, typename U, unsigned int Size>
Vec<detType<T, U>, Size> operator- (const Vec<T, Size>& lhs, const Vec<U, Size>& rhs) {
	Vec<detType<T, U>, Size> newVec;

	for (std::size_t i = 0; i < Size; ++i) {
		newVec[i] = lhs[i] - rhs[i];
	}

	return newVec;
}

// Multiplication operator for other vectors, same as operator+ for 2 Vecs, maybe not that cheap
template<typename T, typename U, unsigned int Size>
Vec<detType<T, U>, Size> operator* (const Vec<T, Size>& lhs, const Vec<U, Size>& rhs) {
	Vec<detType<T, U>, Size> newVec;

	for (std::size_t i = 0; i < Size; ++i) {
		if (i < rhs.GetNumArgs()) {
			newVec.Set(i, lhs.Get(i) * rhs.Get(i));
		}
	}

	return newVec;
}

// Multiplication operator for integral operands
template<typename T, unsigned int Size>
Vec<T, Size> operator* (Vec<T, Size> lhs, int rhs) {
	for (unsigned int i = 0; i < Size; ++i) {
		lhs.Get(i) *= rhs;
	}

	return lhs;
}

// Multiplicatiopn operator for floating point operands, be careful with narrowing on integral vectors
template<typename T, unsigned int Size>
Vec<T, Size> operator* (Vec<T, Size> lhs, float rhs) {
	for (std::size_t i = 0; i < Size; ++i) {
		lhs.Get(i) *= rhs;
	}

	return lhs;
}

template<typename T, unsigned int Size>
Vec<T, Size>& operator*= (Vec<T, Size>& lhs, Vec<T, Size>& rhs) {
	for (unsigned int i = 0; i < Size; ++i) {
		lhs[i] *= rhs[i];
	}

	return lhs;
}

template<typename T, unsigned int Size>
Vec<T, Size>& operator*= (Vec<T, Size>& lhs, int rhs) {
	for (unsigned int i = 0; i < Size; ++i) {
		lhs[i] *= rhs;
	}

	return lhs;
}

template<typename T, unsigned int Size>
Vec<T, Size>& operator*= (Vec<T, Size>& lhs, float rhs) {
	for (unsigned int i = 0; i < Size; ++i) {
		lhs[i] *= rhs;
	}

	return lhs;
}

template<typename T, unsigned int Size>
Vec<T, Size>& operator+= (Vec<T, Size>& lhs, Vec<T, Size>& rhs) {
	for (unsigned int i = 0; i < Size; ++i) {
		lhs[i] += rhs[i];
	}

	return lhs;
}

template<typename T, unsigned int Size>
Vec<T, Size>& operator+= (Vec<T, Size>& lhs, int rhs) {
	for (unsigned int i = 0; i < Size; ++i) {
		lhs[i] += rhs;
	}

	return lhs;
}

template<typename T, unsigned int Size>
Vec<T, Size>& operator+= (Vec<T, Size>& lhs, float rhs) {
	for (unsigned int i = 0; i < Size; ++i) {
		lhs[i] += rhs;
	}

	return lhs;
}

template<typename T, unsigned int Size>
float VecMagnitude(const Vec<T, Size>& vec) {
	float sum = 0;

	for (int i = 0; i < Size; ++i) {
		sum += std::pow(static_cast<float>(vec[i]), 2);
	}

	return std::sqrt(sum);
}

// Euclidean distance
template<typename T, typename U, unsigned int Size>
inline float VecDistance(const Vec<T, Size>& vec1, const Vec<U, Size>& vec2) {
	float sum = 0.f;

	for (unsigned int i = 0; i < Size; ++i) {
		sum += std::pow(static_cast<float>(vec1[i]) - static_cast<float>(vec2[i]), 2);
	}

	return std::sqrt(sum);
}

// Dot product of 2D vector (in degree angle)
template<typename T, typename U>
float VecDot(const Vec<T, 2>& vec1, const Vec<U, 2>& vec2) {
	return vec1.Get(0) * vec2.Get(0) + vec1.Get(1) * vec2.Get(1);
}

// Dot product of 3D vector (in degree angle)
template<typename T, typename U>
float VecDot(const Vec<T, 3>& vec1, const Vec<U, 3>& vec2) {
	return vec1.Get(0) * vec2.Get(0) + vec1.Get(1) * vec2.Get(1) + vec1.Get(2) * vec2.Get(2);
}

// Cross product of 3D vector (3D right angle, remember right hand rule)
template<typename T, typename U>
Vec<float, 3> VecCross(const Vec<T, 3>& vec1, const Vec<U, 3>& vec2) {
	float x = vec1.Get(1) * vec2.Get(2) - vec1.Get(2) * vec2.Get(1);
	float y = vec1.Get(2) * vec2.Get(0) - vec1.Get(0) * vec2.Get(2);
	float z = vec1.Get(0) * vec2.Get(1) - vec1.Get(1) * vec2.Get(0);

	return Vec<float, 3>{x, y, z};
}

} // namespace HE
