#pragma once

namespace HE {

constexpr float Pi = 3.14159265359f;

// Convert degree angle to rad angle
constexpr float FDegreeToRad(float m_degree) {
	return m_degree * Pi / 180.f;
}

// Convert rad angle to degree angle
constexpr float FRadToDegree(float rad) {
	return rad * 180 / Pi;
}

// TODO: Add constexpr support for HRad/HDegree if possible..

class Rad;

class Degree {
public:
	Degree();

	explicit Degree(float degree);

	Degree(const Degree&) = default;
	Degree& operator= (const Degree&) = default;
	Degree(Degree&&) = default;
	Degree& operator= (Degree&&) = default;

	Degree(const Rad& rad);
	Degree& operator= (const Rad& rad);

	// How expensive is this? Since the class is POD and has only a float member..? 
	// operator HRad() const;

	bool operator== (const Degree& other) const;

	float GetValue() const;
	Rad ToRad() const;

private:
	float m_degree;
};

inline Degree::Degree()
	: m_degree{0.f} {}

inline Degree::Degree(float degree)
	: m_degree{degree} {}

// Missing degree to rad conversion operator and assignment operator see after HRad definition (we forward declared it..)

inline bool Degree::operator== (const Degree& other) const {
	return m_degree == other.GetValue();
}

inline float Degree::GetValue() const {
	return m_degree;
}

class Rad {
public:
	Rad();

	explicit Rad(float rad);

	Rad(const Rad&) = default;
	Rad& operator= (const Rad&) = default;
	Rad(Rad&&) = default;
	Rad& operator= (Rad&&) = default;

	Rad(const Degree& degree);
	Rad& operator= (const Degree& degree);

	// How expensive is this? Since the class is POD and has only a float member..? 
	// operator HDegree() const;

	bool operator== (const Rad& other) const;

	static Rad FromDegree(float degree);

	float GetValue() const;
	Degree ToDegree() const;

private:
	float m_rad;
};

// First some function definitions for Degree involving HRad, because we forward declared HRad earlier

inline Degree::Degree(const Rad& rad)
	: m_degree{rad.ToDegree().GetValue()} {}

inline Degree& Degree::operator= (const Rad& rad) {
	m_degree = rad.ToDegree().GetValue();
	return *this;
}

inline Rad Rad::FromDegree(float degree) {
	return Rad{degree * Pi / 180.f};
}

inline Rad Degree::ToRad() const {
	return Rad{m_degree * Pi / 180.f};
}

//inline HDegree::operator HRad() const {
//	return HDegree{ToRad()};
//}

// End

inline Rad::Rad()
	: m_rad{0.f} {}

inline Rad::Rad(float rad)
	: m_rad{rad} {}

inline Rad::Rad(const Degree& degree)
	: m_rad{degree.ToRad().GetValue()} {}

inline Rad& Rad::operator= (const Degree& degree) {
	m_rad = degree.ToRad().GetValue();
	return *this;
}

//inline HRad::operator HDegree() const {
//	return HRad{ToDegree()};
//}

inline bool Rad::operator== (const Rad& other) const {
	return m_rad == other.GetValue();
}

inline float Rad::GetValue() const {
	return m_rad;
}

inline Degree Rad::ToDegree() const {
	return Degree{m_rad * 180.f / Pi};
}

// HRad / HDegree free function operators
inline Degree operator+ (const Degree& lhs, const Degree& rhs) {
	return Degree{lhs.GetValue() + rhs.GetValue()};
}

inline Degree operator* (const Degree& lhs, const Degree& rhs) {
	return Degree{lhs.GetValue() * rhs.GetValue()};
}

inline Degree& operator+= (Degree& lhs, const Degree& rhs) {
	lhs = lhs + rhs;
	return lhs;
}

inline Degree& operator*= (Degree& lhs, const Degree& rhs) {
	lhs = lhs * rhs;
	return lhs;
}

inline Rad operator+ (const Rad& lhs, const Rad& rhs) {
	return Rad{lhs.GetValue() + rhs.GetValue()};
}

inline Rad operator* (const Rad& lhs, const Rad& rhs) {
	return Rad{lhs.GetValue() * rhs.GetValue()};
}

inline Rad& operator+= (Rad& lhs, const Rad& rhs) {
	lhs = lhs + rhs;
	return lhs;
}

inline Rad& operator*= (Rad& lhs, const Rad& rhs) {
	lhs = lhs * rhs;
	return lhs;
}

} // namespace HE